import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:demozzzzzzz/Screen/App/HeaderBar.dart';
import 'package:demozzzzzzz/Screen/App/HomeScreen/HomeScreenController.dart';
import 'package:demozzzzzzz/Screen/CustomDialog.dart';
import 'package:demozzzzzzz/Services/LoginInfo.dart';
import 'package:demozzzzzzz/Services/ServiceError.dart';
import 'package:demozzzzzzz/Services/SharePref.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MenuScreen extends StatefulWidget {
  final LoginInfo loginInfo = LoginInfo.getInstance();

  @override
  State<StatefulWidget> createState() {
    return MenuScreenState();
  }
}

class MenuScreenState extends State<MenuScreen> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  String _newAPI;
  String _testAPI;
  int _timeOut = 20;

  SharedPreferences _sharedPreferences;

  TextEditingController _timeOutTextController = TextEditingController(
    text: "...",
  );

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((value) {
      _sharedPreferences = value;
      setState(() {
        _timeOut = _sharedPreferences.getInt("timeOut") ?? 20;
        _timeOutTextController.text = _timeOut.toString();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MenuScreenColor.background,
        body: Column(
          children: <Widget>[
            HeaderBar(
              height: ScreenSize.safeBlockVerticalSize * 6,
              width: ScreenSize.safeBlockHorizontalSize * 100,
              title: "Menu",
              color: MenuScreenColor.header,
            ),
            Expanded(
              child: buildBody(),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildBody() {
    return SingleChildScrollView(
      child: Container(
        child: Form(
          key: _key,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: (widget.loginInfo.isSuperUser) ? ScreenSize
                    .safeBlockVerticalSize * 12 : ScreenSize
                    .safeBlockVerticalSize * 20,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                color: Colors.white12,
                child: Center(
                  child: Text(
                    "Welcome, " + widget.loginInfo.getUsername,
                    style: TextStyle(
                      fontSize: ScreenSize.safeBlockVerticalSize * 4,
                      fontWeight: FontWeight.w500,
                      color: MenuScreenColor.button,
                    ),
                  ),
                ),
              ),
              (widget.loginInfo.isSuperUser) ? Container(
                height: ScreenSize.safeBlockVerticalSize * 8,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                color: Colors.white12,
                child: Center(
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: ScreenSize.safeBlockVerticalSize * 7,
                        width: ScreenSize.safeBlockHorizontalSize * 100,
                        color: Colors.black38,
                        child: Center(
                          child: Text(
                            "- SuperUser -",
                            style: TextStyle(
                              fontSize: ScreenSize.safeBlockVerticalSize * 2.5,
                              fontWeight: FontWeight.w500,
                              color: MenuScreenColor.button,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ) : Container(),
              (widget.loginInfo.isSuperUser) ? Container(
                height: ScreenSize.safeBlockVerticalSize * 13,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                padding: EdgeInsets.symmetric(
                  vertical: ScreenSize.safeBlockVerticalSize * 1,
                  horizontal: ScreenSize.safeBlockHorizontalSize * 2,
                ),
                color: Colors.white12,
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: ScreenSize.safeBlockVerticalSize * 3,
                        width: ScreenSize.safeBlockHorizontalSize * 100,
                        child: Center(
                          child: Text(
                            "Token",
                            style: TextStyle(
                              fontSize: ScreenSize.safeBlockVerticalSize * 2,
                              color: MenuScreenColor.button,
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            height: ScreenSize.safeBlockVerticalSize * 8,
                            width: ScreenSize.safeBlockHorizontalSize * 76,
                            color: Colors.white12,
                            child: Center(
                              child: Text(
                                widget.loginInfo.getToken,
                                style: TextStyle(
                                  fontSize: ScreenSize.safeBlockVerticalSize *
                                      2.5,
                                  color: MenuScreenColor.button,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            height: ScreenSize.safeBlockVerticalSize * 8,
                            width: ScreenSize.safeBlockHorizontalSize * 20,
                            color: Colors.white12,
                            child: MaterialButton(
                              padding: EdgeInsets.zero,
                              child: Center(
                                child: Text(
                                  "Copy",
                                  style: TextStyle(
                                    fontSize: ScreenSize.safeBlockVerticalSize *
                                        2,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              onPressed: () async {
                                Clipboard.setData(ClipboardData(
                                    text: widget.loginInfo.getToken));
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ) : Container(),
              (widget.loginInfo.isSuperUser) ? Container(
                height: ScreenSize.safeBlockVerticalSize * 13,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                padding: EdgeInsets.symmetric(
                  vertical: ScreenSize.safeBlockVerticalSize * 1,
                  horizontal: ScreenSize.safeBlockHorizontalSize * 2,
                ),
                color: Colors.white12,
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: ScreenSize.safeBlockVerticalSize * 3,
                        width: ScreenSize.safeBlockHorizontalSize * 100,
                        child: Center(
                          child: Text(
                            "Set New API",
                            style: TextStyle(
                              fontSize: ScreenSize.safeBlockVerticalSize * 2,
                              color: MenuScreenColor.button,
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            height: ScreenSize.safeBlockVerticalSize * 8,
                            width: ScreenSize.safeBlockHorizontalSize * 76,
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenSize.safeBlockHorizontalSize *
                                  3,
                            ),
                            color: Colors.white12,
                            child: Center(
                              child: TextFormField(
                                initialValue: SharePref.sourceAPI,
                                      style: TextStyle(
                                        fontSize:
                                            ScreenSize.safeBlockVerticalSize *
                                                2,
                                        color: MenuScreenColor.button,
                                      ),
                                      onSaved: (value) {
                                        _newAPI = value;
                                      },
                                    ),
                            ),
                          ),
                          Container(
                            height: ScreenSize.safeBlockVerticalSize * 8,
                            width: ScreenSize.safeBlockHorizontalSize * 20,
                            color: Colors.white12,
                            child: MaterialButton(
                              padding: EdgeInsets.zero,
                              child: Center(
                                child: Text(
                                  "Set",
                                  style: TextStyle(
                                    fontSize: ScreenSize.safeBlockVerticalSize *
                                        2,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              onPressed: () async {
                                bool isReload = await CustomDialog
                                    .showSelectionDialog(context, "Alert",
                                    "Do you wish to set new api and reload app\n(This will not affect non-SuperUser)");
                                if (isReload) {
                                  _key.currentState.save();
                                  await SharePref.setSourceAPI(_newAPI);
                                  HomeScreenController().logOut(false);
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ) : Container(),
              (widget.loginInfo.isSuperUser) ? Container(
                height: ScreenSize.safeBlockVerticalSize * 13,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                padding: EdgeInsets.symmetric(
                  vertical: ScreenSize.safeBlockVerticalSize * 1,
                  horizontal: ScreenSize.safeBlockHorizontalSize * 2,
                ),
                color: Colors.white12,
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: ScreenSize.safeBlockVerticalSize * 3,
                        width: ScreenSize.safeBlockHorizontalSize * 100,
                        child: Center(
                          child: Text(
                            "Test GET API",
                            style: TextStyle(
                              fontSize: ScreenSize.safeBlockVerticalSize * 2,
                              color: MenuScreenColor.button,
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            height: ScreenSize.safeBlockVerticalSize * 8,
                            width: ScreenSize.safeBlockHorizontalSize * 76,
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenSize.safeBlockHorizontalSize *
                                  3,
                            ),
                            color: Colors.white12,
                            child: Center(
                              child: TextFormField(
                                initialValue: SharePref.sourceAPI,
                                style: TextStyle(
                                  fontSize: ScreenSize.safeBlockVerticalSize *
                                      2,
                                  color: MenuScreenColor.button,
                                ),
                                onSaved: (value) {
                                  _testAPI = value;
                                },
                              ),
                            ),
                          ),
                          Container(
                            height: ScreenSize.safeBlockVerticalSize * 8,
                            width: ScreenSize.safeBlockHorizontalSize * 20,
                            color: Colors.white12,
                            child: MaterialButton(
                              padding: EdgeInsets.zero,
                              child: Center(
                                child: Text(
                                  "GET",
                                  style: TextStyle(
                                    fontSize: ScreenSize.safeBlockVerticalSize *
                                        2,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              onPressed: () async {
                                _key.currentState.save();
                                CustomDialog.showLoadingDialog(context);
                                try {
                                  final Response response = await get(
                                    _testAPI,
                                    headers: {
                                      'id': widget.loginInfo.getToken,
                                    },
                                  ).timeout(
                                    Duration(
                                      seconds: 20,
                                    ),
                                  );
                                  Navigator.of(context).pop();
                                  if (response.statusCode == 200) {
                                    CustomDialog.showGiganticAlertDialog(
                                        context, "Test GET API",
                                        json.decode(response.body).toString());
                                  } else {
                                    CustomDialog.showAlertDialog(context,
                                        "Error " +
                                            response.statusCode.toString(),
                                        ErrorStatus(response.statusCode)
                                            .toString());
                                  }
                                } on TimeoutException {
                                  Navigator.of(context).pop();
                                  CustomDialog.showAlertDialog(
                                      context, "Error 408",
                                      ErrorStatus(408).toString());
                                } on SocketException {
                                  Navigator.of(context).pop();
                                  CustomDialog.showAlertDialog(
                                      context, "No Connection",
                                      ErrorStatus(6969).toString());
                                } on Exception {
                                  Navigator.of(context).pop();
                                  CustomDialog.showAlertDialog(
                                      context, "Error", "Unknown");
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ) : Container(),
              (widget.loginInfo.isSuperUser) ? Container(
                height: ScreenSize.safeBlockVerticalSize * 13,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                padding: EdgeInsets.symmetric(
                  vertical: ScreenSize.safeBlockVerticalSize * 1,
                  horizontal: ScreenSize.safeBlockHorizontalSize * 2,
                ),
                color: Colors.white12,
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: ScreenSize.safeBlockVerticalSize * 3,
                        width: ScreenSize.safeBlockHorizontalSize * 100,
                        child: Center(
                          child: Text(
                            "Set New TimeOut Duration",
                            style: TextStyle(
                              fontSize: ScreenSize.safeBlockVerticalSize * 2,
                              color: MenuScreenColor.button,
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            height: ScreenSize.safeBlockVerticalSize * 8,
                            width: ScreenSize.safeBlockHorizontalSize * 76,
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenSize.safeBlockHorizontalSize *
                                  3,
                            ),
                            color: Colors.white12,
                            child: Center(
                              child: TextFormField(
                                textAlign: TextAlign.center,
                                controller: _timeOutTextController,
                                style: TextStyle(
                                  fontSize: ScreenSize.safeBlockVerticalSize *
                                      2,
                                  color: MenuScreenColor.button,
                                ),
                                onSaved: (value) {
                                  _timeOut = int.parse(value) ?? "20";
                                },
                              ),
                            ),
                          ),
                          Container(
                            height: ScreenSize.safeBlockVerticalSize * 8,
                            width: ScreenSize.safeBlockHorizontalSize * 20,
                            color: Colors.white12,
                            child: MaterialButton(
                              padding: EdgeInsets.zero,
                              child: Center(
                                child: Text(
                                  "Set",
                                  style: TextStyle(
                                    fontSize: ScreenSize.safeBlockVerticalSize *
                                        2,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              onPressed: () async {
                                bool isReload = await CustomDialog
                                    .showSelectionDialog(context, "Alert",
                                    "This will also affect non-SuperUser\nDo you wish to set time out");
                                if (isReload) {
                                  _key.currentState.save();
                                  await _sharedPreferences.setInt(
                                      "timeOut", _timeOut);
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ) : Container(),
              Container(
                height: ScreenSize.safeBlockVerticalSize * 10,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                padding: EdgeInsets.symmetric(
                  vertical: ScreenSize.safeBlockVerticalSize * 1,
                  horizontal: ScreenSize.safeBlockHorizontalSize * 2,
                ),
                color: Colors.white12,
                child: Center(
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: ScreenSize.safeBlockVerticalSize * 8,
                        width: ScreenSize.safeBlockHorizontalSize * 76,
                        color: Colors.white12,
                        child: Center(
                          child: Text(
                            "ASSAI-chan message",
                            style: TextStyle(
                              fontSize: ScreenSize.safeBlockVerticalSize * 2.5,
                              fontWeight: FontWeight.w500,
                              color: MenuScreenColor.button,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: ScreenSize.safeBlockVerticalSize * 8,
                        width: ScreenSize.safeBlockHorizontalSize * 20,
                        color: Colors.white12,
                        child: Center(
                          child: Switch(
                            value: ErrorStatus.isASSAIChanCode,
                            activeColor: MenuScreenColor.button,
                            onChanged: (value) async {
                              await ErrorStatus.setASSAIChanCode(value);
                              setState(() {

                              });
                              await CustomDialog.showAlertDialog(
                                  context, "Test Message",
                                  ErrorStatus(200).toString());
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: ScreenSize.safeBlockVerticalSize * 8,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                padding: EdgeInsets.all(ScreenSize.safeBlockVerticalSize * 1),
                child: MaterialButton(
                  padding: EdgeInsets.zero,
                  color: MenuScreenColor.button,
                  highlightColor: Colors.transparent,
                  focusColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  child: Center(
                    child: Text(
                      "Log out",
                      style: TextStyle(
                        fontSize: ScreenSize.safeBlockVerticalSize * 2.3,
                        fontWeight: FontWeight.w500,
                        color: MenuScreenColor.text,
                      ),
                    ),
                  ),
                  onPressed: () async {
                    bool willExit = await CustomDialog.showSelectionDialog(
                        context, "Warning",
                        "Are you sure you want to log out?");
                    if (willExit) {
                      HomeScreenController().logOut(true);
                    }
                  },
                ),
              ),
              Container(
                height: ScreenSize.safeBlockVerticalSize * 8,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                padding: EdgeInsets.all(ScreenSize.safeBlockVerticalSize * 1),
                child: MaterialButton(
                  color: MenuScreenColor.button,
                  padding: EdgeInsets.zero,
                  highlightColor: Colors.transparent,
                  focusColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  child: Center(
                    child: Text(
                      "Service Info",
                      style: TextStyle(
                        fontSize: ScreenSize.safeBlockVerticalSize * 2.3,
                        fontWeight: FontWeight.w500,
                        color: MenuScreenColor.text,
                      ),
                    ),
                  ),
                  onPressed: () async {
                    await CustomDialog.showAlertDialog(
                        context, "Alert", "Coming Soon");
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
