import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashSlideForm {
  final String title;
  final String subTitle;
  final Color backgroundColor;
  final String picturePath;
  final Color titleColor;
  final Color subTitleColor;

  SplashSlideForm(
    this.title,
    this.titleColor,
    this.subTitle,
    this.subTitleColor,
    this.backgroundColor,
    this.picturePath,
  );
}

List<SplashSlideForm> splashSlideList = [
  SplashSlideForm(
    "Out of anime to watch?",
    Colors.white,
    "We can help",
    Colors.white54,
    LoginScreenColor.slide1Background,
    "assets/Splash/SplashSlide1.png",
  ),
  SplashSlideForm(
    "Have I watched this one?",
    Colors.white,
    "A personal note for you",
    Colors.white54,
    LoginScreenColor.slide2Background,
    "assets/Splash/SplashSlide2.png",
  ),
  SplashSlideForm(
    "A personal movie list designed for me?",
    Colors.black,
    "Correct, shall we start!",
    Colors.black87,
    LoginScreenColor.slide3Background,
    "assets/Splash/SplashSlide3.png",
  ),
];
