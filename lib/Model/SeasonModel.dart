import 'package:flutter/material.dart';

class SeasonModel {
  String name;
  String releaseDate;
  int numberOfEpisode;
  bool isCompleted;
  String link;

  SeasonModel({
    @required this.name,
    @required this.releaseDate,
    @required this.numberOfEpisode,
    @required this.isCompleted,
    @required this.link,
  });

  factory SeasonModel.fromJson(Map<String, dynamic> json) {
    return new SeasonModel(
      name: json['name'].toString(),
      releaseDate: json['releaseDate'].toString(),
      numberOfEpisode: json['numberOfEpisode'] ?? 0,
      isCompleted: (json['isCompleted'].toString() == "1") ? true : false,
      link: json['link'].toString(),
    );
  }
}
