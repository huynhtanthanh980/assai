import 'dart:math' as math;

import 'package:demozzzzzzz/Data/DataSource.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Screen/App/HomeScreen/HomeScreenController.dart';
import 'package:demozzzzzzz/Screen/App/SearchScreen/SearchController.dart';
import 'package:demozzzzzzz/Services/MovieLoader.dart';
import 'package:demozzzzzzz/Services/SharePref.dart';
import 'package:demozzzzzzz/Services/UrlLauncher.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FavWatchController {
  static FavWatchController _instance;

  MovieLoader _watchingLoader;
  MovieLoader _favouriteLoader;

  int _watchingSortType = 0;
  int _favouriteSortType = 0;
  List<MovieModel> _watchingList;
  List<MovieModel> _favouriteList;

  //Constructor/////////////////////////////////////////////////////////////////

  FavWatchController._privateConstructor(String _token) {
    String _watchApiLink = (_token == DataSource.localTestToken)
        ? DataSource.watchDemo
        : (SharePref.sourceAPI + "/watch");
    this._watchingLoader = MovieLoader(_watchApiLink, _token);
    String _favApiLink = (_token == DataSource.localTestToken)
        ? DataSource.favouriteDemo
        : (SharePref.sourceAPI + "/fav");
    this._favouriteLoader = MovieLoader(_favApiLink, _token);
  }

  factory FavWatchController(String token) {
    if (_instance == null) {
      _instance = FavWatchController._privateConstructor(token);
    }
    return _instance;
  }

  factory FavWatchController.getInstance() {
    return _instance;
  }

  static void dispose() {
    _instance = null;
  }

  //Loader//////////////////////////////////////////////////////////////////////
  List<MovieModel> get getWatchingList {
    return _watchingList;
  }

  List<MovieModel> get getFavouriteList {
    return _favouriteList;
  }

  Future<void> loadList(int timeOut) async {
    try {
      _favouriteList = await _favouriteLoader.loadMovie(timeOut);
      _watchingList = await _watchingLoader.loadMovie(timeOut);
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> loadWatchingList(int timeOut) async {
    try {
      await loadList(timeOut);
      return _watchingList;
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> loadFavouriteList(int timeOut) async {
    try {
      await loadList(timeOut);
      return _favouriteList;
    } on Exception {
      rethrow;
    }
  }

  bool isWatchingNull() {
    return _watchingList == null;
  }

  bool isFavouriteNull() {
    return _favouriteList == null;
  }

  void setWatchingNull() {
    _watchingList = null;
  }

  void setFavouriteNull() {
    _favouriteList = null;
  }

  void reload() {
    setWatchingNull();
    setFavouriteNull();
  }

  Future<void> launchURL(String url) async {
    await UrlLauncher.launchURL(url);
  }

  void onLogOutError() {
    HomeScreenController().logOut(true);
  }

  void reloadSearch() {
    SearchController.getInstance().reload();
  }

  //Sorting/////////////////////////////////////////////////////////////////////
  int getSortType(bool isWatching) {
    return isWatching ? _watchingSortType : _favouriteSortType;
  }

  Widget getSortIcon(bool isWatching, double size, Color color) {
    switch (isWatching ? _watchingSortType : _favouriteSortType) {
      case 0:
        return Icon(
          Icons.sort_by_alpha,
          size: size,
          color: color,
        );
      case 1:
        return Icon(
          Icons.sort,
          size: size,
          color: color,
        );
      case 2:
        return Transform(
          alignment: Alignment.center,
          transform: Matrix4.rotationY(math.pi),
          child: Transform.rotate(
            angle: -math.pi,
            child: Icon(
              Icons.sort,
              size: size,
              color: color,
            ),
          ),
        );
      default:
        return Icon(
          Icons.sort,
          size: size,
          color: color,
        );
    }
  }

  int changeSort(bool isWatching) {
    if (isWatching) {
      _watchingSortType = (_watchingSortType == 2) ? 0 : _watchingSortType + 1;
      return _watchingSortType;
    } else {
      _favouriteSortType =
          (_favouriteSortType == 2) ? 0 : _favouriteSortType + 1;
      return _favouriteSortType;
    }
  }

  List<MovieModel> sort(List<MovieModel> movieList, bool isWatching) {
    int _sortType = isWatching ? _watchingSortType : _favouriteSortType;
    movieList.sort((movie1, movie2) {
      String compare1 = movie1.name;
      String compare2 = movie2.name;

      if (_sortType == 1) {
        compare1 = (100 - movie1.compatibleScore).toString() + compare1;
        compare2 = (100 - movie2.compatibleScore).toString() + compare2;
      } else if (_sortType == 2) {
        compare1 = movie1.compatibleScore.toString() + compare1;
        compare2 = movie2.compatibleScore.toString() + compare2;
      }

      return compare1.compareTo(compare2);
    });

    return movieList;
  }
}
