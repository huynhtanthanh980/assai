import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:demozzzzzzz/Screen/App/TinderScreen/CardController.dart';
import 'package:demozzzzzzz/Screen/App/TinderScreen/SwipableCard.dart';
import 'package:demozzzzzzz/Screen/App/TinderScreen/TinderCard.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class TinderScreen extends StatefulWidget {
  final CardController cardController = CardController.getInstance();

  @override
  State<StatefulWidget> createState() {
    return TinderScreenState();
  }
}

class TinderScreenState extends State<TinderScreen> {
  double bottomHeight;
  double topPadding;
  double leftPadding;
  double cardHeight;
  double cardWidth;

  double stackHeight;

  @override
  void initState() {
    super.initState();
    cardHeight = ScreenSize.safeBlockVerticalSize * 80;
    bottomHeight = ScreenSize.safeBlockVerticalSize * 18;
    stackHeight = ScreenSize.safeBlockVerticalSize * 100 - bottomHeight;
    topPadding = (stackHeight - cardHeight) / 2;
    cardWidth = ScreenSize.safeBlockHorizontalSize * 95;
    leftPadding = (ScreenSize.safeBlockHorizontalSize * 100 - cardWidth) / 2;

    widget.cardController.getNewList(true, 10);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        widget.cardController.onCancelCompleter();
        if (widget.cardController.willReload) {
          widget.cardController.reloadOtherList();
        }
        Navigator.of(context).pop();
        return true;
      },
      child: Scaffold(
        key: widget.cardController.scaffoldKey,
        resizeToAvoidBottomPadding: false,
        backgroundColor: TinderScreenColor.background,
        body: SafeArea(
          child: buildBody(),
        ),
      ),
    );
  }

  Widget buildBody() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 100,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      child: StreamBuilder<List<MovieModel>>(
        stream: widget.cardController.getCardList,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            return Stack(
              children: buildSwipeCardList(snapshot.data),
            );
          }
          return buildBottomCard(true);
        },
      ),
    );
  }

  List<Widget> buildSwipeCardList(List<MovieModel> movieList) {
    List<Widget> result = List<Widget>.generate(movieList.length, (index) {
      MovieModel movieModel = movieList[index];
      return new SwipeableCard(
        id: movieList[index].id.toString(),
        swipeController: widget.cardController.swipeController,
        topPadding: topPadding,
        leftPadding: leftPadding,
        cardHeight: cardHeight,
        cardWidth: cardWidth,
        movieModel: movieModel,
        onLike: () {
          widget.cardController.likeCard.add(movieModel);
        },
        onDislike: () {
          widget.cardController.dislikeCard.add(movieModel);
        },
      );
    });

    result.insert(0, buildBottomCard(movieList.length <= 1));

    return result;
  }

  Widget buildBottomCard(bool isLoading) {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 100,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      child: Column(
        children: <Widget>[
          Container(
            height: stackHeight,
            width: ScreenSize.safeBlockHorizontalSize * 100,
            child: (!isLoading)
                ? Container()
                : Center(
                    child: TinderCard(
                      height: cardHeight,
                      width: cardWidth,
                      movieDetail: null,
                      likeStatus: 0,
                      onPressed: null,
                    ),
                  ),
          ),
          Container(
            height: bottomHeight,
            width: ScreenSize.safeBlockHorizontalSize * 100,
            child: buildBottomButton(),
          ),
        ],
      ),
    );
  }

  Widget buildBottomButton() {
    double bigButtonSize = bottomHeight * 0.6;
    double bigIconSize = bottomHeight * 0.35;
    double smallButtonSize = bottomHeight * 0.4;
    double smallIconSize = bottomHeight * 0.3;

    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: smallButtonSize,
            width: smallButtonSize,
            child: MaterialButton(
              elevation: 1.0,
              padding: EdgeInsets.zero,
              shape: CircleBorder(),
              color: Colors.white,
              child: Icon(
                Icons.arrow_back,
                color: Colors.orangeAccent,
                size: smallIconSize,
              ),
              onPressed: () async {
                widget.cardController.onCancelCompleter();
                if (widget.cardController.willReload) {
                  widget.cardController.reloadOtherList();
                }
                Navigator.of(context).pop();
              },
            ),
          ),
          Container(
            height: bigButtonSize,
            width: bigButtonSize,
            child: MaterialButton(
              padding: EdgeInsets.zero,
              shape: CircleBorder(),
              color: Colors.white,
              child: Center(
                child: Icon(
                  Icons.thumb_down,
                  color: Colors.redAccent,
                  size: bigIconSize,
                ),
              ),
              onPressed: () async {
                await widget.cardController.onDislikeButtonPressed();
              },
            ),
          ),
          Container(
            height: bigButtonSize,
            width: bigButtonSize,
            child: MaterialButton(
              padding: EdgeInsets.zero,
              shape: CircleBorder(),
              color: Colors.white,
              child: Center(
                child: Icon(
                  Icons.thumb_up,
                  color: Colors.greenAccent,
                  size: bigIconSize,
                ),
              ),
              onPressed: () async {
                await widget.cardController.onLikeButtonPressed();
              },
            ),
          ),
          Container(
            height: smallButtonSize,
            width: smallButtonSize,
            child: MaterialButton(
              padding: EdgeInsets.zero,
              shape: CircleBorder(),
              color: Colors.white,
              child: Icon(
                Icons.add,
                color: Colors.deepPurpleAccent,
                size: smallIconSize,
              ),
              onPressed: () async {
                await widget.cardController.onAddButtonPressed();
              },
            ),
          ),
        ],
      ),
    );
  }
}