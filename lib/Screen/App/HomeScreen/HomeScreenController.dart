import 'dart:async';

import 'package:demozzzzzzz/Screen/App/FavWatchController.dart';
import 'package:demozzzzzzz/Screen/App/FavouriteScreen/FavouriteScreen.dart';
import 'package:demozzzzzzz/Screen/App/MenuScreen/MenuScreen.dart';
import 'package:demozzzzzzz/Screen/App/SearchScreen/SearchController.dart';
import 'package:demozzzzzzz/Screen/App/SearchScreen/SearchScreen.dart';
import 'package:demozzzzzzz/Screen/App/TinderScreen/CardController.dart';
import 'package:demozzzzzzz/Screen/App/TinderScreen/TinderScreen.dart';
import 'package:demozzzzzzz/Screen/App/WatchingScreen/WatchingScreen.dart';
import 'package:demozzzzzzz/Screen/Login/LoginScreen/LoginController.dart';
import 'package:demozzzzzzz/Screen/Login/LoginScreen/LoginScreen.dart';
import 'package:demozzzzzzz/Services/LoginInfo.dart';
import 'package:demozzzzzzz/Services/LoginService.dart';
import 'package:demozzzzzzz/Services/PostService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class HomeScreenController {
  static HomeScreenController _instance;

  GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  int _currentScreenIndex = 0;

  GlobalKey<ScaffoldState> get getScaffordKey => _key;

  //Constructor/////////////////////////////////////////////////////////////////

  HomeScreenController._privateConstructor() {
    _screenController.stream.listen(_buildNewScreen);
  }

  factory HomeScreenController() {
    if (_instance == null) {
      _instance = HomeScreenController._privateConstructor();
    }
    return _instance;
  }

  void dispose() {
    _screenController.close();
    _getScreenBuildSubject.close();
    _getCurrentScreenIndex.close();
    _instance = null;
  }

  //Controller//////////////////////////////////////////////////////////////////

  Sink<int> get changeScreen => _screenController.sink;
  final _screenController = StreamController<int>();

  Stream<Widget> get getScreenBuildStream => _getScreenBuildSubject.stream;
  final _getScreenBuildSubject = BehaviorSubject<Widget>();

  Stream<int> get getCurrentScreenIndexStream => _getCurrentScreenIndex.stream;
  final _getCurrentScreenIndex = BehaviorSubject<int>();

  void _buildNewScreen(int screenIndex) {
    if (screenIndex >= 0 && screenIndex < 4) {
      _getScreenBuildSubject.add(_getScreen(screenIndex));
      _getCurrentScreenIndex.add(screenIndex);
      _currentScreenIndex = screenIndex;
    }
  }

  Widget _getScreen(int index) {
    switch (index) {
      case 0:
        return WatchingScreen();
      case 1:
        return SearchScreen();
      case 2:
        return FavouriteScreen();
      case 3:
        return MenuScreen();
      case 4:
        return Container();
    }
    return Container();
  }

  Widget get getTinderScreen {
    return TinderScreen();
  }

  int get getCurrentScreenIndex {
    return _currentScreenIndex;
  }

  Future<void> logOut(bool isDeleteAll) async {
    if (isDeleteAll) {
      LoginService.deleteLoginSave();
    }

    BuildContext context = _key.currentState.context;

    FavWatchController.dispose();
    SearchController.dispose();
    CardController.dispose();
    LoginController.dispose();
    HomeScreenController().dispose();
    LoginInfo.dispose();
    PostService.dispose();

    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => LoginScreen(
          loginController: LoginController(
            LoginService(),
          ),
          isLoggedIn: !isDeleteAll,
        ),
      ),
    );
  }
}
