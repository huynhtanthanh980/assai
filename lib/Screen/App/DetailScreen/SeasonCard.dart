import 'package:auto_size_text/auto_size_text.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/SeasonModel.dart';
import 'package:demozzzzzzz/Services/UrlLauncher.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SeasonCard extends StatefulWidget {
  final double height;
  final double width;

  final SeasonModel seasonDetail;

  SeasonCard({
    @required this.height,
    @required this.width,
    @required this.seasonDetail,
  });

  @override
  State<StatefulWidget> createState() {
    return SeasonCardState();
  }
}

class SeasonCardState extends State<SeasonCard> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: widget.height * 0.05,
      ),
      child: Container(
        width: widget.width,
        constraints: BoxConstraints(
          minHeight: widget.height * 0.7,
        ),
        padding: EdgeInsets.symmetric(
          vertical: widget.height * 0.05,
        ),
        decoration: BoxDecoration(
          color: DetailScreenColor.seasonCard,
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: widget.width * 0.85,
              padding: EdgeInsets.symmetric(
                vertical: widget.height * 0.1,
                horizontal: widget.width * 0.05,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: widget.width * 0.7,
                    child: AutoSizeText(
                      widget.seasonDetail.name,
                      maxLines: 2,
                      style: TextStyle(
                        fontSize: widget.height * 0.24,
                        fontWeight: FontWeight.w600,
                        color: DetailScreenColor.seasonMainText,
                      ),
                      overflow: TextOverflow.visible,
                    ),
                  ),
                  SizedBox(
                    height: widget.height * 0.1,
                  ),
                  Container(
                    width: widget.width * 0.7,
                    child: Container(
                      child: Text(
                        " - " + widget.seasonDetail.releaseDate + " - ",
                        style: TextStyle(
                          fontSize: widget.height * 0.2,
                          fontWeight: FontWeight.w500,
                          color: Colors.black87,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: widget.height * 0.1,
                  ),
                  Container(
                    width: widget.width * 0.7,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Text(
                            widget.seasonDetail.isCompleted
                                ? "Completed"
                                : "Ongoing",
                            style: TextStyle(
                              fontSize: widget.height * 0.21,
                              fontWeight: FontWeight.w500,
                              color: DetailScreenColor.seasonSubText,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Container(
                          width: widget.width * 0.1,
                        ),
                        Container(
                          child: Text(
                            (widget.seasonDetail.numberOfEpisode > 1)
                                ? widget.seasonDetail.numberOfEpisode
                                        .toString() +
                                    " episodes"
                                : widget.seasonDetail.numberOfEpisode
                                        .toString() +
                                    " episode",
                            style: TextStyle(
                              fontSize: widget.height * 0.21,
                              fontWeight: FontWeight.w500,
                              color: DetailScreenColor.seasonSubText,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: widget.width * 0.15,
              child: MaterialButton(
                padding: EdgeInsets.zero,
                shape: CircleBorder(),
                child: _isLoading
                    ? Container(
                        height: widget.height * 0.3,
                        width: widget.height * 0.3,
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      )
                    : Center(
                        child: Icon(
                          Icons.play_circle_outline,
                          color: DetailScreenColor.background,
                          size: widget.height * 0.4,
                        ),
                      ),
                onPressed: () async {
                  setState(() {
                    _isLoading = true;
                  });
                  await UrlLauncher.launchURL(widget.seasonDetail.link);
                  setState(() {
                    _isLoading = false;
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
