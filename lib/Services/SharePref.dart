import 'package:shared_preferences/shared_preferences.dart';

class SharePref {
  static SharedPreferences instance;
  static String _sourceAPI = "https://hachihachi.pythonanywhere.com/v2";

  static Future<void> init() async {
    instance = await SharedPreferences.getInstance();
    String _savedAPI = instance.getString("sourceAPI");
    if (_savedAPI != null) {
      _sourceAPI = _savedAPI;
    }
  }

  //Custom API Source
  static String get sourceAPI => _sourceAPI;

  static Future<bool> setSourceAPI(String _api) async =>
      await instance.setString("sourceAPI", _api);

  //is an account saved locally
  static bool get isSavedAccount => instance.getBool("savedLogin") ?? false;

  static Future<bool> setSavedAccount(bool _isSaved) async =>
      await instance.setBool("savedLogin", _isSaved);

  static int get timeOut => instance.getInt("timeOut");

  static Future<bool> setTimeOut(int _timeOut) async =>
      await instance.setInt("timeOut", _timeOut);
}
