import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:demozzzzzzz/Data/DataSource.dart';
import 'package:demozzzzzzz/Services/LoginInfo.dart';
import 'package:demozzzzzzz/Services/ServiceError.dart';
import 'package:demozzzzzzz/Services/SharePref.dart';
import 'package:http/http.dart';

class LoginService {
  String _loginAPI = SharePref.sourceAPI + "/login";
  String _registerAPI = SharePref.sourceAPI + "/regis";

  Future<void> saveLogin(String username, String password) async {
    await SharePref.instance.setString("username", username);
    await SharePref.instance.setString("password", password);
    await SharePref.instance.setBool("savedLogin", true);
  }

  static Future<void> deleteLoginSave() async {
    await SharePref.instance.setString("username", null);
    await SharePref.instance.setString("password", null);
    await SharePref.instance.setBool("savedLogin", false);
  }

  Future<LoginInfo> loginWithSavedAcc() async {
    String username = SharePref.instance.getString("username");
    String password = SharePref.instance.getString("password");
    try {
      return login(username, password);
    } on Exception {
      rethrow;
    }
  }

  Future<LoginInfo> login(String username, String password) async {
    if (username == "KMMLocalTest" && password == "123456") {
      await Future.delayed(Duration(milliseconds: 500));
      return LoginInfo(username, DataSource.localTestToken);
    }

    int savedTimeOut = SharePref.timeOut ?? 20;

    try {
      Map<String, String> _body = {
        'name': username,
        'pw': password,
      };

      final Response response = await post(_loginAPI, body: _body).timeout(
        Duration(
          seconds: savedTimeOut,
        ),
      );

      if (response.statusCode == 200) {
        return LoginInfo(username, json.decode(response.body));
      } else {
        throw ErrorStatus(response.statusCode);
      }
    } on TimeoutException {
      throw ErrorStatus(408);
    } on SocketException {
      throw ErrorStatus(6969);
    } on Exception {
      rethrow;
    }
  }

  Future<LoginInfo> register(String username, String password) async {
    try {
      Map<String, String> _body = {
        'name': username,
        'pw': password,
      };

      int savedTimeOut = SharePref.instance.getInt("timeOut") ?? 20;

      final Response response = await post(_registerAPI, body: _body).timeout(
        Duration(
          seconds: savedTimeOut,
        ),
      );
      if (response.statusCode == 200) {
        return LoginInfo(username, json.decode(response.body));
      } else {
        throw ErrorStatus(response.statusCode);
      }
    } on TimeoutException {
      throw ErrorStatus(408);
    } on SocketException {
      throw ErrorStatus(6969);
    } on Exception {
      rethrow;
    }
  }
}
