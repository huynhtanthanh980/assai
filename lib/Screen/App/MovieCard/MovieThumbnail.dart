import 'package:cached_network_image/cached_network_image.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:flutter/material.dart';

class MovieThumbnail extends StatelessWidget {
  final double height;
  final String pictureLink;

  MovieThumbnail({
    @required this.height,
    this.pictureLink,
  });

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.0,
      child: Container(
        height: height,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.grey,
          border: Border.all(
              width: height * 0.02, color: MovieCardColor.thumbnailBorder),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 1.0,
              spreadRadius: 1.0,
              offset: Offset(
                1.0,
                1.0,
              ),
            ),
          ],
        ),
        child: (pictureLink != null)
            ? CachedNetworkImage(
                imageUrl: pictureLink,
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) => CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                    MovieCardColor.mainText,
                  ),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              )
            : Container(),
      ),
    );
  }
}
