class LoginInfo {
  static LoginInfo _instance;

  String _username;
  String _token;
  bool _isSuperUser = false;

  LoginInfo._privateConstructor(String _username, String _token) {
    if (_username.substring(0, 3) == "KMM" && _username.length > 3) {
      _isSuperUser = true;
    }
    this._token = _token;
    this._username = _username;
  }

  factory LoginInfo(String _username, String _token) {
    if (_instance == null) {
      _instance = LoginInfo._privateConstructor(_username, _token);
    }
    return _instance;
  }

  factory LoginInfo.getInstance() {
    return _instance;
  }

  static void dispose() {
    _instance = null;
  }

  bool get isSuperUser => _isSuperUser;

  String get getUsername => _username;

  String get getToken => _token;
}
