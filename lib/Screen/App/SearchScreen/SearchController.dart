import 'dart:async';
import 'dart:math';

import 'package:async/async.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/FilterSearchModel.dart';
import 'package:demozzzzzzz/Model/FilterTag.dart';
import 'package:demozzzzzzz/Model/HotResultModel.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Screen/App/FavWatchController.dart';
import 'package:demozzzzzzz/Screen/App/HomeScreen/HomeScreenController.dart';
import 'package:demozzzzzzz/Services/SearchService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchController {
  static SearchController _instance;

  SearchService _searchService;

  List<HotResultModel> _hotResultList;
  List<FilterTag> _tagList;
  List<FilterTag> _removedList = List<FilterTag>();
  List<FilterTag> _addedList = List<FilterTag>();

  //0: null, 1: finish loading, 2: load search/filter, 3: load browse all
  int _searchStatus = 0;

  String _searchInput;
  List<MovieModel> _searchResult;

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  GlobalKey<ScaffoldState> get getKey => _scaffoldKey;

  CancelableOperation<List<MovieModel>> _completer;

  //Constructor/////////////////////////////////////////////////////////////////

  SearchController._privateConstructor(
    String token,
  ) {
    this._searchService = SearchService(token);
  }

  factory SearchController(String token) {
    if (_instance == null) {
      _instance = SearchController._privateConstructor(token);
    }
    return _instance;
  }

  factory SearchController.getInstance() {
    return _instance;
  }

  static void dispose() {
    _instance = null;
  }

  //Controller//////////////////////////////////////////////////////////////////

  Future<void> initNameList() async {
    try {
      _hotResultList = await _searchService.getNameList();
    } on Exception {
      rethrow;
    }
  }

  Future<void> initTagList() async {
    try {
      _tagList = await _searchService.getTagList();
      _tagList.sort((tag1, tag2) {
        return tag1.tag.compareTo(tag2.tag);
      });
    } on Exception {
      rethrow;
    }
  }

  List<FilterTag> get getTagList => _tagList;

  List<MovieModel> get getSearchResult => _searchResult;

  String get getSearchHistory {
    if (_searchInput == null) {
      if (_lastSearchType == 2) {
        String result = "";
        if (_addedList.length != 0) {
          result += "Tag:";
          for (FilterTag filterTag in _addedList) {
            result += filterTag.tag + ",";
          }
          return result;
        } else if (_removedList.length != 0) {
          result += "Remove:";
          for (FilterTag filterTag in _removedList) {
            result += filterTag.tag + ",";
          }
          return result;
        }
        return "Input here";
      } else if (_lastSearchType == 3) {
        return "Browse all";
      } else {
        return "Input here";
      }
    }
    return _searchInput;
  }

  set searchHistory(String input) {
    _searchInput = input;
  }

  void openDrawer() {
    _scaffoldKey.currentState.openEndDrawer();
  }

  void closeDrawer() {
    if (_scaffoldKey.currentState.isEndDrawerOpen) {
      Navigator.of(_scaffoldKey.currentState.context).pop();
    }
  }

  void resetFilter() {
    _searchStatus = 0;
    _searchResult = null;
    _searchInput = null;
    if (_removedList.isNotEmpty) {
      for (FilterTag tag in _removedList) {
        tag.filterStatus = 0;
      }
      _removedList = List<FilterTag>();
    }
    if (_addedList.isNotEmpty) {
      for (FilterTag tag in _addedList) {
        tag.filterStatus = 0;
      }
      _addedList = List<FilterTag>();
    }
  }

  String getRandomName() {
    if (_hotResultList.length == 0) {
      return null;
    }
    Random random = Random();
    return _hotResultList[random.nextInt(_hotResultList.length)].name;
  }

  List<HotResultModel> quickSearch(String input) {
    return _hotResultList
        .where(
          (movieResult) =>
      movieResult.name.contains(input.toLowerCase()) ||
          movieResult.transName.contains(input.toLowerCase()),
    )
        .toList();
  }

  void onFilterTagPressed(int index) {
    int status = _tagList[index].toggleTag();
    if (status == 0) {
      _setDefaultFilterTag(_tagList[index]);
    } else if (status == 1) {
      _addFilterTag(_tagList[index]);
    } else if (status == -1) {
      _removeFilterTag(_tagList[index]);
    }
  }

  void _addFilterTag(FilterTag filterTag) {
    if (_removedList.contains(filterTag)) {
      _removedList.remove(filterTag);
    }
    _addedList.add(filterTag);
  }

  void _removeFilterTag(FilterTag filterTag) {
    if (_addedList.contains(filterTag)) {
      _addedList.remove(filterTag);
    }
    _removedList.add(filterTag);
  }

  void _setDefaultFilterTag(FilterTag filterTag) {
    if (_addedList.contains(filterTag)) {
      _addedList.remove(filterTag);
    }
    if (_removedList.contains(filterTag)) {
      _removedList.remove(filterTag);
    }
  }

  Color getTagColor(int status) {
    switch (status) {
      case -1:
        return SearchScreenColor.removedTag;
      case 0:
        return SearchScreenColor.defaultTag;
      case 1:
        return SearchScreenColor.addedTag;
      default:
        return SearchScreenColor.defaultTag;
    }
  }

  void onLogOutError() {
    HomeScreenController().logOut(true);
  }

  void reloadOtherList() {
    FavWatchController.getInstance().reload();
  }

  //Search//////////////////////////////////////////////////////////////////////
  //1: no search, 2: search, 3: browse all
  int _lastSearchType = 0;
  int _searchResultCount = 0;

  int get getSearchCount => _searchResultCount;

  int get getLastSearchType => _lastSearchType;

  void reload() {
    _searchStatus = (_searchResult == null) ? 0 : 1;
    _searchResult = null;
  }

  void toggleSearch() {
    _searchResult = null;
    _searchStatus = 2;
    _lastSearchType = 2;
  }

  void toggleBrowseAll() {
    resetFilter();
    _searchResult = null;
    _searchStatus = 3;
    _lastSearchType = 3;
    _searchInput = null;
  }

  bool isEmptySearch() {
    return _searchStatus == 0;
  }

  Future<List<MovieModel>> getSearchFuture() async {
    _searchResult = [];
    try {
      if (_completer != null) {
        await _completer.cancel();
      }
      switch (_searchStatus) {
        case 1:
          _searchStatus = _lastSearchType;
          return getSearchFuture();
        case 2:
          return _createSearchCompleter();
        case 3:
          return _createBrowsePageCompleter(1);
        default:
          return _defaultSearch();
      }
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> loadMorePage(int index) async {
    _searchResult = [];
    try {
      if (_completer != null) {
        await _completer.cancel();
      }
      return _createBrowsePageCompleter(index);
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> _createSearchCompleter() async {
    try {
      _completer = CancelableOperation.fromFuture(
        Future.value(_search()),
        onCancel: () {
          return [];
        },
      );
      return _completer.value;
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> _search() async {
    try {
      if (_addedList.isNotEmpty || _removedList.isNotEmpty) {
        _searchResult = await _searchWithFilter();
      } else if (_searchInput != null) {
        _searchResult = await _searchWithName();
      } else {
        await Future.delayed(Duration(milliseconds: 200));
        _searchStatus = 0;
        _searchInput = null;
        _searchResultCount = 0;
        return [];
      }
      _searchStatus = 1;
      _searchResultCount = _searchResult.length;
      return _searchResult;
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> _searchWithName() async {
    try {
      List<MovieModel> result =
      await _searchService.searchWithName(_searchInput);
      return result;
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> _searchWithFilter() async {
    try {
      List<String> _addedTag =
      List.generate(_addedList.length, (index) => _addedList[index].tag);
      List<String> _removedTag = List.generate(
          _removedList.length, (index) => _removedList[index].tag);
      FilterSearchModel filterSearchModel = FilterSearchModel(
        name: _searchInput ?? "",
        addedTag: _addedTag,
        removedTag: _removedTag,
      );
      List<MovieModel> result =
      await _searchService.searchFilter(filterSearchModel);
      return result;
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> _createBrowsePageCompleter(int index) async {
    try {
      _completer = CancelableOperation.fromFuture(
        Future.value(_browsePage(index)),
        onCancel: () {
          return [];
        },
      );
      return _completer.value;
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> _browsePage(int index) async {
    try {
      if (index == 1) {
        _searchResultCount = await _searchService.getPageCount();
      }
      _searchResult = await _searchService.browsePage(index);
      _searchStatus = 1;
      return _searchResult;
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> _defaultSearch() async {
    return [];
  }
}