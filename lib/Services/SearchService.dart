import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:demozzzzzzz/Data/DataSource.dart';
import 'package:demozzzzzzz/Model/FilterSearchModel.dart';
import 'package:demozzzzzzz/Model/FilterTag.dart';
import 'package:demozzzzzzz/Model/HotResultModel.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Services/ServiceError.dart';
import 'package:demozzzzzzz/Services/SharePref.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchService {
  String _hotResultTestPath = DataSource.hotResultDemo;
  String _filterTagTestPath = DataSource.tagDemo;
  String _searchNameTestPath = DataSource.searchDemo;
  String _searchFilterTestPath = DataSource.searchFilterDemo;
  String _browseTestPath = DataSource.browseAllDemo;

  String _searchAPI = SharePref.sourceAPI + "/search";
  String _hotResultApi = SharePref.sourceAPI + "/name";
  String _tagApi = SharePref.sourceAPI + "/tags";
  String _browsePageApi = SharePref.sourceAPI + "/page";

  String _token;

  SearchService(String _token) {
    this._token = _token;
  }

  Future<List<HotResultModel>> getNameList() async {
    List<dynamic> result;

    try {
      if (_token == DataSource.localTestToken) {
        result = await _loadLocalList(_hotResultTestPath);
      } else {
        result = await _loadList(_hotResultApi);
      }
      return List<HotResultModel>.generate(
          result.length, (index) => HotResultModel.fromJson(result[index]));
    } on Exception {
      rethrow;
    }
  }

  Future<List<FilterTag>> getTagList() async {
    List<dynamic> result;

    try {
      if (_token == DataSource.localTestToken) {
        result = await _loadLocalList(_filterTagTestPath);
      } else {
        result = await _loadList(_tagApi);
      }
      return List<FilterTag>.generate(
          result.length,
          (index) => FilterTag(
                tag: result[index],
                filterStatus: 0,
              ));
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> searchWithName(String name) async {
    List<dynamic> result;

    try {
      if (_token == DataSource.localTestToken) {
        result = await _loadLocalList(_searchNameTestPath);
      } else {
        result =
            await _loadList(_searchAPI + "?name=" + name.replaceAll(' ', '_'));
      }
      return _convertToList(result);
    } on Exception {
      rethrow;
    }
  }

  Future<int> getPageCount() async {
    try {
      if (_token == DataSource.localTestToken) {
        return 10;
      } else {
        List<dynamic> result = await _loadList(_browsePageApi + "/0");
        return result.first;
      }
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> browsePage(int page) async {
    List<dynamic> result;

    try {
      if (_token == DataSource.localTestToken) {
        result = await _loadLocalList(_browseTestPath);
      } else {
        result = await _loadList(_browsePageApi + "/$page");
      }
      return _convertToList(result);
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> searchFilter(FilterSearchModel model) async {
    List<dynamic> result;

    try {
      if (_token == DataSource.localTestToken) {
        result = await _loadLocalList(_searchFilterTestPath);
      } else {
        String _api =
            _searchAPI + (("?name=" + model.name).replaceAll(' ', '_') ?? "");

        if (model.addedTag.length > 0) {
          _api += "&tags=";
          for (String tag in model.addedTag) {
            _api += tag + ",";
          }
          //remove the last comma
          _api = _api.substring(0, _api.length - 1);
        }

        if (model.removedTag.length > 0) {
          _api += "&not=";
          for (String tag in model.removedTag) {
            _api += tag + ",";
          }
          //remove the last comma
          _api = _api.substring(0, _api.length - 1);
        }

        print(_api);

        result = await _loadList(_api);
      }

      return _convertToList(result);
    } on Exception {
      rethrow;
    }
  }

  Future<List<dynamic>> _loadList(String api) async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    int savedTimeOut = _pref.getInt("timeOut") ?? 20;

    try {
      final Response response = await get(
        api,
        headers: {
          'id': _token,
        },
      ).timeout(
        Duration(
          seconds: savedTimeOut,
        ),
      );
      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        throw ErrorStatus(response.statusCode);
      }
    } on TimeoutException {
      throw ErrorStatus(408);
    } on SocketException {
      throw ErrorStatus(6969);
    } on Exception {
      rethrow;
    }
  }

  Future<List<dynamic>> _loadLocalList(String _filePath) async {
    String data = await rootBundle.loadString(_filePath);
    List<dynamic> jsonFile = json.decode(data);
    return jsonFile;
  }

  List<MovieModel> _convertToList(List<dynamic> json) {
    return json.map((i) => MovieModel.fromJson(i)).toList();
  }
}
