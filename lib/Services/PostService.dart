import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:demozzzzzzz/Data/DataSource.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Services/ServiceError.dart';
import 'package:demozzzzzzz/Services/SharePref.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PostService {
  final String _watchAPI = SharePref.sourceAPI + "/watch";
  final String _favouriteAPI = SharePref.sourceAPI + "/fav";

  String _token;
  static PostService _instance;

  PostService._privateConStructor(String _token) {
    this._token = _token;
  }

  factory PostService(String _token) {
    if (_instance == null) {
      _instance = PostService._privateConStructor(_token);
    }
    return _instance;
  }

  static dispose() {
    _instance = null;
  }

  Future<int> addToWatching(MovieModel movieDetail) async {
    if (_token == DataSource.localTestToken) {
      return Future.delayed(Duration(seconds: 2), () => 0);
    } else {
      Map<String, String> body = {
        'id': _token,
        'title': movieDetail.id.toString(),
        'op': "a",
      };
      return _post(_watchAPI, body);
    }
  }

  Future<int> removeFromWatching(MovieModel movieDetail) async {
    if (_token == DataSource.localTestToken) {
      return Future.delayed(Duration(seconds: 2), () => 0);
    } else {
      Map<String, String> body = {
        'id': _token,
        'title': movieDetail.id.toString(),
        'op': "r",
      };
      return _post(_watchAPI, body);
    }
  }

  Future<int> addToFavourite(MovieModel movieDetail) async {
    if (_token == DataSource.localTestToken) {
      return Future.delayed(Duration(seconds: 2), () => 0);
    } else {
      Map<String, String> body = {
        'id': _token,
        'title': movieDetail.id.toString(),
        'op': "af",
      };
      return _post(_favouriteAPI, body);
    }
  }

  Future<int> removeFromFavourite(MovieModel movieDetail) async {
    if (_token == DataSource.localTestToken) {
      return Future.delayed(Duration(seconds: 2), () => 0);
    } else {
      Map<String, String> body = {
        'id': _token,
        'title': movieDetail.id.toString(),
        'op': "rf",
      };
      return _post(_favouriteAPI, body);
    }
  }

  Future<int> addToNonFavourite(MovieModel movieDetail) async {
    if (_token == DataSource.localTestToken) {
      return Future.delayed(Duration(seconds: 2), () => 0);
    } else {
      Map<String, String> body = {
        'id': _token,
        'title': movieDetail.id.toString(),
        'op': "an",
      };
      return _post(_favouriteAPI, body);
    }
  }

  Future<int> removeFromNonFavourite(MovieModel movieDetail) async {
    if (_token == DataSource.localTestToken) {
      return Future.delayed(Duration(seconds: 2), () => 0);
    } else {
      Map<String, String> body = {
        'id': _token,
        'title': movieDetail.id.toString(),
        'op': "rn",
      };
      return _post(_favouriteAPI, body);
    }
  }

  Future<int> _post(String api, Map<String, String> body) async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    int savedTimeOut = _pref.getInt("timeOut") ?? 20;

    try {
      final Response response = await post(api, body: body).timeout(
        Duration(
          seconds: savedTimeOut,
        ),
      );
      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        throw ErrorStatus(response.statusCode);
      }
    } on TimeoutException {
      throw ErrorStatus(408);
    } on SocketException {
      throw ErrorStatus(6969);
    } on Exception {
      rethrow;
    }
  }
}
