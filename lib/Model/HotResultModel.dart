import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HotResultModel {
  String name;
  String transName;

  HotResultModel({
    @required this.name,
    this.transName,
  });

  factory HotResultModel.fromJson(Map<String, dynamic> json) {
    return HotResultModel(
      name: json['name'].toString().toLowerCase(),
      transName: json['transName'].toString().toLowerCase(),
    );
  }
}
