import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/HotResultModel.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:demozzzzzzz/Screen/App/SearchScreen/SearchController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomSearchBox extends StatefulWidget {
  final SearchController searchController;

  CustomSearchBox(
    this.searchController,
  );

  @override
  State<StatefulWidget> createState() {
    return _CustomSearchBoxState();
  }
}

class _CustomSearchBoxState extends State<CustomSearchBox> {
  final textController = TextEditingController();
  List<HotResultModel> hotResultList = List<HotResultModel>();

  @override
  void initState() {
    super.initState();
    textController.addListener(onTextChange);
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: Container(
        height: ScreenSize.safeBlockVerticalSize * 100,
        width: ScreenSize.safeBlockHorizontalSize * 100,
        child: Stack(
          children: <Widget>[
            Container(
              height: ScreenSize.safeBlockVerticalSize * 100,
              width: ScreenSize.safeBlockHorizontalSize * 100,
              child: MaterialButton(
                padding: EdgeInsets.zero,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                focusColor: Colors.transparent,
                elevation: 0.0,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            Container(
              height: ScreenSize.safeBlockVerticalSize * 100,
              width: ScreenSize.safeBlockHorizontalSize * 100,
              padding: EdgeInsets.only(
                top: ScreenSize.safeBlockVerticalSize * 10,
              ),
              child: Container(
                child: Stack(
                  children: <Widget>[
                    buildHotResultBox(),
                    buildSearchBox(),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildSearchBox() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 6,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      padding: EdgeInsets.symmetric(
          horizontal: ScreenSize.safeBlockHorizontalSize * 3),
      decoration: BoxDecoration(
        color: SearchScreenColor.searchBar,
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: TextFormField(
        controller: textController,
        autofocus: true,
        cursorColor: SearchScreenColor.mainText,
        decoration: new InputDecoration(
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          contentPadding:
              EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
          hintText: widget.searchController.getRandomName(),
          hintStyle: TextStyle(
            color: Colors.white24,
            fontSize: ScreenSize.safeBlockVerticalSize * 3.5,
            fontWeight: FontWeight.w500,
          ),
        ),
        onFieldSubmitted: (text) {
          Navigator.of(context).pop(text);
        },
        style: TextStyle(
          color: SearchScreenColor.mainText,
          fontSize: ScreenSize.safeBlockVerticalSize * 3.5,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }

  Widget buildHotResultBox() {
    return Container(
      width: ScreenSize.safeBlockHorizontalSize * 100,
      padding: EdgeInsets.only(
        top: ScreenSize.safeBlockVerticalSize * 6,
      ),
      constraints: BoxConstraints(
        minHeight: ScreenSize.safeBlockVerticalSize * 6,
        maxHeight: ScreenSize.safeBlockVerticalSize * 50,
      ),
      decoration: BoxDecoration(
        color: SearchScreenColor.searchBox,
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: List<Widget>.generate(
            hotResultList.length,
            (index) {
              return MaterialButton(
                padding: EdgeInsets.zero,
                child: ListTile(
                  title: Text(
                    hotResultList[index].name,
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: ScreenSize.safeBlockVerticalSize * 2.2,
                    ),
                  ),
                  subtitle: Text(
                    hotResultList[index].transName,
                    style: TextStyle(
                      color: Colors.black54,
                      fontSize: ScreenSize.safeBlockVerticalSize * 1.7,
                    ),
                  ),
                ),
                onPressed: () {
                  textController.text = hotResultList[index].name;
                  Navigator.of(context).pop(textController.text);
                },
              );
            },
          ),
        ),
      ),
    );
  }

  void onTextChange() {
    setState(() {
      if (textController.text.isNotEmpty) {
        hotResultList =
            widget.searchController.quickSearch(textController.text);
      } else {
        hotResultList = [];
      }
    });
  }
}
