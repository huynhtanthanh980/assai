import 'package:flutter/material.dart';

class HeaderBar extends StatelessWidget {
  final double height;
  final double width;
  final Color color;

  final String title;
  final Color titleColor;

  final Icon leftButtonIcon;
  final Icon rightButtonIcon;

  final VoidCallback onLeftButtonPressed;
  final VoidCallback onRightButtonPressed;
  final VoidCallback onHeaderPressed;

  HeaderBar({
    @required this.height,
    @required this.width,
    @required this.title,
    this.color = Colors.transparent,
    this.titleColor = Colors.white,
    this.leftButtonIcon,
    this.rightButtonIcon,
    this.onLeftButtonPressed,
    this.onRightButtonPressed,
    this.onHeaderPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      color: color,
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            (leftButtonIcon == null && rightButtonIcon == null)
                ? Container()
                : Container(
                    height: height,
                    width: height,
                    child: (leftButtonIcon == null)
                        ? Container()
                        : IconButton(
                            padding: EdgeInsets.zero,
                            color: titleColor,
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            focusColor: Colors.transparent,
                            icon: leftButtonIcon,
                            onPressed: onLeftButtonPressed,
                          ),
                  ),
            Expanded(
              child: MaterialButton(
                padding: EdgeInsets.zero,
                highlightColor: Colors.transparent,
                focusColor: Colors.transparent,
                splashColor: Colors.transparent,
                child: Center(
                  child: Text(
                    title.toUpperCase(),
                    style: TextStyle(
                      color: titleColor,
                      fontSize: height * 0.4,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                onPressed: onHeaderPressed,
              ),
            ),
            (leftButtonIcon == null && rightButtonIcon == null)
                ? Container()
                : Container(
                    height: height,
                    width: height,
                    child: (rightButtonIcon == null)
                        ? Container()
                        : IconButton(
                            padding: EdgeInsets.zero,
                            color: titleColor,
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            focusColor: Colors.transparent,
                            icon: rightButtonIcon,
                            onPressed: onRightButtonPressed,
                          ),
                  ),
          ],
        ),
      ),
    );
  }
}
