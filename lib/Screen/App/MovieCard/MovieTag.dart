import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:flutter/material.dart';

class MovieTag extends StatelessWidget {
  final double height;
  final double width;
  final String title;

  MovieTag({
    @required this.height,
    @required this.width,
    @required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      constraints: BoxConstraints(
        minWidth: width,
      ),
      padding: EdgeInsets.symmetric(
        vertical: height * 0.1,
        horizontal: width * 0.2,
      ),
      decoration: BoxDecoration(
        color: MovieCardColor.movieTag,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Center(
        child: Text(
          title,
          style: TextStyle(
            fontSize: height * 0.7,
            color: MovieCardColor.mainText,
            fontWeight: FontWeight.w500,
          ),
        ),
        widthFactor: 1.0,
      ),
    );
  }
}
