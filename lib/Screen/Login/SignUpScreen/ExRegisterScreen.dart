import 'package:auto_size_text/auto_size_text.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:demozzzzzzz/Screen/App/HeaderBar.dart';
import 'package:demozzzzzzz/Screen/App/MovieCard/CheckableMovieCard.dart';
import 'package:demozzzzzzz/Screen/App/MovieCard/EmptyMovieCard.dart';
import 'package:demozzzzzzz/Screen/CustomDialog.dart';
import 'package:demozzzzzzz/Screen/PageList/LoadMorePageList.dart';
import 'package:demozzzzzzz/Services/PostService.dart';
import 'package:demozzzzzzz/Services/SearchService.dart';
import 'package:demozzzzzzz/Services/ServiceError.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ExRegisterScreen extends StatefulWidget {
  final VoidCallback onLogOutError;
  SearchService _searchService;
  PostService _postService;

  ExRegisterScreen({String token, this.onLogOutError}) {
    this._searchService = SearchService(token);
    this._postService = PostService(token);
  }

  @override
  State<StatefulWidget> createState() {
    return ExRegisterScreenState();
  }
}

class ExRegisterScreenState extends State<ExRegisterScreen> {
  List<MovieModel> _selectedList = [];
  int _totalMovie;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async {
          return await CustomDialog.showSelectionDialog(
                context,
                "Alert",
                "Do you really want to skip this",
              ) ??
              false;
        },
        child: Scaffold(
          backgroundColor: LoginScreenColor.background,
          body: Container(
            height: ScreenSize.safeBlockVerticalSize * 100,
            width: ScreenSize.safeBlockHorizontalSize * 100,
            child: Column(
              children: <Widget>[
                HeaderBar(
                  height: ScreenSize.safeBlockVerticalSize * 6,
                  width: ScreenSize.safeBlockHorizontalSize * 100,
                  title: "Choose 3 favourite movies",
                  color: WatchingScreenColor.header,
                ),
                Expanded(
                  child: buildBody(),
                ),
                Container(
                  height: ScreenSize.safeBlockVerticalSize * 6,
                  width: ScreenSize.safeBlockHorizontalSize * 100,
                  color: WatchingScreenColor.header,
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: ScreenSize.safeBlockVerticalSize * 6,
                        width: ScreenSize.safeBlockHorizontalSize * 50,
                        child: MaterialButton(
                          padding: EdgeInsets.zero,
                          child: Center(
                            child: AutoSizeText(
                              "Skip",
                              maxLines: 1,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize:
                                    ScreenSize.safeBlockVerticalSize * 2.8,
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      Container(
                        height: ScreenSize.safeBlockVerticalSize * 6,
                        width: ScreenSize.safeBlockHorizontalSize * 50,
                        child: MaterialButton(
                          padding: EdgeInsets.zero,
                          child: Center(
                            child: AutoSizeText(
                              "Done",
                              maxLines: 1,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize:
                                    ScreenSize.safeBlockVerticalSize * 2.8,
                              ),
                            ),
                          ),
                          onPressed: () async {
                            if (_selectedList.length >= 3) {
                              CustomDialog.showLoadingDialog(context);
                              await onFinish();
                              if (CustomDialog.isDialogOn) {
                                Navigator.of(context).pop();
                              }
                              Navigator.of(context).pop();
                            } else {
                              await CustomDialog.showAlertDialog(context,
                                  "Alert", "Please choose at least 3 movies");
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildBody() {
    return Container(
      child: FutureBuilder(
        future: loadMovieList().catchError((error) async {
          if (error.runtimeType == StateError &&
              error.message == "Operation already completed") {
            return [];
          }
          if (error.runtimeType == ErrorStatus) {
            if (error.status == ErrorCode.TimeOut ||
                error.status == ErrorCode.Unavailable) {
              bool isRetry = await CustomDialog.showSelectionDialog(
                  context,
                  "Error " + error.code.toString(),
                  error.toString() + "\nDo you want to retry?") ??
                  false;
              if (isRetry) {
                setState(() {});
              }
            } else {
              await CustomDialog.showAlertDialog(
                  context, "Error " + error.code.toString(), error.toString());
              widget.onLogOutError();
              Navigator.of(context).pop();
            }
          } else {
            await CustomDialog.showAlertDialog(
                context, "Error ", error.toString());
            widget.onLogOutError();
            Navigator.of(context).pop();
          }
          return [];
        }),
        builder: (context, snapshot) {
          if (!snapshot.hasData ||
              snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: Container(
                height: ScreenSize.safeBlockVerticalSize * 9,
                width: ScreenSize.safeBlockVerticalSize * 9,
                child: Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(30.0),
                    child: Container(
                      padding:
                          EdgeInsets.all(ScreenSize.safeBlockVerticalSize * 2),
                      color: DialogColor.lightBackground,
                      child: Image.asset(
                        "assets/loading.gif",
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
              ),
            );
          } else {
            return LoadMorePageList(
              initialList: snapshot.data,
              totalResult: _totalMovie,
              onLoadMore: (index) {
                return widget._searchService.browsePage(index);
              },
              itemBuild: (context, movieDetail) {
                if (movieDetail == null) {
                  return EmptyMovieCard(
                    height: ScreenSize.safeBlockVerticalSize * 20,
                    width: ScreenSize.safeBlockHorizontalSize * 100,
                  );
                }
                return CheckableMovieCard(
                  height: ScreenSize.safeBlockVerticalSize * 20,
                  width: ScreenSize.safeBlockHorizontalSize * 100,
                  movieDetail: movieDetail,
                  isCardSelected: _selectedList
                          .where((element) => element.id == movieDetail.id)
                          .length !=
                      0,
                  onPressed: () {
                    if (_selectedList
                            .where((element) => element.id == movieDetail.id)
                            .length !=
                        0) {
                      _selectedList.remove(movieDetail);
                    } else {
                      _selectedList.add(movieDetail);
                    }
                  },
                );
              },
            );
          }
        },
      ),
    );
  }

  Future<List<MovieModel>> loadMovieList() async {
    try {
      _totalMovie = await widget._searchService.getPageCount();
      return widget._searchService.browsePage(1);
    } on Exception {
      rethrow;
    }
  }

  Future<void> onFinish() async {
    try {
      for (MovieModel movieDetail in _selectedList) {
        await widget._postService.addToFavourite(movieDetail);
        await widget._postService.addToWatching(movieDetail);
      }
    } catch (exception) {
      bool isRetry = await CustomDialog.showSelectionDialog(
        context,
            "Error",
            exception.toString() + "\nTry again?",
          ) ??
          false;
      if (isRetry) {
        return await onFinish();
      }
    }
  }
}
