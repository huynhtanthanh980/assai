import 'package:shared_preferences/shared_preferences.dart';

enum ErrorCode {
  Ok,
  Unavailable,
  TimeOut,
  BadRequest,
  Unauthorized,
  NotAllowed,
  Conflict,
  ServerInternalError,
  ServiceUnavailable,
  Unknown,
}

class ErrorStatus implements Exception {
  static bool isASSAIChanCode;

  int code;
  ErrorCode status;

  ErrorStatus(int errorCode) {
    code = errorCode;
    this.status = getError(errorCode);
  }

  static initError() async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    isASSAIChanCode = _pref.getBool("ASSAIChan") ?? true;
  }

  static setASSAIChanCode(bool value) async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    isASSAIChanCode = value;
    await _pref.setBool("ASSAIChan", value);
    print(isASSAIChanCode);
  }

  ErrorCode getError(int errorCode) {
    switch (errorCode) {
      case 200:
        return ErrorCode.Ok;
      case 400:
        return ErrorCode.BadRequest;
      case 401:
        return ErrorCode.Unauthorized;
      case 405:
        return ErrorCode.NotAllowed;
      case 408:
        return ErrorCode.TimeOut;
      case 409:
        return ErrorCode.Conflict;
      case 500:
        return ErrorCode.ServerInternalError;
      case 503:
        return ErrorCode.ServiceUnavailable;
      case 6969:
        return ErrorCode.Unavailable;
      default:
        return ErrorCode.Unknown;
    }
  }

  String toString() {
    if (isASSAIChanCode) {
      switch (status) {
        case ErrorCode.Ok:
          return "Everything is OK, Onii-chan";
        case ErrorCode.TimeOut:
          return "Request timed out, Onii-chan";
        case ErrorCode.BadRequest:
          return "This is unacceptable, Onii-chan";
        case ErrorCode.Unauthorized:
          return "Please try login again, Onii-chan";
        case ErrorCode.NotAllowed:
          return "You cannot do that, Onii-chan";
        case ErrorCode.Conflict:
          return "Please try again, Onii-chan";
        case ErrorCode.ServiceUnavailable:
          return "Service is down, Onii-chan";
        case ErrorCode.ServerInternalError:
          return "ASSAI-chan is out";
        case ErrorCode.Unknown:
          return "Something is wrong, Onii-chan";
        case ErrorCode.Unavailable:
          return "ASSAI-chan cannot connect to the server";
        default:
          return "Something is wrong, Onii-chan";
      }
    } else {
      switch (status) {
        case ErrorCode.Ok:
          return "Service is working properly";
        case ErrorCode.TimeOut:
          return "Request timed out";
        case ErrorCode.BadRequest:
          return "Bad Request";
        case ErrorCode.Unauthorized:
          return "Unauthorized user, please try login again";
        case ErrorCode.NotAllowed:
          return "Not Allowed";
        case ErrorCode.Conflict:
          return "Service conflict";
        case ErrorCode.ServiceUnavailable:
          return "Service is temporarily unavailable";
        case ErrorCode.ServerInternalError:
          return "Internal Server Error";
        case ErrorCode.Unknown:
          return "Unknown error";
        case ErrorCode.Unavailable:
          return "Connection error";
        default:
          return "Unknown error";
      }
    }
  }
}
