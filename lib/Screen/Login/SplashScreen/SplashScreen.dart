import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:demozzzzzzz/Screen/Login/LoginScreen/LoginController.dart';
import 'package:demozzzzzzz/Screen/Login/LoginScreen/LoginScreen.dart';
import 'package:demozzzzzzz/Screen/Login/SplashScreen/LogoScreen.dart';
import 'package:demozzzzzzz/Screen/Login/SplashScreen/SplashSlide.dart';
import 'package:demozzzzzzz/Screen/Login/SplashScreen/SplashSlideForm.dart';
import 'package:demozzzzzzz/Services/LoginService.dart';
import 'package:demozzzzzzz/Services/ServiceError.dart';
import 'package:demozzzzzzz/Services/SharePref.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  CarouselController _carouselController = CarouselController();

  Future<int> init() async {
    //set duration 1 second to make sure logo screen turn on for a while
    await whenNotZero(Stream<double>.periodic(
            Duration(seconds: 1), (x) => MediaQuery.of(context).size.width))
        .then((value) {
      ScreenSize().init(context);
    });

    await SharePref.init();
    await ErrorStatus.initError();

    bool _isLoggedIn = SharePref.isSavedAccount;

    if (_isLoggedIn) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) => LoginScreen(
            loginController: LoginController(LoginService()),
            isLoggedIn: true,
          ),
        ),
      );
    }

    return 1;
  }

  // ignore: missing_return
  Future<int> whenNotZero(Stream<double> source) async {
    await for (double value in source) {
      if (value > 0) {
        return 1;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: init(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return buildBody();
        }
        return LogoScreen();
      },
    );
  }

  Widget buildBody() {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: LoginScreenColor.background,
        body: Container(
          height: ScreenSize.safeBlockVerticalSize * 100,
          width: ScreenSize.safeBlockHorizontalSize * 100,
          child: buildSlider(),
        ),
      ),
    );
  }

  Widget buildSlider() {
    return CarouselSlider(
      items: buildSlideList(),
      carouselController: _carouselController,
      options: CarouselOptions(
        height: ScreenSize.safeBlockVerticalSize * 100,
        aspectRatio: ScreenSize.safeBlockVerticalSize /
            ScreenSize.safeBlockHorizontalSize,
        viewportFraction: 1.0,
        initialPage: 0,
        enableInfiniteScroll: false,
      ),
    );
  }

  List<Widget> buildSlideList() {
    List<SplashSlideForm> list = splashSlideList;

    List<Widget> result = List<Widget>.generate(
      list.length,
      (index) {
        return SplashSlide(
          title: list[index].title,
          titleColor: list[index].titleColor,
          subTitle: list[index].subTitle,
          subTitleColor: list[index].subTitleColor,
          backgroundColor: list[index].backgroundColor,
          picturePath: list[index].picturePath,
          onSlide: (index != list.length - 1)
              ? () => onSlide()
              : () => onLastSlide(),
        );
      },
    );

    return result;
  }

  void onSlide() {
    _carouselController.nextPage(
      duration: Duration(
        milliseconds: 300,
      ),
      curve: Curves.linear,
    );
  }

  void onLastSlide() {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (BuildContext context) =>
          LoginScreen(loginController: LoginController(LoginService()),
            isLoggedIn: false,
          ),
      ),
    );
  }
}
