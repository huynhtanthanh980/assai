import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Screen/App/HomeScreen/HomeScreenController.dart';
import 'package:demozzzzzzz/Screen/CustomDialog.dart';
import 'package:demozzzzzzz/Screen/Login/SplashScreen/LogoScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  final HomeScreenController _homeScreenController = HomeScreenController();

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeScreenController _homeScreenController;

  @override
  initState() {
    super.initState();
    _homeScreenController = widget._homeScreenController;
    _homeScreenController.changeScreen.add(0);
  }

  @override
  void dispose() {
    _homeScreenController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async {
          return await CustomDialog.showSelectionDialog(
            context, "Alert", "Are you sure you want to exit?",) ?? false;
        },
        child: Scaffold(
          key: widget._homeScreenController.getScaffordKey,
          resizeToAvoidBottomPadding: false,
          backgroundColor: HomeScreenColor.background,
          body: StreamBuilder(
            stream: _homeScreenController.getScreenBuildStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return snapshot.data;
              }
              return buildLoadingScreen();
            },
          ),
          floatingActionButton: buildFloatingActionButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
          bottomNavigationBar: buildBottomNavigationBar(),
        ),
      ),
    );
  }

  Widget buildLoadingScreen() {
    //Todo: add new loading screen
    return LogoScreen();
  }

  Widget buildFloatingActionButton() {
    return FloatingActionButton(
      onPressed: () async {
        await Navigator.of(context).push<bool>(
          MaterialPageRoute(
            builder: (context) => _homeScreenController.getTinderScreen,
          ),
        );
        _homeScreenController.changeScreen
            .add(_homeScreenController.getCurrentScreenIndex);
      },
      child: Icon(Icons.add),
      backgroundColor: HomeScreenColor.floatingButton,
    );
  }

  Widget buildBottomNavigationBar() {
    return StreamBuilder(
      stream: _homeScreenController.getCurrentScreenIndexStream,
      builder: (context, snapshot) {
        return BubbleBottomBar(
          opacity: .2,
          currentIndex: (snapshot.hasData) ? snapshot.data : 0,
          onTap: (newValue) {
            _homeScreenController.changeScreen.add(newValue);
          },
          backgroundColor: HomeScreenColor.navigationBarBackground,
          borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
          elevation: 8,
          fabLocation: BubbleBottomBarFabLocation.end,
          //new
          hasNotch: true,
          //new
          hasInk: true,
          //new, gives a cute ink effect
          inkColor: Colors.black12,
          //optional, uses theme color if not specified
          items: <BubbleBottomBarItem>[
            BubbleBottomBarItem(
                backgroundColor: HomeScreenColor.navigationBarButton1,
                icon: Icon(
                  Icons.play_circle_outline,
                  color: HomeScreenColor.navigationBarButtonIconOff,
                ),
                activeIcon: Icon(
                  Icons.play_circle_filled,
                  color: HomeScreenColor.navigationBarButton1,
                ),
                title: Text("Watching")),
            BubbleBottomBarItem(
                backgroundColor: HomeScreenColor.navigationBarButton2,
                icon: Icon(
                  Icons.search,
                  color: HomeScreenColor.navigationBarButtonIconOff,
                ),
                activeIcon: Icon(
                  Icons.search,
                  color: HomeScreenColor.navigationBarButton2,
                ),
                title: Text("Search")),
            BubbleBottomBarItem(
                backgroundColor: HomeScreenColor.navigationBarButton3,
                icon: Icon(
                  Icons.favorite_border,
                  color: HomeScreenColor.navigationBarButtonIconOff,
                ),
                activeIcon: Icon(
                  Icons.favorite,
                  color: HomeScreenColor.navigationBarButton3,
                ),
                title: Text("Favourite")),
            BubbleBottomBarItem(
                backgroundColor: HomeScreenColor.navigationBarButton4,
                icon: Icon(
                  Icons.menu,
                  color: HomeScreenColor.navigationBarButtonIconOff,
                ),
                activeIcon: Icon(
                  Icons.menu,
                  color: HomeScreenColor.navigationBarButton4,
                ),
                title: Text("Menu"))
          ],
        );
      },
    );
  }
}
