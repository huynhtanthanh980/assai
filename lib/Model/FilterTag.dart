class FilterTag {
  final String tag;

  //0: default
  //1: add
  //2: remove
  int filterStatus;

  FilterTag({
    this.tag,
    this.filterStatus,
  });

  int toggleTag() {
    filterStatus = (filterStatus == 1) ? -1 : filterStatus + 1;
    return filterStatus;
  }
}
