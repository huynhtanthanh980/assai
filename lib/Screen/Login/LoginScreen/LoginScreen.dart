import 'package:auto_size_text/auto_size_text.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:demozzzzzzz/Screen/CustomDialog.dart';
import 'package:demozzzzzzz/Screen/Login/LoginScreen/BackGroundSlider.dart';
import 'package:demozzzzzzz/Screen/Login/LoginScreen/LoginController.dart';
import 'package:demozzzzzzz/Screen/Login/LoginScreen/LoginForm.dart';
import 'package:demozzzzzzz/Screen/Login/LoginScreen/LoginOptionForm.dart';
import 'package:demozzzzzzz/Screen/Login/SignUpScreen/SignUpFrom.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  final LoginController loginController;
  final bool isLoggedIn;

  const LoginScreen({
    Key key,
    @required this.loginController,
    @required this.isLoggedIn,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  @override
  void initState() {
    super.initState();
    if (widget.isLoggedIn) {
      widget.loginController.onLoginWithSavedAcc(context);
    } else {
      widget.loginController.changeForm.add(0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async {
          bool _willPop = widget.loginController.onPop();
          if (_willPop) {
            return await CustomDialog.showSelectionDialog(
              context,
              "Alert",
              "Are you sure you want to exit?",
            ) ??
                false;
          }
          return false;
        },
        child: Scaffold(
          backgroundColor: LoginScreenColor.background,
          body: buildBody(),
        ),
      ),
    );
  }

  Widget buildBody() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 100,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      child: Stack(
        children: <Widget>[
          BackGroundSlider(
            onchange: (index) {
              widget.loginController.setNewBackGround = index;
            },
          ),
          Align(
            alignment: Alignment(0.0, 1.0),
            child: buildLoginPad(),
          ),
        ],
      ),
    );
  }

  Widget buildLoginPad() {
    return StreamBuilder<int>(
      stream: widget.loginController.getFormBuildStream,
      builder: (context, snapshot) {
        return AnimatedContainer(
          height: widget.loginController.getPadHeight,
          width: ScreenSize.safeBlockHorizontalSize * 100,
          duration: Duration(milliseconds: 600),
          curve: Curves.ease,
          decoration: BoxDecoration(
            color: LoginScreenColor.loginPad,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              topRight: Radius.circular(20.0),
            ),
          ),
          child: (snapshot.hasData)
              ? getLoginPadBody(snapshot.data)
              : getLoginPadBody(3),
        );
      },
    );
  }

  Widget getLoginPadBody(int index) {
    switch (index) {
      case 0:
        return LoginOptionForm(
          height: widget.loginController.getPadHeight,
          width: ScreenSize.safeBlockHorizontalSize * 100,
          onLoginPressed: () {
            widget.loginController.changeForm.add(1);
          },
          onSignUpPressed: () {
            widget.loginController.changeForm.add(2);
          },
        );
      case 1:
        return LoginForm(
          height: widget.loginController.getPadHeight,
          width: ScreenSize.safeBlockHorizontalSize * 100,
          onSignUpPressed: () async {
            widget.loginController.changeForm.add(2);
          },
          onForgotPassword: () async {
            await CustomDialog.showAlertDialog(context, "Alert", "Coming Soon");
          },
          loginCallBack: (String username, String password) {
            widget.loginController.onLogin(context, username, password);
          },
        );
      case 2:
        return SignUpForm(
          height: widget.loginController.getPadHeight,
          width: ScreenSize.safeBlockHorizontalSize * 100,
          onClosePressed: () async {
            widget.loginController.changeForm.add(1);
          },
          signUpCallBack: (String username, String password) async {
            widget.loginController.onRegister(context, username, password);
          },
        );
      case 3:
        widget.loginController.changeQuote.add(0);
        return Container(
          height: widget.loginController.getPadHeight,
          width: ScreenSize.safeBlockHorizontalSize * 100,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: widget.loginController.getPadHeight * 0.3,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenSize.safeBlockHorizontalSize * 10,
                ),
                color: Colors.white12,
                child: StreamBuilder(
                  stream: widget.loginController.getProgressStream,
                  builder: (context, snapshot) {
                    String _loginStatus = "Logging in";
                    if (snapshot.hasData) {
                      if (snapshot.data == 0.25) {
                        _loginStatus = "Loading movie list";
                      } else if (snapshot.data == 0.5) {
                        _loginStatus = "Loading name list";
                      } else if (snapshot.data == 0.75) {
                        _loginStatus = "Loading tag list";
                      } else if (snapshot.data == 1.0) {
                        _loginStatus = "All Done";
                      }
                    }
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(
                          backgroundColor: LoginScreenColor.background,
                          valueColor: AlwaysStoppedAnimation<Color>(
                              LoginScreenColor.textColor),
                          strokeWidth: 10,
                          value: (snapshot.hasData) ? snapshot.data : null,
                        ),
                        SizedBox(
                          width: ScreenSize.safeBlockHorizontalSize * 7,
                        ),
                        Text(
                          _loginStatus,
                          style: TextStyle(
                            color: LoginScreenColor.textColor,
                            fontSize: widget.loginController.getPadHeight * 0.1,
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
              Container(
                height: widget.loginController.getPadHeight * 0.7,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                padding: EdgeInsets.symmetric(
                  vertical: widget.loginController.getPadHeight * 0.03,
                  horizontal: ScreenSize.safeBlockHorizontalSize * 7,
                ),
                child: StreamBuilder(
                  stream: widget.loginController.getQuoteStream,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Center(
                        child: AutoSizeText(
                          "   " + snapshot.data,
                          maxFontSize: widget.loginController.getPadHeight *
                              0.08,
                          minFontSize: widget.loginController.getPadHeight *
                              0.05,
                          stepGranularity: widget.loginController.getPadHeight *
                              0.01,
                          style: TextStyle(
                            color: Colors.white54,
                            fontSize: widget.loginController.getPadHeight * 0.1,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      );
                    }
                    return Container();
                  },
                ),
              ),
            ],
          ),
        );
      default:
        return Container();
    }
  }
}
