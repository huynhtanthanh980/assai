import 'package:scoped_model/scoped_model.dart';

class SwipeController extends Model {
  int cardSpeed;
  int swipeDelay;
  String nextRemoveCard;
  bool isLike;
  bool _isLock = false;

  SwipeController({this.swipeDelay = 400, this.cardSpeed = 100});

  void swipeCard(String id, bool isRightSwipe) {
    nextRemoveCard = id;
    this.isLike = isRightSwipe;
    notifyListeners();
  }

  bool isSwipeLocked() {
    return _isLock;
  }

  void lockSwipe() {
    _isLock = true;
  }

  void unlockSwipe() {
    _isLock = false;
  }

  void resetNext() {
    nextRemoveCard = null;
  }
}
