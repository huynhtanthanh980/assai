import 'package:auto_size_text/auto_size_text.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef Widget ItemBuild(BuildContext context, int index);

class PageList extends StatefulWidget {
  final List<MovieModel> movieList;
  final ItemBuild itemBuild;

  PageList({
    Key key,
    @required this.movieList,
    @required this.itemBuild,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return PageListState();
  }
}

class PageListState extends State<PageList> {
  int _firstIndex = 1;
  int _listLenght;

  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    _listLenght = widget.movieList.length;
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        (widget.movieList.length > 10) ? buildController() : Container(),
        Expanded(
          child: Container(
            child: buildList(),
          ),
        ),
      ],
    );
  }

  Widget buildController() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 5,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      color: Colors.black12,
      padding: EdgeInsets.symmetric(
        vertical: ScreenSize.safeBlockVerticalSize * 0.5,
        horizontal: ScreenSize.safeBlockHorizontalSize * 10,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4,
            width: ScreenSize.safeBlockVerticalSize * 4,
            child: MaterialButton(
              padding: EdgeInsets.zero,
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: Center(
                child: Icon(
                  Icons.skip_previous,
                  color: Colors.grey,
                  size: ScreenSize.safeBlockVerticalSize * 3.5,
                ),
              ),
              onPressed: onSkipPrevious,
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4,
            width: ScreenSize.safeBlockVerticalSize * 5,
            child: MaterialButton(
              padding: EdgeInsets.zero,
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: Center(
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.grey,
                  size: ScreenSize.safeBlockVerticalSize * 3,
                ),
              ),
              onPressed: onPrevious,
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4,
            width: ScreenSize.safeBlockHorizontalSize * 80 -
                ScreenSize.safeBlockVerticalSize * 23,
            child: Center(
              child: AutoSizeText(
                getPageTag(),
                maxLines: 1,
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: ScreenSize.safeBlockVerticalSize * 2.3,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4,
            width: ScreenSize.safeBlockVerticalSize * 5,
            child: MaterialButton(
              padding: EdgeInsets.zero,
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: Center(
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.grey,
                  size: ScreenSize.safeBlockVerticalSize * 3,
                ),
              ),
              onPressed: onNext,
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4,
            width: ScreenSize.safeBlockVerticalSize * 4,
            child: MaterialButton(
              padding: EdgeInsets.zero,
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: Center(
                child: Icon(
                  Icons.skip_next,
                  color: Colors.grey,
                  size: ScreenSize.safeBlockVerticalSize * 3.5,
                ),
              ),
              onPressed: onSkipNext,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildList() {
    return ListView.builder(
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: (_listLenght >= _firstIndex + 9)
            ? 11
            : _listLenght - _firstIndex + 2,
        itemBuilder: (context, index) {
          int lastIndex = (_listLenght >= _firstIndex + 9)
              ? 10
              : _listLenght - _firstIndex + 1;
          if (index == lastIndex) {
            bool isEnd = _listLenght - _firstIndex <= 9;
            return Container(
              height: ScreenSize.safeBlockVerticalSize * 10,
              width: ScreenSize.safeBlockHorizontalSize * 100,
              child: MaterialButton(
                padding: EdgeInsets.zero,
                highlightColor: Colors.transparent,
                focusColor: Colors.transparent,
                splashColor: Colors.transparent,
                child: Center(
                  child: Text(
                    (isEnd) ? "End of the list" : "Next page",
                    style: TextStyle(
                      color: SearchScreenColor.mainText,
                      fontSize: ScreenSize.safeBlockVerticalSize * 2,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                onPressed: (isEnd) ? null : () => onNext(),
              ),
            );
          }
          return widget.itemBuild(context, _firstIndex + index - 1);
        });
  }

  String getPageTag() {
    if (_firstIndex + 9 > _listLenght) {
      return _firstIndex.toString() +
          " - " +
          _listLenght.toString() +
          "/" +
          _listLenght.toString();
    }
    if (_firstIndex == _listLenght) {
      return "$_firstIndex/$_listLenght";
    }
    return _firstIndex.toString() +
        " - " +
        (_firstIndex + 9).toString() +
        "/" +
        _listLenght.toString();
  }

  void onSkipPrevious() {
    _scrollToTop();
    setState(() {
      _firstIndex = 1;
    });
  }

  void onSkipNext() {
    _scrollToTop();
    setState(() {
      _firstIndex = _listLenght - (_listLenght % 10) + 1;
    });
  }

  void onPrevious() {
    _scrollToTop();
    setState(() {
      if (_firstIndex - 10 >= 1) {
        _firstIndex -= 10;
      }
    });
  }

  void onNext() {
    _scrollToTop();
    setState(() {
      if (_firstIndex + 10 <= _listLenght) {
        _firstIndex += 10;
      }
    });
  }

  void _scrollToTop() {
    _scrollController.animateTo(0,
        duration: Duration(milliseconds: 300), curve: Curves.easeIn);
  }
}
