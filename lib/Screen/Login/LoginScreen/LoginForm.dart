import 'package:auto_size_text/auto_size_text.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef void LoginCallBack(String username, String password);

class LoginForm extends StatefulWidget {
  final double height;
  final double width;
  final LoginCallBack loginCallBack;
  final VoidCallback onSignUpPressed;
  final VoidCallback onForgotPassword;

  LoginForm({
    Key key,
    @required this.height,
    @required this.width,
    @required this.onSignUpPressed,
    @required this.loginCallBack,
    @required this.onForgotPassword,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginFormState();
  }
}

class LoginFormState extends State<LoginForm> {
  String _username;
  String _password;

  bool _isObscureText = true;
  GlobalKey<FormState> _key = GlobalKey<FormState>();

  get height => widget.height;

  get width => widget.width;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _key,
        child: Container(
          height: height,
          width: width,
          padding: EdgeInsets.symmetric(
            vertical: height * 0.02,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: height * 0.2,
                width: width * 0.8,
                padding: EdgeInsets.symmetric(
                  vertical: height * 0.045,
                ),
                child: AutoSizeText(
                  "Welcome back",
                  maxLines: 1,
                  style: TextStyle(
                    color: LoginScreenColor.textColor,
                    fontSize: height * 0.11,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  vertical: height * 0.02,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: Container(
                    height: height * 0.14,
                    width: ScreenSize.safeBlockHorizontalSize * 80,
                    color: LoginScreenColor.background,
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: height * 0.14,
                          width: height * 0.14,
                          child: Center(
                            child: Icon(
                              Icons.person,
                              color: Colors.white70,
                            ),
                          ),
                        ),
                        Container(
                          height: height * 0.14,
                          width: width * 0.8 - height * 0.14,
                          color: LoginScreenColor.textColor,
                          child: Center(
                            child: TextFormField(
                              cursorColor: Colors.white,
                              decoration: new InputDecoration(
                                hintText: "Username",
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: height * 0.03,
                                    horizontal: width * 0.03),
                              ),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: height * 0.06,
                                fontWeight: FontWeight.w500,
                              ),
                              onSaved: (String result) {
                                _username = result;
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  vertical: height * 0.02,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: Container(
                    height: height * 0.14,
                    width: ScreenSize.safeBlockHorizontalSize * 80,
                    color: LoginScreenColor.background,
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: height * 0.14,
                          width: height * 0.14,
                          child: Center(
                            child: Icon(
                              Icons.vpn_key,
                              color: Colors.white70,
                            ),
                          ),
                        ),
                        Container(
                          height: height * 0.14,
                          width: width * 0.8 - height * 0.14,
                          color: LoginScreenColor.textColor,
                          child: Center(
                            child: TextFormField(
                              cursorColor: Colors.white,
                              decoration: new InputDecoration(
                                hintText: "Password",
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: height * 0.03,
                                    horizontal: width * 0.03),
                              ),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: height * 0.06,
                                fontWeight: FontWeight.w500,
                              ),
                              obscureText: _isObscureText,
                              onSaved: (String result) {
                                _password = result;
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                height: height * 0.06,
                width: width * 0.8,
                alignment: Alignment(
                  1.0,
                  0.0,
                ),
                child: MaterialButton(
                  padding: EdgeInsets.zero,
                  highlightColor: Colors.transparent,
                  focusColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  child: Text(
                    (_isObscureText) ? "Show password" : "Hide password",
                    style: TextStyle(
                      color: Colors.white60,
                      fontSize: height * 0.05,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      _isObscureText = !_isObscureText;
                    });
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  vertical: height * 0.05,
                ),
                child: Container(
                  height: height * 0.12,
                  width: width * 0.4,
                  child: Center(
                    child: MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      color: LoginScreenColor.textColor,
                      padding: EdgeInsets.zero,
                      highlightColor: Colors.transparent,
                      focusColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      child: Center(
                        child: Text(
                          "Login",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: height * 0.06,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      onPressed: () {
                        _key.currentState.save();
                        widget.loginCallBack(_username, _password);
                      },
                    ),
                  ),
                ),
              ),
              Container(
                height: height * 0.1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: height * 0.1,
                      child: Center(
                        child: MaterialButton(
                          padding: EdgeInsets.zero,
                          highlightColor: Colors.transparent,
                          focusColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Center(
                            child: Text(
                              "Don't have an account",
                              style: TextStyle(
                                color: Colors.white38,
                                fontSize: height * 0.05,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          onPressed: widget.onSignUpPressed,
                        ),
                      ),
                    ),
                    Container(
                      height: height * 0.1,
                      child: Center(
                        child: MaterialButton(
                          padding: EdgeInsets.zero,
                          highlightColor: Colors.transparent,
                          focusColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Center(
                            child: Text(
                              "Forgot password",
                              style: TextStyle(
                                color: Colors.white38,
                                fontSize: height * 0.05,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          onPressed: widget.onForgotPassword,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
