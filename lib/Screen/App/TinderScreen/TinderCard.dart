import 'package:auto_size_text/auto_size_text.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Screen/App/MovieCard/MovieTag.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TinderCard extends StatelessWidget {
  final double height;
  final double width;
  final MovieModel movieDetail;
  final int likeStatus; //0 neutral, 1 like, -1 dislike
  final VoidCallback onPressed;

  TinderCard({
    @required this.height,
    @required this.width,
    @required this.movieDetail,
    @required this.likeStatus,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    if (movieDetail == null) {
      return _buildEmptyBody();
    } else {
      return _buildBody(context);
    }
  }

  Widget _buildEmptyBody() {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: TinderScreenColor.emptyCardColor,
      ),
      child: Center(
        child: Container(
          height: height * 0.1,
          width: height * 0.1,
          child: Center(
            child: Image.asset(
              "assets/loading.gif",
              fit: BoxFit.contain,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(15.0),
      child: Container(
        height: height,
        width: width,
        color: Colors.grey,
        child: MaterialButton(
          padding: EdgeInsets.zero,
          child: Stack(
            children: <Widget>[
              _buildCard(),
              _buildLikeBox(),
              _buildNopeBox(),
            ],
          ),
          onPressed: onPressed,
        ),
      ),
    );
  }

  Widget _buildCard() {
    return Container(
      height: height,
      width: width,
      child: Stack(
        children: <Widget>[
          Hero(
            tag: movieDetail.id.toString() + "Thumbnail",
            child: Material(
              type: MaterialType.transparency,
              child: Stack(
                children: <Widget>[
                  Container(
                    height: height,
                    width: width,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(movieDetail.pictureLink),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
                    height: height,
                    width: width,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [
                          (likeStatus == 0) ? 0.6 : 0.0,
                          1.0,
                        ],
                        colors: [Colors.transparent, Colors.grey],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: height,
            width: width,
            padding: EdgeInsets.symmetric(
              vertical: height * 0.03,
              horizontal: width * 0.05,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        constraints: BoxConstraints(
                          maxWidth: width * 0.75,
                        ),
                        child: Hero(
                          tag: movieDetail.id.toString() + "Name",
                          child: Material(
                            type: MaterialType.transparency,
                            child: Center(
                              child: AutoSizeText(
                                movieDetail.name,
                                maxLines: 3,
                                style: TextStyle(
                                  fontSize: height * 0.05,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white,
                                ),
                                overflow: TextOverflow.visible,
                              ),
                              widthFactor: 1.0,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: width * 0.15,
                        child: Center(
                          child: Container(
                            height: width * 0.1,
                            width: width * 0.1,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              //Todo: fix score color
                              color: Colors.greenAccent,
                            ),
                            child: Center(
                              child: Text(
                                (movieDetail.compatibleScore.round())
                                    .toString(),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: height * 0.03,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: height * 0.03,
                ),
                Container(
                  width: width,
                  constraints: BoxConstraints(maxHeight: height * 0.15),
                  child: Hero(
                    tag: movieDetail.id.toString() + "Tag",
                    child: Material(
                      type: MaterialType.transparency,
                      child: Wrap(
                        spacing: width * 0.015,
                        runSpacing: width * 0.015,
                        children: List<Widget>.generate(
                          movieDetail.tags.length,
                              (i) {
                            return MovieTag(
                              height: height * 0.04,
                              width: width * 0.13,
                              title: movieDetail.tags[i],
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLikeBox() {
    return (likeStatus == 1)
        ? Align(
      alignment: Alignment(-0.8, -0.8),
      child: Transform.rotate(
        angle: -0.3,
        child: Container(
          height: height * 0.12,
          width: width * 0.4,
          decoration: BoxDecoration(
            border: Border.all(
              width: 8.0,
              color: Colors.greenAccent,
            ),
          ),
          child: Center(
            child: Text(
              (likeStatus == 1) ? "Like" : "Nope",
              style: TextStyle(
                fontSize: height * 0.07,
                fontWeight: FontWeight.w900,
                color: Colors.greenAccent,
              ),
            ),
          ),
        ),
      ),
    )
        : Container();
  }

  Widget _buildNopeBox() {
    return (likeStatus == -1)
        ? Align(
      alignment: Alignment(0.8, -0.8),
      child: Transform.rotate(
        angle: 0.3,
        child: Container(
          height: height * 0.12,
          width: width * 0.4,
          decoration: BoxDecoration(
            border: Border.all(
              width: 8.0,
              color: Colors.redAccent,
            ),
          ),
          child: Center(
            child: Text(
              (likeStatus == 1) ? "Like" : "Nope",
              style: TextStyle(
                fontSize: height * 0.07,
                fontWeight: FontWeight.w900,
                color: Colors.redAccent,
              ),
            ),
          ),
        ),
      ),
    )
        : Container();
  }
}
