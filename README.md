# ASSAI

**Anime Suggesting Service with Artificial Intelligence**

Report: [Report](https://drive.google.com/file/d/1_jxif89WL_HRMM-ccTeFMx-_ejxY6FUu/view?usp=sharing)

Server: [API](https://github.com/hachivc1999/ASAAI)

![giphy](https://media.giphy.com/media/wf5fJ6E0Se2XlmvJIG/giphy.gif)

# Project Description:

Anime Suggesting System with Artificial Intelligence, or ASSAI is a Tinderbehavior application for animation movies, provides multiple methods for user to fulfill their necessities in watching anime including searching, favorizing, watching and recommendation.

- To use ASSAI, user has to provide an account with username and password.

<img src="/uploads/19f4b929bde7c18fadcaccc72a72d0af/Screenshot_2021-05-02_205903.png" width="250">
<img src="/uploads/73052f2f3cc47e7ef2e8c51c2ca106ed/Screenshot_2021-05-02_210404.png" width="250">
<img src="https://media.giphy.com/media/X2lDvIBegTdE8d1cYE/giphy.gif" width="250">

- When a user accesses a title, system will return the name, translated name, producer, images, description, tags, seasons and numerous other information. Because of copyright issues, this application will not support watching. Instead, user can select a season they favor and direct to movies sites.

<img src="https://media.giphy.com/media/cZujemwB5WIAz2unMP/giphy.gif" width="250">
<img src="https://media.giphy.com/media/rH5TA1w5CCZ2Ht6XRJ/giphy.gif" width="250">

- User can search for a title by entering the search section while giving the name and tags of the title user want to get. ASSAI supports searching with partial word, tags and not-wanted tags at the same time.

<img src="https://media.giphy.com/media/A9M0obkxCCvIxVGjiM/giphy.gif" width="250">
<img src="https://media.giphy.com/media/1TGO7iRyWVP2cHoGEC/giphy.gif" width="250">
<img src="/uploads/6b067503e83c4ab6a5a8f5e8ab736be3/Screenshot_2021-05-02_211305.png" width="250">

- In the recommendation option, user will get suitable titles from the server based on their records. From this point, user can decide to add the title in his or her favorite and watching lists. User can also add this title in the blacklist, expecting it not to appear again.

<img src="https://media.giphy.com/media/e8XOmgkh4SFrMWM51o/giphy.gif" width="250">
<img src="/uploads/980872ae2db43dc4532202ebdddec058/Screenshot_2021-05-02_211510.png" width="250">
<img src="/uploads/37667c8c6371108b798693450e72c362/Screenshot_2021-05-02_211525.png" width="250">

- Favorite and watching lists are available at the main menu. User can decide to add or remove a title any time.
