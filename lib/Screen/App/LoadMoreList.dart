import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef Widget ItemBuild(BuildContext context, int index);

class LoadMoreList extends StatefulWidget {
  final List<MovieModel> movieList;
  final ItemBuild itemBuild;

  LoadMoreList({
    Key key,
    @required this.movieList,
    @required this.itemBuild,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoadMoreListState();
  }
}

class LoadMoreListState extends State<LoadMoreList> {
  ScrollController _scrollController;
  int _limitLength;
  int _defaultLimit;
  List<MovieModel> _movieList;
  bool _loadlock = false;

  @override
  void initState() {
    super.initState();

    _scrollController = ScrollController();
    _movieList = widget.movieList;
    _defaultLimit = (_movieList.length <= 10) ? _movieList.length : 10;
    _limitLength = _defaultLimit;

    _scrollController.addListener(() async {
      final max = _scrollController.position.maxScrollExtent;
      final offset = _scrollController.offset;

      if (offset >= max - ScreenSize.safeBlockVerticalSize * 10 && !_loadlock) {
        _loadlock = true;
        await Future.delayed(Duration(milliseconds: 200));
        bool willReload = onLoadMore();
        if (willReload) {
          setState(() {});
        }
        _loadlock = false;
      }
    });
  }

  void resetListLimit() {
    _limitLength = _defaultLimit;
  }

  bool onLoadMore() {
    if (_limitLength < _movieList.length - 10) {
      _limitLength += 10;
      return true;
    } else if (_limitLength < _movieList.length) {
      _limitLength = _movieList.length;
      return true;
    }
    return false;
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        physics: AlwaysScrollableScrollPhysics(),
        controller: _scrollController,
        itemCount: _limitLength + 1,
        itemBuilder: (context, index) {
          if (index < _limitLength) {
            return widget.itemBuild(context, index);
          }
          return Container(
            height: ScreenSize.safeBlockVerticalSize * 10,
            width: ScreenSize.safeBlockHorizontalSize * 100,
            child: Center(
              child: Text(
                (_limitLength == _movieList.length)
                    ? "End of the list"
                    : "Loading",
                style: TextStyle(
                  fontSize: ScreenSize.safeBlockVerticalSize * 2,
                  color: WatchingScreenColor.loadText,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          );
        });
  }
}
