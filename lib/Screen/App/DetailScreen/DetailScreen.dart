import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:demozzzzzzz/Screen/App/DetailScreen/DetailController.dart';
import 'package:demozzzzzzz/Screen/App/DetailScreen/SeasonCard.dart';
import 'package:demozzzzzzz/Screen/App/MovieCard/MovieTag.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatefulWidget {
  final DetailController controller;

  DetailScreen({
    @required this.controller,
  });

  @override
  State<StatefulWidget> createState() {
    return DetailScreenState();
  }
}

class DetailScreenState extends State<DetailScreen> {
  ScrollController _scrollController = ScrollController();
  bool isInFavouriteList;

  @override
  void initState() {
    super.initState();
    isInFavouriteList = widget.controller.getMovieDetail.compatibleScore >= 0;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: ScreenSize.safeBlockVerticalSize * 100,
          width: ScreenSize.safeBlockHorizontalSize * 100,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: DetailScreenColor.background,
            boxShadow: [
              BoxShadow(
                color: Colors.white24,
                blurRadius: 5.0,
                spreadRadius: 1.0,
                offset: Offset(
                  0.0,
                  0.0,
                ),
              ),
            ],
          ),
          child: Column(
            children: <Widget>[
              NotificationListener<ScrollNotification>(
                onNotification: (notification) {
                  if (notification is OverscrollNotification &&
                      notification.overscroll <
                          -ScreenSize.safeBlockVerticalSize * 2) {
                    Navigator.of(context)
                        .pop(widget.controller.willReloadAfterPop ? 1 : 0);
                  }
                  return true;
                },
                child: Container(
                  height: ScreenSize.safeBlockVerticalSize * 95,
                  width: ScreenSize.safeBlockHorizontalSize * 100,
                  child: ScrollConfiguration(
                    behavior: new ScrollBehavior()
                      ..buildViewportChrome(context, null, AxisDirection.down),
                    child: CustomScrollView(
                      controller: _scrollController,
                      slivers: <Widget>[
                        buildAppBar(),
                        buildBody(),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                height: ScreenSize.safeBlockVerticalSize * 5,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                child: buildBottomButton(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildAppBar() {
    return SliverAppBar(
      expandedHeight: ScreenSize.safeBlockHorizontalSize * 100,
      floating: false,
      pinned: true,
      backgroundColor: DetailScreenColor.appBar,
      elevation: 5.0,
      leading: Container(
        child: IconButton(
          padding: EdgeInsets.zero,
          icon: Icon(
            Icons.arrow_back,
            color: DetailScreenColor.mainText,
            size: ScreenSize.safeBlockVerticalSize * 4,
          ),
          onPressed: () {
            Navigator.of(context)
                .pop(widget.controller.willReloadAfterPop ? 1 : 0);
          },
        ),
      ),
      flexibleSpace: FlexibleSpaceBar(
        title: Container(
          child: Hero(
            tag: widget.controller.getMovieDetail.id.toString() + "Name",
            child: Material(
              type: MaterialType.transparency,
              child: AutoSizeText(
                widget.controller.getMovieDetail.name,
                maxLines: 3,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: ScreenSize.safeBlockVerticalSize * 2.5,
                ),
                overflow: TextOverflow.visible,
              ),
            ),
          ),
        ),
        background: Hero(
          tag: widget.controller.getMovieDetail.id.toString() + "Thumbnail",
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                color: DetailScreenColor.background,
                child: CachedNetworkImage(
                  imageUrl: widget.controller.getMovieDetail.pictureLink,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  placeholder: (context, url) => Center(
                    child: Container(
                      height: ScreenSize.safeBlockVerticalSize * 10,
                      width: ScreenSize.safeBlockVerticalSize * 10,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/loading.gif'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(
                    Icons.error,
                    color: Colors.white,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [
                      0.5,
                      1.0,
                    ],
                    colors: [Colors.transparent, Colors.black87],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildBody() {
    return SliverToBoxAdapter(
      child: Container(
        width: ScreenSize.safeBlockHorizontalSize * 100,
        color: DetailScreenColor.background,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            (widget.controller.getMovieDetail.compatibleScore >= 0)
                ? buildCompatibilityScore()
                : Container(),
            buildTag(),
            buildSeasonList(),
            buildDescription(),
            buildDetail("Producer", widget.controller.getMovieDetail.producer),
            (widget.controller.getFavouriteStatus != 2)
                ? buildRemoveButton()
                : Container(),
          ],
        ),),
    );
  }

  Widget buildBottomButton() {
    return Container(
      decoration: BoxDecoration(
        color: DetailScreenColor.background,
        boxShadow: [
          BoxShadow(
            color: DetailScreenColor.buttonBoxShadow,
            blurRadius: 3.0,
            spreadRadius: 3.0,
            offset: Offset(
              0.0,
              -3.0,
            ),
          ),
        ],
      ),
      child: MaterialButton(
        elevation: 0.0,
        color: Colors.greenAccent,
        child: Center(
          child: Text(
            widget.controller.getMainButtonTitle(),
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: ScreenSize.safeBlockVerticalSize * 2.4,
            ),
          ),
        ),
        onPressed: () async {
          await widget.controller.onMainButtonPressed(context);
          setState(() {});
        },
      ),
    );
  }

  Widget buildDetail(String title, String subTitle) {
    return Container(
      width: ScreenSize.safeBlockHorizontalSize * 100,
      padding: EdgeInsets.symmetric(
        vertical: ScreenSize.safeBlockVerticalSize * 1.5,
        horizontal: ScreenSize.safeBlockHorizontalSize * 5,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title + ": ",
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: ScreenSize.safeBlockVerticalSize * 2.6,
              color: DetailScreenColor.mainText,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              top: ScreenSize.safeBlockVerticalSize * 1,
              left: ScreenSize.safeBlockHorizontalSize * 5,
            ),
            child: Text(
              subTitle,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: ScreenSize.safeBlockVerticalSize * 2.4,
                color: DetailScreenColor.subText,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildTag() {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: ScreenSize.safeBlockVerticalSize * 1.5,
        horizontal: ScreenSize.safeBlockHorizontalSize * 5,
      ),
      child: Column(
        children: <Widget>[
          Container(
            width: ScreenSize.safeBlockHorizontalSize * 90,
            child: Text(
              "Tags",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: ScreenSize.safeBlockVerticalSize * 2.6,
                color: DetailScreenColor.mainText,
              ),
            ),
          ),
          Container(
            width: ScreenSize.safeBlockHorizontalSize * 90,
            padding: EdgeInsets.symmetric(
              vertical: ScreenSize.safeBlockVerticalSize * 1,
            ),
            child: Hero(
              tag: widget.controller.getMovieDetail.id.toString() + "Tag",
              child: Material(
                type: MaterialType.transparency,
                child: Wrap(
                  spacing: ScreenSize.safeBlockHorizontalSize * 1,
                  runSpacing: ScreenSize.safeBlockVerticalSize * 1,
                  children: List<Widget>.generate(
                    widget.controller.getMovieDetail.tags.length,
                        (i) {
                      return MovieTag(
                        height: ScreenSize.safeBlockVerticalSize * 3.1,
                        width: ScreenSize.safeBlockHorizontalSize * 20,
                        title: widget.controller.getMovieDetail.tags[i],
                      );
                    },
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildCompatibilityScore() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 18,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      padding: EdgeInsets.symmetric(
        vertical: ScreenSize.safeBlockVerticalSize * 2,
        horizontal: ScreenSize.safeBlockHorizontalSize * 5,
      ),
      child: Stack(
        children: <Widget>[
          Container(
            height: ScreenSize.safeBlockVerticalSize * 14,
            width: ScreenSize.safeBlockHorizontalSize * 90,
            padding: EdgeInsets.only(
              top: ScreenSize.safeBlockVerticalSize * 0.5,
              bottom: ScreenSize.safeBlockVerticalSize * 0.5,
              left: ScreenSize.safeBlockVerticalSize * 14 +
                  ScreenSize.safeBlockHorizontalSize * 3,
              right: ScreenSize.safeBlockHorizontalSize * 3,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: Colors.white12,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                AutoSizeText(
                  "Latest update:",
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: ScreenSize.safeBlockVerticalSize * 2.2,
                    fontWeight: FontWeight.w400,
                    color: Colors.white54,
                  ),
                ),
                AutoSizeText(
                  "    " +
                      widget.controller.getMovieDetail.seasonForm.last.name,
                  maxLines: 2,
                  style: TextStyle(
                    fontSize: ScreenSize.safeBlockVerticalSize * 2.3,
                    fontWeight: FontWeight.w500,
                    color: DetailScreenColor.mainText,
                  ),
                ),
                AutoSizeText(
                  "     - " + widget.controller.getMovieDetail.seasonForm.last
                      .releaseDate + " -     ",
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: ScreenSize.safeBlockVerticalSize * 2.2,
                    fontWeight: FontWeight.w500,
                    color: Colors.white60,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 14,
            width: ScreenSize.safeBlockVerticalSize * 14,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(width: 5, color: Colors.black54),
              color: Colors.greenAccent,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                AutoSizeText(
                  (widget.controller.getMovieDetail.compatibleScore / 10)
                      .toStringAsFixed(1),
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: ScreenSize.safeBlockVerticalSize * 3.5,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildDescription() {
    return Container(
      padding:
      EdgeInsets.symmetric(
        vertical: ScreenSize.safeBlockVerticalSize * 1.5,
        horizontal: ScreenSize.safeBlockHorizontalSize * 5,
      ),
      child: Column(
        children: <Widget>[
          Container(
            width: ScreenSize.safeBlockHorizontalSize * 90,
            child: Text(
              "Description",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: ScreenSize.safeBlockVerticalSize * 2.6,
                color: DetailScreenColor.mainText,
              ),
            ),
          ),
          Container(
            width: ScreenSize.safeBlockHorizontalSize * 90,
            padding: EdgeInsets.only(
              top: ScreenSize.safeBlockVerticalSize * 1,
              bottom: ScreenSize.safeBlockVerticalSize * 1,
              left: ScreenSize.safeBlockHorizontalSize * 4,
            ),
            child: Text(
              widget.controller.getMovieDetail.description,
              style: TextStyle(
                fontSize: ScreenSize.safeBlockVerticalSize * 2.3,
                color: DetailScreenColor.subText,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildSeasonList() {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: ScreenSize.safeBlockVerticalSize * 1.5,
        horizontal: ScreenSize.safeBlockHorizontalSize * 5,
      ),
      child: Column(
        children: <Widget>[
          Container(
            width: ScreenSize.safeBlockHorizontalSize * 90,
            child: Text(
              "Seasons",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: ScreenSize.safeBlockVerticalSize * 2.6,
                color: DetailScreenColor.mainText,
              ),
            ),
          ),
          Container(
            width: ScreenSize.safeBlockHorizontalSize * 90,
            padding: EdgeInsets.symmetric(
                vertical: ScreenSize.safeBlockVerticalSize * 2),
            child: Column(
              children: List<Widget>.generate(
                widget.controller.getMovieDetail.seasonForm.length,
                    (i) =>
                    SeasonCard(
                      height: ScreenSize.safeBlockVerticalSize * 10,
                      width: ScreenSize.safeBlockHorizontalSize * 80,
                      seasonDetail: widget.controller.getMovieDetail
                          .seasonForm[i],
                    ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildRemoveButton() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 18,
      width: ScreenSize.safeBlockHorizontalSize * 90,
      padding: EdgeInsets.symmetric(
          vertical: ScreenSize.safeBlockVerticalSize * 0.5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: ScreenSize.safeBlockVerticalSize * 5,
            width: ScreenSize.safeBlockHorizontalSize * 90,
            child: Center(
              child: Text(
                "You don't like this movie ?",
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: ScreenSize.safeBlockVerticalSize * 2.2,
                  color: DetailScreenColor.mainText,
                ),
              ),
            ),
          ),
          SizedBox(
            height: ScreenSize.safeBlockVerticalSize * 2,
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 5,
            width: ScreenSize.safeBlockHorizontalSize * 70,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.white24,
                  blurRadius: 2.0,
                  spreadRadius: 0.0,
                  offset: Offset(
                    0.0,
                    2.0,
                  ),
                ),
              ],
            ),
            child: MaterialButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              color: Colors.redAccent,
              child: Text(
                widget.controller.getSubButtonTitle(),
                style: TextStyle(
                  fontSize: ScreenSize.safeBlockVerticalSize * 2.2,
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () async {
                await widget.controller.onSubButtonPressed(context);
                setState(() {});
              },
            ),
          ),
          SizedBox(
            height: ScreenSize.safeBlockVerticalSize * 5,
          ),
        ],
      ),
    );
  }
}
