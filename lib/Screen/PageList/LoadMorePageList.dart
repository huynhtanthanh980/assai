import 'package:auto_size_text/auto_size_text.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef Widget LoadMoreItemBuild(BuildContext context, MovieModel movieModel);

typedef Future<List<MovieModel>> PageLoadMoreCallBack(int pageIndex);

class LoadMorePageList extends StatefulWidget {
  final List<MovieModel> initialList;
  final LoadMoreItemBuild itemBuild;
  final int totalResult;
  final PageLoadMoreCallBack onLoadMore;

  LoadMorePageList({
    Key key,
    @required this.initialList,
    @required this.itemBuild,
    @required this.totalResult,
    @required this.onLoadMore,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoadMorePageListState();
  }
}

class LoadMorePageListState extends State<LoadMorePageList> {
  int _pageIndex = 1;
  int _lastPage;
  List<MovieModel> _movieList;

  @override
  initState() {
    super.initState();
    _movieList = widget.initialList;
    _lastPage = (widget.totalResult / 10.0).ceil();
  }

  ScrollController _scrollController = ScrollController();

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        (widget.totalResult > 10) ? buildController() : Container(),
        Expanded(
          child: Container(
            child: (_movieList == null)
                ? buildEmptyList()
                : buildList(_movieList, _pageIndex == _lastPage),
          ),
        ),
      ],
    );
  }

  Widget buildController() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 5,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      color: Colors.black12,
      padding: EdgeInsets.symmetric(
        vertical: ScreenSize.safeBlockVerticalSize * 0.5,
        horizontal: ScreenSize.safeBlockHorizontalSize * 10,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4,
            width: ScreenSize.safeBlockVerticalSize * 4,
            child: MaterialButton(
              padding: EdgeInsets.zero,
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: Center(
                child: Icon(
                  Icons.skip_previous,
                  color: Colors.grey,
                  size: ScreenSize.safeBlockVerticalSize * 3.5,
                ),
              ),
              onPressed: () async {
                await onSkipPrevious();
              },
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4,
            width: ScreenSize.safeBlockVerticalSize * 5,
            child: MaterialButton(
              padding: EdgeInsets.zero,
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: Center(
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.grey,
                  size: ScreenSize.safeBlockVerticalSize * 3,
                ),
              ),
              onPressed: () async {
                await onPrevious();
              },
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4,
            width: ScreenSize.safeBlockHorizontalSize * 80 -
                ScreenSize.safeBlockVerticalSize * 23,
            child: Center(
              child: AutoSizeText(
                getPageTag(),
                maxLines: 1,
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: ScreenSize.safeBlockVerticalSize * 2.3,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4,
            width: ScreenSize.safeBlockVerticalSize * 5,
            child: MaterialButton(
              padding: EdgeInsets.zero,
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: Center(
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.grey,
                  size: ScreenSize.safeBlockVerticalSize * 3,
                ),
              ),
              onPressed: () async {
                await onNext();
              },
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4,
            width: ScreenSize.safeBlockVerticalSize * 4,
            child: MaterialButton(
              padding: EdgeInsets.zero,
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: Center(
                child: Icon(
                  Icons.skip_next,
                  color: Colors.grey,
                  size: ScreenSize.safeBlockVerticalSize * 3.5,
                ),
              ),
              onPressed: () async {
                await onSkipNext();
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget buildList(List<MovieModel> data, bool isEnd) {
    return ListView.builder(
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: data.length + 1,
        itemBuilder: (context, index) {
          if (index == data.length) {
            return Container(
              height: ScreenSize.safeBlockVerticalSize * 10,
              width: ScreenSize.safeBlockHorizontalSize * 100,
              child: MaterialButton(
                padding: EdgeInsets.zero,
                highlightColor: Colors.transparent,
                focusColor: Colors.transparent,
                splashColor: Colors.transparent,
                child: Center(
                  child: Text(
                    (isEnd) ? "End of the list" : "Next page",
                    style: TextStyle(
                      color: SearchScreenColor.mainText,
                      fontSize: ScreenSize.safeBlockVerticalSize * 2,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                onPressed: (isEnd) ? null : () => onNext(),
              ),
            );
          }
          return widget.itemBuild(context, data[index]);
        });
  }

  Widget buildEmptyList() {
    return ListView.builder(
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: 4,
        itemBuilder: (context, index) {
          return widget.itemBuild(context, null);
        });
  }

  String getPageTag() {
    int _firstIndex = (_pageIndex - 1) * 10 + 1;
    int _listLength = widget.totalResult;
    if (_firstIndex + 9 > _listLength) {
      return _firstIndex.toString() +
          " - " +
          _listLength.toString() +
          "/" +
          _listLength.toString();
    }
    if (_firstIndex == _listLength) {
      return "$_firstIndex/$_listLength";
    }
    return _firstIndex.toString() +
        " - " +
        (_firstIndex + 9).toString() +
        "/" +
        _listLength.toString();
  }

  void onSkipPrevious() {
    if (_pageIndex != 1) {
      _scrollToTop();
      setState(() {
        _pageIndex = 1;
        _movieList = widget.initialList;
      });
    }
  }

  Future<void> onSkipNext() async {
    if (_pageIndex != _lastPage) {
      _scrollToTop();
      setState(() {
        _movieList = null;
      });
      _pageIndex = _lastPage;
      _movieList = await widget.onLoadMore(_pageIndex);
      setState(() {});
    }
  }

  Future<void> onPrevious() async {
    if (_pageIndex == 2) {
      onSkipPrevious();
    }
    if (_pageIndex > 2) {
      _scrollToTop();
      setState(() {
        _movieList = null;
      });
      _pageIndex -= 1;
      _movieList = await widget.onLoadMore(_pageIndex);
      setState(() {});
    }
  }

  Future<void> onNext() async {
    if (_pageIndex <= _lastPage) {
      _scrollToTop();
      setState(() {
        _movieList = null;
      });
      _pageIndex += 1;
      _movieList = await widget.onLoadMore(_pageIndex);
      setState(() {});
    }
  }

  void _scrollToTop() {
    _scrollController.animateTo(0,
        duration: Duration(milliseconds: 300), curve: Curves.easeIn);
  }
}
