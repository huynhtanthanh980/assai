import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Services/ServiceError.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Data/DataSource.dart';

class MovieLoader {
  String _token;
  String _apiLink;

  MovieLoader(
    this._apiLink,
    this._token,
  );

  Future<List<MovieModel>> loadMovie(int timeOut) async {
    try {
      List<dynamic> jsonResponse = (_token == DataSource.localTestToken)
          ? await loadMovieLocal(timeOut)
          : await _loadMovieAPI(timeOut);
      List<MovieModel> result = _convertToList(jsonResponse);
      return result;
    } on Exception {
      rethrow;
    }
  }

  Future<List<dynamic>> loadMovieLocal(int timeOut) async {
    String data = await rootBundle.loadString(_apiLink);
    List<dynamic> jsonFile = json.decode(data);
    return jsonFile;
  }

  Future<List<dynamic>> _loadMovieAPI(int timeOut) async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    int savedTimeOut = _pref.getInt("timeOut") ?? 20;
    timeOut = (timeOut > savedTimeOut) ? timeOut : savedTimeOut;

    try {
      final Response response = await get(
        _apiLink,
        headers: {
          'id': _token,
        },
      ).timeout(
        Duration(
          seconds: timeOut,
        ),
      );
      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        throw ErrorStatus(response.statusCode);
      }
    } on TimeoutException {
      throw ErrorStatus(408);
    } on SocketException {
      throw ErrorStatus(6969);
    } on Exception {
      rethrow;
    }
  }

  List<MovieModel> _convertToList(List<dynamic> json) {
    return json.map((i) => MovieModel.fromJson(i)).toList();
  }
}
