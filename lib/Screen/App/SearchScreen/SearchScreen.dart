import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:demozzzzzzz/Screen/App/DetailScreen/DetailController.dart';
import 'package:demozzzzzzz/Screen/App/DetailScreen/DetailScreen.dart';
import 'package:demozzzzzzz/Screen/App/HeaderBar.dart';
import 'package:demozzzzzzz/Screen/App/MovieCard/EmptyMovieCard.dart';
import 'package:demozzzzzzz/Screen/App/MovieCard/MovieCard.dart';
import 'package:demozzzzzzz/Screen/App/SearchScreen/CustomSearchBox.dart';
import 'package:demozzzzzzz/Screen/App/SearchScreen/FilterDrawer.dart';
import 'package:demozzzzzzz/Screen/App/SearchScreen/SearchController.dart';
import 'package:demozzzzzzz/Screen/CustomDialog.dart';
import 'package:demozzzzzzz/Screen/CustomScrollBehavior.dart';
import 'package:demozzzzzzz/Screen/PageList/LoadMorePageList.dart';
import 'package:demozzzzzzz/Screen/PageList/PageList.dart';
import 'package:demozzzzzzz/Services/ServiceError.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class SearchScreen extends StatefulWidget {
  final SearchController controller = SearchController.getInstance();

  @override
  State<StatefulWidget> createState() {
    return SearchScreenState();
  }
}

class SearchScreenState extends State<SearchScreen>
    with SingleTickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: widget.controller.getKey,
        resizeToAvoidBottomPadding: false,
        backgroundColor: SearchScreenColor.background,
        endDrawer: FilterDrawer(
          height: ScreenSize.safeBlockVerticalSize * 100,
          width: ScreenSize.safeBlockHorizontalSize * 70,
          controller: widget.controller,
          onSearchBarPressed: () {
            showSearchDialog();
          },
          onBrowseAllPressed: () {
            setState(() {
              widget.controller.resetFilter();
              widget.controller.toggleBrowseAll();
            });
          },
          onSearchPressed: () {
            setState(() {
              widget.controller.toggleSearch();
            });
          },
          onResetPressed: () {
            setState(() {});
          },
        ),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              HeaderBar(
                height: ScreenSize.safeBlockVerticalSize * 6,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                title: "search",
                color: WatchingScreenColor.header,
              ),
              Expanded(
                child: buildBody(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildBody() {
    return Container(
      child: Column(
        children: <Widget>[
          buildSearchBar(),
          (widget.controller.isEmptySearch())
              ? buildEmptySearchBody()
              : (widget.controller.getSearchResult == null)
                  ? buildSearchBody()
                  : (widget.controller.getSearchResult.length == 0)
                      ? buildNoResultBody()
                      : buildSearchList(widget.controller.getSearchResult),
        ],
      ),
    );
  }

  Widget buildSearchBar() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 5.5,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      color: SearchScreenColor.searchBar,
      padding: EdgeInsets.symmetric(
        vertical: ScreenSize.safeBlockVerticalSize * 0.75,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(25.0),
            child: Container(
              height: ScreenSize.safeBlockVerticalSize * 4,
              width: ScreenSize.safeBlockHorizontalSize * 85,
              padding: EdgeInsets.symmetric(
                vertical: ScreenSize.safeBlockVerticalSize * 0,
                horizontal: ScreenSize.safeBlockHorizontalSize * 4,
              ),
              color: SearchScreenColor.searchBox,
              child: MaterialButton(
                padding: EdgeInsets.zero,
                highlightColor: Colors.transparent,
                focusColor: Colors.transparent,
                splashColor: Colors.transparent,
                child: Text(
                  widget.controller.getSearchHistory ?? "Input here",
                  style: TextStyle(
                    color: SearchScreenColor.mainText,
                    fontSize: ScreenSize.safeBlockVerticalSize * 2.5,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
                onPressed: () async {
                  await showSearchDialog();
                },
              ),
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4,
            width: ScreenSize.safeBlockHorizontalSize * 10,
            child: IconButton(
              padding: EdgeInsets.zero,
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              splashColor: Colors.transparent,
              icon: Icon(
                Icons.filter_list,
                color: SearchScreenColor.mainText,
              ),
              iconSize: ScreenSize.safeBlockHorizontalSize * 7,
              onPressed: () {
                widget.controller.openDrawer();
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget buildSearchBody() {
    return FutureBuilder<List<MovieModel>>(
      future: widget.controller.getSearchFuture().catchError((error) async {
        if (error.runtimeType == StateError &&
            error.message == "Operation already completed") {
          return [];
        }
        if (error.runtimeType == ErrorStatus) {
          if (error.status == ErrorCode.TimeOut ||
              error.status == ErrorCode.Unavailable) {
            bool isRetry = await CustomDialog.showSelectionDialog(
                    context,
                    "Error " + error.code.toString(),
                    error.toString() + "\nDo you want to retry?") ??
                false;
            if (isRetry) {
              setState(() {
                widget.controller.reload();
              });
            }
          } else {
            await CustomDialog.showAlertDialog(
                context, "Error " + error.code.toString(), error.toString());
            widget.controller.onLogOutError();
          }
        } else {
          await CustomDialog.showAlertDialog(
              context, "Error ", error.toString());
        }
        return [];
      }),
      builder: (context, snapshot) {
        if (!snapshot.hasData ||
            snapshot.connectionState == ConnectionState.waiting) {
          return buildSearchLoading();
        } else if (snapshot.hasData && snapshot.data.length == 0) {
          return buildNoResultBody();
        } else {
          return buildSearchList(snapshot.data);
        }
      },
    );
  }

  Widget buildEmptySearchBody() {
    return Expanded(
      child: MaterialButton(
        padding: EdgeInsets.zero,
        highlightColor: Colors.transparent,
        focusColor: Colors.transparent,
        splashColor: Colors.transparent,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.search,
              size: ScreenSize.safeBlockVerticalSize * 10,
              color: Colors.white12,
            ),
            Text(
              "Tap to search",
              style: TextStyle(
                  fontSize: ScreenSize.safeBlockVerticalSize * 3,
                  color: Colors.white12),
            ),
          ],
        ),
        onPressed: () async {
          await showSearchDialog();
        },
      ),
    );
  }

  Widget buildNoResultBody() {
    return Expanded(
      child: MaterialButton(
        padding: EdgeInsets.zero,
        highlightColor: Colors.transparent,
        focusColor: Colors.transparent,
        splashColor: Colors.transparent,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.report,
              size: ScreenSize.safeBlockVerticalSize * 15,
              color: Colors.white12,
            ),
            Text(
              "No Result!",
              style: TextStyle(
                  fontSize: ScreenSize.safeBlockVerticalSize * 3,
                  color: Colors.white12),
            ),
            Text(
              "Tap to try again",
              style: TextStyle(
                  fontSize: ScreenSize.safeBlockVerticalSize * 3,
                  color: Colors.white12),
            ),
          ],
        ),
        onPressed: () async {
          await showSearchDialog();
        },
      ),
    );
  }

  Widget buildSearchLoading() {
    return Expanded(
      child: ListView.builder(
        itemCount: 4,
        itemBuilder: (context, index) => EmptyMovieCard(
          height: ScreenSize.safeBlockVerticalSize * 20,
          width: ScreenSize.safeBlockHorizontalSize * 100,
        ),
      ),
    );
  }

  Widget buildSearchList(List<MovieModel> movieList) {
    if (widget.controller.getLastSearchType == 2) {
      return Expanded(
        child: ScrollConfiguration(
          behavior: CustomScrollBehavior(),
          child: PageList(
            movieList: movieList,
            itemBuild: (context, index) {
              return MovieCard(
                height: ScreenSize.safeBlockVerticalSize * 20,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                movieDetail: movieList[index],
                onPressed: () async {
                  await onMovieCardPressed(movieList[index]);
                },
              );
            },
          ),
        ),
      );
    } else if (widget.controller.getLastSearchType == 3) {
      return Expanded(
        child: ScrollConfiguration(
          behavior: CustomScrollBehavior(),
          child: LoadMorePageList(
            initialList: movieList,
            totalResult: widget.controller.getSearchCount ?? 0,
            onLoadMore: (index) {
              return widget.controller.loadMorePage(index);
            },
            itemBuild: (context, movieDetail) {
              if (movieDetail == null) {
                return EmptyMovieCard(
                  height: ScreenSize.safeBlockVerticalSize * 20,
                  width: ScreenSize.safeBlockHorizontalSize * 100,
                );
              }
              return MovieCard(
                height: ScreenSize.safeBlockVerticalSize * 20,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                movieDetail: movieDetail,
                onPressed: () async {
                  await onMovieCardPressed(movieDetail);
                },
              );
            },
          ),
        ),
      );
    } else {
      return buildNoResultBody();
    }
  }

  Future<void> showSearchDialog() async {
    String _input = await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return CustomSearchBox(widget.controller);
        });

    if (_input != null && _input.isNotEmpty) {
      setState(() {
        widget.controller.searchHistory = _input;
        widget.controller.toggleSearch();
      });
    }
  }

  Future<void> onMovieCardPressed(MovieModel movieDetail) async {
    int _willReload = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) =>
            DetailScreen(
              controller: DetailController(
                movieDetail,
                null,
              ),
            ),
      ),
    );

    if (_willReload == 1) {
      setState(() {
        widget.controller.reload();
        widget.controller.reloadOtherList();
      });
    }
  }
}