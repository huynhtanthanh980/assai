import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LogoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Container(
          height: double.infinity,
          width: double.infinity,
          child: Center(
            child: Image.asset("assets/Logo.png"),
          ),
        ),
      ),
    );
  }
}
