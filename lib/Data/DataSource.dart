class DataSource {
  static String localTestToken = "LocalTestingToken";
  static String watchDemo = "assets/json/WatchingTest.json";
  static String favouriteDemo = "assets/json/FavouriteTest.json";
  static String recommendDemo = "assets/json/TinderTest.json";
  static String hotResultDemo = "assets/json/QuickNameTest.json";
  static String tagDemo = "assets/json/TagTest.json";
  static String searchDemo = "assets/json/SearchTest.json";
  static String searchFilterDemo = "assets/json/SearchFilterTest.json";
  static String browseAllDemo = "assets/json/SearchBrowseAllTest.json";
}