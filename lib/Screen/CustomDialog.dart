import 'package:auto_size_text/auto_size_text.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomDialog {
  static bool _isDialogOn = false;

  static bool get isDialogOn => _isDialogOn;

  static void showLoadingDialog(BuildContext context) {
    _isDialogOn = true;
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return Dialog(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            child: Container(
              height: ScreenSize.safeBlockVerticalSize * 9,
              width: ScreenSize.safeBlockVerticalSize * 9,
              child: Center(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(30.0),
                  child: Container(
                    padding:
                    EdgeInsets.all(ScreenSize.safeBlockVerticalSize * 2),
                    color: DialogColor.lightBackground,
                    child: Image.asset(
                      "assets/loading.gif",
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ),
            ),
          );
        }).whenComplete(() {
      _isDialogOn = false;
    });
  }

  static Future<bool> showSelectionDialog(
      BuildContext context, String title, String message) async {
    _isDialogOn = true;
    return await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return Dialog(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Container(
                height: ScreenSize.safeBlockVerticalSize * 35,
                width: ScreenSize.safeBlockHorizontalSize * 90,
                color: DialogColor.background,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: ScreenSize.safeBlockVerticalSize * 7,
                      width: ScreenSize.safeBlockHorizontalSize * 90,
                      padding: EdgeInsets.symmetric(
                        vertical: ScreenSize.safeBlockVerticalSize * 0.5,
                        horizontal: ScreenSize.safeBlockHorizontalSize * 2.5,
                      ),
                      child: Center(
                        child: AutoSizeText(
                          title,
                          maxLines: 3,
                          style: TextStyle(
                            fontSize: ScreenSize.safeBlockVerticalSize * 3,
                            color: DialogColor.text,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: ScreenSize.safeBlockVerticalSize * 15,
                      width: ScreenSize.safeBlockHorizontalSize * 85,
                      padding: EdgeInsets.symmetric(
                        vertical: ScreenSize.safeBlockVerticalSize * 0.5,
                        horizontal: ScreenSize.safeBlockHorizontalSize * 2.5,
                      ),
                      color: DialogColor.lightBackground,
                      child: Center(
                        child: Text(
                          message,
                          style: TextStyle(
                            fontSize: ScreenSize.safeBlockVerticalSize * 2.5,
                            color: DialogColor.text,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: ScreenSize.safeBlockVerticalSize * 13,
                      width: ScreenSize.safeBlockHorizontalSize * 80,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          MaterialButton(
                            padding: EdgeInsets.zero,
                            highlightColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            focusColor: Colors.transparent,
                            child: Container(
                              height: ScreenSize.safeBlockVerticalSize * 8,
                              width: ScreenSize.safeBlockVerticalSize * 8,
                              decoration: BoxDecoration(
                                color: DialogColor.background,
                                shape: BoxShape.circle,
                                border: Border.all(
                                    width:
                                    ScreenSize.safeBlockVerticalSize * 0.3,
                                    color: Colors.lightBlueAccent),
                              ),
                              child: Center(
                                child: Container(
                                  height: ScreenSize.safeBlockVerticalSize * 6,
                                  width: ScreenSize.safeBlockVerticalSize * 6,
                                  decoration: BoxDecoration(
                                    color: Colors.lightBlueAccent,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Center(
                                    child: Container(
                                      height:
                                      ScreenSize.safeBlockVerticalSize * 4,
                                      width:
                                      ScreenSize.safeBlockVerticalSize * 4,
                                      decoration: BoxDecoration(
                                        color: DialogColor.background,
                                        shape: BoxShape.circle,
                                      ),
                                      child: Center(
                                        child: Container(
                                          height:
                                          ScreenSize.safeBlockVerticalSize *
                                              2,
                                          width:
                                          ScreenSize.safeBlockVerticalSize *
                                              2,
                                          decoration: BoxDecoration(
                                            color: Colors.lightBlueAccent,
                                            shape: BoxShape.circle,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            onPressed: () {
                              _isDialogOn = false;
                              Navigator.of(context).pop(true);
                            },
                          ),
                          MaterialButton(
                            padding: EdgeInsets.zero,
                            highlightColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            focusColor: Colors.transparent,
                            child: Container(
                              height: ScreenSize.safeBlockVerticalSize * 8,
                              width: ScreenSize.safeBlockVerticalSize * 8,
                              decoration: BoxDecoration(
                                color: DialogColor.background,
                                shape: BoxShape.circle,
                                border: Border.all(
                                    width:
                                    ScreenSize.safeBlockVerticalSize * 0.3,
                                    color: Colors.redAccent),
                              ),
                              child: Center(
                                child: Container(
                                  height: ScreenSize.safeBlockVerticalSize * 6,
                                  width: ScreenSize.safeBlockVerticalSize * 6,
                                  decoration: BoxDecoration(
                                    color: Colors.redAccent,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Center(
                                    child: Icon(
                                      Icons.close,
                                      size:
                                      ScreenSize.safeBlockVerticalSize * 5,
                                      color: DialogColor.background,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            onPressed: () {
                              _isDialogOn = false;
                              Navigator.of(context).pop(false);
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  static Future<void> showAlertDialog(BuildContext context, String title,
      String message) async {
    _isDialogOn = true;
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return Dialog(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Container(
                height: ScreenSize.safeBlockVerticalSize * 35,
                width: ScreenSize.safeBlockHorizontalSize * 90,
                color: DialogColor.background,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: ScreenSize.safeBlockVerticalSize * 7,
                      width: ScreenSize.safeBlockHorizontalSize * 90,
                      padding: EdgeInsets.symmetric(
                        vertical: ScreenSize.safeBlockVerticalSize * 0.5,
                        horizontal: ScreenSize.safeBlockHorizontalSize * 2.5,
                      ),
                      child: Center(
                        child: AutoSizeText(
                          title,
                          maxLines: 3,
                          style: TextStyle(
                            fontSize: ScreenSize.safeBlockVerticalSize * 3,
                            color: DialogColor.text,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: ScreenSize.safeBlockVerticalSize * 15,
                      width: ScreenSize.safeBlockHorizontalSize * 85,
                      padding: EdgeInsets.symmetric(
                        vertical: ScreenSize.safeBlockVerticalSize * 0.5,
                        horizontal: ScreenSize.safeBlockHorizontalSize * 2.5,
                      ),
                      color: DialogColor.lightBackground,
                      child: Center(
                        child: Text(
                          message,
                          style: TextStyle(
                            fontSize: ScreenSize.safeBlockVerticalSize * 2.5,
                            color: DialogColor.text,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: ScreenSize.safeBlockVerticalSize * 13,
                      width: ScreenSize.safeBlockHorizontalSize * 80,
                      child: Center(
                        child: MaterialButton(
                          padding: EdgeInsets.zero,
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          focusColor: Colors.transparent,
                          child: Container(
                            height: ScreenSize.safeBlockVerticalSize * 8,
                            width: ScreenSize.safeBlockVerticalSize * 8,
                            decoration: BoxDecoration(
                              color: DialogColor.background,
                              shape: BoxShape.circle,
                              border: Border.all(
                                  width:
                                  ScreenSize.safeBlockVerticalSize * 0.3,
                                  color: Colors.lightBlueAccent),
                            ),
                            child: Center(
                              child: Container(
                                height: ScreenSize.safeBlockVerticalSize * 6,
                                width: ScreenSize.safeBlockVerticalSize * 6,
                                decoration: BoxDecoration(
                                  color: Colors.lightBlueAccent,
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                  child: Container(
                                    height:
                                    ScreenSize.safeBlockVerticalSize * 4,
                                    width:
                                    ScreenSize.safeBlockVerticalSize * 4,
                                    decoration: BoxDecoration(
                                      color: DialogColor.background,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Center(
                                      child: Container(
                                        height:
                                        ScreenSize.safeBlockVerticalSize *
                                            2,
                                        width:
                                        ScreenSize.safeBlockVerticalSize *
                                            2,
                                        decoration: BoxDecoration(
                                          color: Colors.lightBlueAccent,
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          onPressed: () {
                            _isDialogOn = false;
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  static Future<void> showGiganticAlertDialog(
      BuildContext context, String title, String message) async {
    _isDialogOn = true;
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return Dialog(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            child: ClipRRect(
              child: Container(
                height: ScreenSize.safeBlockVerticalSize * 90,
                width: ScreenSize.safeBlockHorizontalSize * 100,
                color: DialogColor.background,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: ScreenSize.safeBlockVerticalSize * 7,
                      width: ScreenSize.safeBlockHorizontalSize * 100,
                      padding: EdgeInsets.symmetric(
                        vertical: ScreenSize.safeBlockVerticalSize * 0.5,
                        horizontal: ScreenSize.safeBlockHorizontalSize * 2.5,
                      ),
                      child: Center(
                        child: AutoSizeText(
                          title,
                          maxLines: 3,
                          style: TextStyle(
                            fontSize: ScreenSize.safeBlockVerticalSize * 3,
                            color: DialogColor.text,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: ScreenSize.safeBlockVerticalSize * 73,
                      width: ScreenSize.safeBlockHorizontalSize * 100,
                      padding: EdgeInsets.symmetric(
                        vertical: ScreenSize.safeBlockVerticalSize * 0.5,
                        horizontal: ScreenSize.safeBlockHorizontalSize * 2.5,
                      ),
                      color: DialogColor.lightBackground,
                      child: Center(
                        child: SingleChildScrollView(
                          child: Text(
                            message,
                            style: TextStyle(
                              fontSize: ScreenSize.safeBlockVerticalSize * 2.5,
                              color: DialogColor.text,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: ScreenSize.safeBlockVerticalSize * 10,
                      width: ScreenSize.safeBlockHorizontalSize * 80,
                      child: Center(
                        child: MaterialButton(
                          padding: EdgeInsets.zero,
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          focusColor: Colors.transparent,
                          child: Container(
                            height: ScreenSize.safeBlockVerticalSize * 8,
                            width: ScreenSize.safeBlockVerticalSize * 8,
                            decoration: BoxDecoration(
                              color: DialogColor.background,
                              shape: BoxShape.circle,
                              border: Border.all(
                                  width: ScreenSize.safeBlockVerticalSize * 0.3,
                                  color: Colors.lightBlueAccent),
                            ),
                            child: Center(
                              child: Container(
                                height: ScreenSize.safeBlockVerticalSize * 6,
                                width: ScreenSize.safeBlockVerticalSize * 6,
                                decoration: BoxDecoration(
                                  color: Colors.lightBlueAccent,
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                  child: Container(
                                    height:
                                        ScreenSize.safeBlockVerticalSize * 4,
                                    width: ScreenSize.safeBlockVerticalSize * 4,
                                    decoration: BoxDecoration(
                                      color: DialogColor.background,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Center(
                                      child: Container(
                                        height:
                                            ScreenSize.safeBlockVerticalSize *
                                                2,
                                        width:
                                            ScreenSize.safeBlockVerticalSize *
                                                2,
                                        decoration: BoxDecoration(
                                          color: Colors.lightBlueAccent,
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          onPressed: () {
                            _isDialogOn = false;
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
