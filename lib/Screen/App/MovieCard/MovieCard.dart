import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Screen/App/MovieCard/MovieTag.dart';
import 'package:demozzzzzzz/Screen/App/MovieCard/MovieThumbnail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MovieCard extends StatelessWidget {
  final double height;
  final double width;
  final MovieModel movieDetail;
  final VoidCallback onPressed;

  double verticalPadding;
  double horizontalPadding;
  double thumbnailSize;
  double cardHeight;
  double cardWidth;
  double infoButtonWidth;
  double detailBoxWidth;
  double detailBoxLeftShift;

  MovieCard({
    @required this.movieDetail,
    @required this.height,
    @required this.width,
    @required this.onPressed,
  }) {
    verticalPadding = height * 0.085;
    horizontalPadding = width * 0.05;
    thumbnailSize = height * 0.9;
    cardHeight = height * 0.7;
    cardWidth = width * 0.75;
    infoButtonWidth = width * 0.05;

    detailBoxLeftShift =
        cardWidth + thumbnailSize - (width - horizontalPadding) + width * 0.06;
    detailBoxWidth =
        cardWidth - detailBoxLeftShift - infoButtonWidth - width * 0.03;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      padding: EdgeInsets.only(
        top: verticalPadding,
        bottom: verticalPadding,
        left: horizontalPadding,
        right: horizontalPadding,
      ),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment(1.0, -1.0),
            child: buildCard(context),
          ),
          Align(
            alignment: Alignment(-1.0, 1.0),
            child: (movieDetail.compatibleScore >= 0)
                ? buildMovieThumbnailWithScore(context)
                : buildMovieThumbnail(context),
          ),
        ],
      ),
    );
  }

  Widget buildCard(BuildContext context) {
    return Container(
      height: cardHeight,
      width: cardWidth,
      decoration: BoxDecoration(
        color: MovieCardColor.movieCard,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
            color: MovieCardColor.movieCardShadow,
            blurRadius: 0.0,
            spreadRadius: 0.0,
            offset: Offset(
              -8.0,
              8.0,
            ),
          ),
        ],
      ),
      child: Container(
        height: cardHeight,
        width: cardWidth - infoButtonWidth,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: cardHeight,
              width: cardWidth - infoButtonWidth,
              padding: EdgeInsets.only(
                top: height * 0.03,
                bottom: height * 0.03,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: height * 0.15,
                    width: cardWidth - infoButtonWidth,
                    padding: EdgeInsets.only(
                      left: detailBoxLeftShift,
                    ),
                    child: Text(
                      movieDetail.name,
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: height * 0.12,
                        color: MovieCardColor.mainText,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Container(
                    width: cardWidth - infoButtonWidth,
                    padding: EdgeInsets.only(
                      left: detailBoxLeftShift,
                    ),
                    child: Container(
                      height: height * 0.12,
                      width: detailBoxWidth,
                      child: Text(
                        (movieDetail.seasonForm.length > 1)
                            ? movieDetail.seasonForm.length.toString() +
                                " seasons"
                            : "1 season",
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: height * 0.1,
                          color: MovieCardColor.subText,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                  Container(
                    height: height * 0.13,
                    width: cardWidth - infoButtonWidth,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: List.generate(movieDetail.tags.length +
                            1,
                              (index) {
                            if (index == 0) {
                              return Container(
                                height: height * 0.13,
                                width: detailBoxLeftShift,
                              );
                            } else {
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width * 0.007),
                                child: MovieTag(
                                  height: height * 0.13,
                                  width: width * 0.15,
                                  title: movieDetail.tags[index - 1],
                                ),
                              );
                            }
                          },
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: cardHeight,
              width: infoButtonWidth,
              child: MaterialButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                ),
                padding: EdgeInsets.zero,
                elevation: 1.0,
                focusElevation: 0.0,
                highlightElevation: 0.0,
                color: MovieCardColor.infoButton,
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.white,
                  size: width * 0.05,
                ),
                onPressed: onCardPressed,
              ),
            ),
          ],
        ),)
      ,
    );
  }

  Widget buildMovieThumbnailWithScore(BuildContext context) {
    return Hero(
      tag: movieDetail.id.toString() + "Thumbnail",
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          height: thumbnailSize,
          width: thumbnailSize,
          child: MaterialButton(
            elevation: 0.0,
            focusElevation: 0.0,
            padding: EdgeInsets.zero,
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            child: Stack(
              children: <Widget>[
                MovieThumbnail(
                  height: thumbnailSize,
                  pictureLink: movieDetail.pictureLink,
                ),
                Align(
                  alignment: Alignment(0.9, 0.9),
                  child: Container(
                    height: height * 0.25,
                    width: height * 0.25,
                    decoration: BoxDecoration(
                      color: getScoreColor(),
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: Text(
                        (movieDetail.compatibleScore.round()).toString(),
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: height * 0.1,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            onPressed: onCardPressed,
          ),
        ),
      ),
    );
  }

  Widget buildMovieThumbnail(BuildContext context) {
    return Hero(
      tag: movieDetail.id.toString() + "Thumbnail",
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          height: thumbnailSize,
          width: thumbnailSize,
          child: MaterialButton(
            elevation: 0.0,
            focusElevation: 0.0,
            padding: EdgeInsets.zero,
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            child: Stack(
              children: <Widget>[
                MovieThumbnail(
                  height: thumbnailSize,
                  pictureLink: movieDetail.pictureLink,
                ),
                Align(
                  alignment: Alignment(0.9, 0.9),
                  child: Container(
                    height: height * 0.25,
                    width: height * 0.25,
                  ),
                ),
              ],
            ),
            onPressed: onCardPressed,
          ),
        ),
      ),
    );
  }

  Color getScoreColor() {
    double score = movieDetail.compatibleScore;

    if (score <= 30) {
      return Colors.redAccent;
    } else if (score > 30 && score < 70) {
      return Colors.blue;
    } else if (score >= 70) {
      return Colors.green;
    }

    return Colors.black;
  }

  void onCardPressed() {
    onPressed();
  }
}
