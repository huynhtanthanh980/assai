import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Screen/CustomDialog.dart';
import 'package:demozzzzzzz/Services/PostService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetailController {
  final MovieModel _movieDetail;
  final PostService _postController = PostService(null);
  bool _willPopAfterPressed;
  bool willReloadAfterPop = false;
  int _favouriteStatus;

  //-1: tinder
  //0: not in any list
  //1: favourite list
  //2: watching list

  DetailController(
    this._movieDetail,
    int favouriteStatus,
  ) {
    this._favouriteStatus = favouriteStatus ?? _movieDetail.favouriteStatus;
    this._willPopAfterPressed = favouriteStatus != null;
  }

  MovieModel get getMovieDetail => _movieDetail;

  int get getFavouriteStatus => _favouriteStatus;

  //Detail Card/////////////////////////////////////////////////////////////////

  //0: normal
  //1: fav
  //2: nonfav
  //3: fav + watching
  //4: nonfav + watching
  //5: watching screen
  //6: favourite screen

  String getMainButtonTitle() {
    switch (_favouriteStatus) {
      case 0:
        return "Start watching";
      case 1:
        return "Add to watching list";
      case 2:
        return "Add to favourite list";
      case 3:
        return "Finished Watching";
      case 4:
        return "Add to favourite list";
      case 5:
        return "Finished Watching";
      case 6:
        return "Add to watching list";
      default:
        return "Error: status = ${_favouriteStatus.toString()}";
    }
  }

  String getSubButtonTitle() {
    switch (_favouriteStatus) {
      case 0:
        return "Do not recommend it again!";
      case 1:
        return "Remove!";
      case 3:
        return "Remove!";
      case 4:
        return "Do not recommend it again!";
      case 5:
        return "Remove!";
      case 6:
        return "Remove!";
      default:
        return "Error: status = ${_favouriteStatus.toString()}";
    }
  }

  Future<void> onMainButtonPressed(BuildContext context) async {
    CustomDialog.showLoadingDialog(context);

    try {
      switch (_favouriteStatus) {
        case 0:
          _favouriteStatus += await addToFavourite();
          _favouriteStatus += await addToWatching();
          break;
        case 1:
          _favouriteStatus += await addToWatching();
          break;
        case 2:
          _favouriteStatus += await removeFromNonFavourite();
          _favouriteStatus += await addToFavourite();
          break;
        case 3:
          _favouriteStatus += await removeFromWatching();
          break;
        case 4:
          _favouriteStatus += await removeFromNonFavourite();
          _favouriteStatus += await addToFavourite();
          break;
        case 5:
          _favouriteStatus += await removeFromWatching();
          break;
        case 6:
          _favouriteStatus += await addToWatching();
          break;
        default:
          Navigator.of(context).pop();
          return;
      }

      if (CustomDialog.isDialogOn) {
        Navigator.of(context).pop();
      }

      if (_willPopAfterPressed) {
        Navigator.of(context).pop(1);
      } else {
        willReloadAfterPop = true;
      }
    } catch (exception) {
      Navigator.of(context).pop();
      bool isRetry =
          await CustomDialog.showSelectionDialog(
            context,
            "Error",
            exception.toString() + "/nTry again?",
          ) ??
          false;
      if (isRetry) {
        await onMainButtonPressed(context);
      }
    }
  }

  Future<void> onSubButtonPressed(BuildContext context) async {
    CustomDialog.showLoadingDialog(context);

    try {
      switch (_favouriteStatus) {
        case 0:
          _favouriteStatus += await addToNonFavourite();
          Navigator.of(context).pop();
          Navigator.of(context).pop(1);
          return;
        case 1:
          _favouriteStatus += await removeFromFavourite();
          break;
        case 3:
          _favouriteStatus += await removeFromWatching();
          _favouriteStatus += await removeFromFavourite();
          break;
        case 4:
          await removeFromWatching();
          Navigator.of(context).pop();
          Navigator.of(context).pop(1);
          return;
        case 5:
          _favouriteStatus += await removeFromWatching();
          _favouriteStatus += await removeFromFavourite();
          break;
        case 6:
          _favouriteStatus += await removeFromWatching();
          _favouriteStatus += await removeFromFavourite();
          break;
        default:
          Navigator.of(context).pop();
          return;
      }

      if (CustomDialog.isDialogOn) {
        Navigator.of(context).pop();
      }

      if (_willPopAfterPressed) {
        Navigator.of(context).pop(2);
      } else {
        willReloadAfterPop = true;
      }
    } catch (exception) {
      Navigator.of(context).pop();
      bool isRetry =
          await CustomDialog.showSelectionDialog(
            context,
            "Error",
            exception.toString() + "\nTry again?",
          ) ??
          false;
      if (isRetry) {
        return onMainButtonPressed(context);
      }
    }
  }

  //Post////////////////////////////////////////////////////////////////////////

  Future<int> addToWatching() async {
    try {
      return await _postController.addToWatching(_movieDetail);
    } on Exception {
      rethrow;
    }
  }

  Future<int> removeFromWatching() async {
    try {
      return await _postController.removeFromWatching(_movieDetail);
    } on Exception {
      rethrow;
    }
  }

  Future<int> removeFromFavourite() async {
    try {
      return await _postController.removeFromFavourite(_movieDetail);
    } on Exception {
      rethrow;
    }
  }

  Future<int> addToFavourite() async {
    try {
      return await _postController.addToFavourite(_movieDetail);
    } on Exception {
      rethrow;
    }
  }

  Future<int> addToNonFavourite() async {
    try {
      return await _postController.addToNonFavourite(_movieDetail);
    } on Exception {
      rethrow;
    }
  }

  Future<int> removeFromNonFavourite() async {
    try {
      return await _postController.removeFromNonFavourite(_movieDetail);
    } on Exception {
      rethrow;
    }
  }
}
