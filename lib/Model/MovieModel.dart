import 'package:demozzzzzzz/Model/SeasonModel.dart';
import 'package:flutter/material.dart';

class MovieModel {
  final int id;
  final String name;
  final String transName;
  final String producer;
  final String pictureLink;
  final List<String> tags;
  final String description;
  final List<SeasonModel> seasonForm;
  final double compatibleScore;
  final int favouriteStatus;

  MovieModel({
    @required this.id,
    @required this.name,
    @required this.transName,
    @required this.producer,
    @required this.pictureLink,
    @required this.tags,
    @required this.description,
    @required this.seasonForm,
    @required this.compatibleScore,
    @required this.favouriteStatus,
  });

  factory MovieModel.fromJson(Map<String, dynamic> json) {
    var tagsFromJson = json['tags'];
    List<String> tagList = new List<String>.from(tagsFromJson);

    var list = json['seasons'] as List;
    List<SeasonModel> seasonList =
        list.map((i) => SeasonModel.fromJson(i)).toList();

    return MovieModel(
      id: json['id'],
      name: json['name'] ?? "",
      transName: json['transName'] ?? "",
      producer: json['producer'] ?? "",
      pictureLink: json['pictureLink'] ?? "",
      tags: tagList,
      description: json['description'],
      seasonForm: seasonList,
      compatibleScore: (json['compatibleScore'] != null)
          ? json['compatibleScore'] + 1.0
          : -1.0,
      favouriteStatus: json['status'] ?? -1,
    );
  }
}
