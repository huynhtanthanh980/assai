import 'package:auto_size_text/auto_size_text.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Screen/App/MovieCard/MovieTag.dart';
import 'package:demozzzzzzz/Screen/App/MovieCard/MovieThumbnail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CheckableMovieCard extends StatefulWidget {
  final double height;
  final double width;
  final MovieModel movieDetail;
  final VoidCallback onPressed;
  bool isCardSelected;

  double verticalPadding;
  double horizontalPadding;
  double thumbnailSize;
  double cardHeight;
  double cardWidth;
  double infoButtonWidth;
  double detailBoxWidth;
  double detailBoxLeftShift;

  CheckableMovieCard({
    @required this.movieDetail,
    @required this.height,
    @required this.width,
    @required this.onPressed,
    @required this.isCardSelected,
  }) {
    verticalPadding = height * 0.085;
    horizontalPadding = width * 0.05;
    thumbnailSize = height * 0.9;
    cardHeight = height * 0.7;
    cardWidth = width * 0.75;
    infoButtonWidth = width * 0.05;

    detailBoxLeftShift =
        cardWidth + thumbnailSize - (width - horizontalPadding) + width * 0.06;
    detailBoxWidth =
        cardWidth - detailBoxLeftShift - infoButtonWidth - width * 0.03;
  }

  @override
  _CheckableMovieCardState createState() => _CheckableMovieCardState();
}

class _CheckableMovieCardState extends State<CheckableMovieCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      width: widget.width,
      padding: EdgeInsets.only(
        top: widget.verticalPadding,
        bottom: widget.verticalPadding,
        left: widget.horizontalPadding,
        right: widget.horizontalPadding,
      ),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment(1.0, -1.0),
            child: buildCard(context),
          ),
          Align(
            alignment: Alignment(-1.0, 1.0),
            child: buildMovieThumbnail(context),
          ),
        ],
      ),
    );
  }

  Widget buildCard(BuildContext context) {
    return Container(
      height: widget.cardHeight,
      width: widget.cardWidth,
      decoration: BoxDecoration(
        color: MovieCardColor.movieCard,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
            color: MovieCardColor.movieCardShadow,
            blurRadius: 0.0,
            spreadRadius: 0.0,
            offset: Offset(
              -8.0,
              8.0,
            ),
          ),
        ],
      ),
      child: Container(
        height: widget.cardHeight,
        width: widget.cardWidth - widget.infoButtonWidth,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: widget.cardHeight,
              width: widget.cardWidth - widget.infoButtonWidth,
              padding: EdgeInsets.only(
                top: widget.height * 0.02,
                bottom: widget.height * 0.02,
                right: widget.width * 0.02,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    child: Container(
                      width: widget.cardWidth - widget.infoButtonWidth,
                      padding: EdgeInsets.only(
                        top: widget.height * 0.05,
                        bottom: widget.height * 0.05,
                        left: widget.detailBoxLeftShift,
                      ),
                      child: AutoSizeText(
                        widget.movieDetail.name,
                        maxLines: 3,
                        minFontSize: widget.height * 0.1,
                        stepGranularity: widget.height * 0.05,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: widget.height * 0.13,
                          color: MovieCardColor.mainText,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                  Container(
                    height: widget.height * 0.135,
                    width: widget.cardWidth - widget.infoButtonWidth,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: List.generate(
                          widget.movieDetail.tags.length + 1,
                          (index) {
                            if (index == 0) {
                              return Container(
                                height: widget.height * 0.13,
                                width: widget.detailBoxLeftShift,
                              );
                            } else {
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: widget.width * 0.007),
                                child: MovieTag(
                                  height: widget.height * 0.13,
                                  width: widget.width * 0.15,
                                  title: widget.movieDetail.tags[index - 1],
                                ),
                              );
                            }
                          },
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: widget.cardHeight,
              width: widget.infoButtonWidth,
              child: MaterialButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                ),
                padding: EdgeInsets.zero,
                elevation: 1.0,
                focusElevation: 0.0,
                highlightElevation: 0.0,
                color: MovieCardColor.infoButton,
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.white,
                  size: widget.width * 0.05,
                ),
                onPressed: onCardPressed,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildMovieThumbnail(BuildContext context) {
    return Hero(
      tag: widget.movieDetail.id.toString() + "Thumbnail",
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          height: widget.thumbnailSize,
          width: widget.thumbnailSize,
          child: MaterialButton(
            elevation: 0.0,
            focusElevation: 0.0,
            padding: EdgeInsets.zero,
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            child: Stack(
              children: <Widget>[
                MovieThumbnail(
                  height: widget.thumbnailSize,
                  pictureLink: widget.movieDetail.pictureLink,
                ),
                (widget.isCardSelected)
                    ? AspectRatio(
                        aspectRatio: 1.0,
                        child: Container(
                          height: widget.thumbnailSize,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white38,
                          ),
                          child: Center(
                            child: Icon(
                              Icons.check,
                              color: Colors.greenAccent,
                              size: widget.thumbnailSize * 0.5,
                            ),
                          ),
                        ),
                      )
                    : Container(),
                Align(
                  alignment: Alignment(0.9, 0.9),
                  child: Container(
                    height: widget.height * 0.25,
                    width: widget.height * 0.25,
                  ),
                ),
              ],
            ),
            onPressed: onCardPressed,
          ),
        ),
      ),
    );
  }

  void onCardPressed() {
    setState(() {
      widget.isCardSelected = !widget.isCardSelected;
    });
    widget.onPressed();
  }
}
