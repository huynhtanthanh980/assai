import 'package:auto_size_text/auto_size_text.dart';
import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/FilterTag.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:demozzzzzzz/Screen/App/SearchScreen/SearchController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FilterDrawer extends StatefulWidget {
  final SearchController controller;
  final double height;
  final double width;
  final VoidCallback onSearchBarPressed;
  final VoidCallback onSearchPressed;
  final VoidCallback onBrowseAllPressed;
  final VoidCallback onResetPressed;

  const FilterDrawer({
    Key key,
    @required this.controller,
    @required this.height,
    @required this.width,
    @required this.onSearchPressed,
    @required this.onBrowseAllPressed,
    @required this.onSearchBarPressed,
    @required this.onResetPressed,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return FilterDrawerState();
  }
}

class FilterDrawerState extends State<FilterDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      width: widget.width,
      decoration: BoxDecoration(
        color: SearchScreenColor.filterBox,
      ),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: widget.height * 0.03,
            ),
            child: Container(
              height: widget.height * 0.06,
              child: buildSearchBar(),
            ),
          ),
          Container(
            height: widget.height * 0.6,
            width: widget.width,
            child: Column(
              children: <Widget>[
                Container(
                  height: widget.height * 0.53,
                  width: widget.width,
                  child: buildFilterTag(),
                ),
                SizedBox(
                  height: widget.height * 0.02,
                ),
                buildResetButton(),
              ],
            ),
          ),
          SizedBox(
            height: widget.height * 0.02,
          ),
          Container(
            child: buildFilterButton(),
          ),
        ],
      ),
    );
  }

  Widget buildSearchBar() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(25.0),
      child: Container(
        height: widget.height * 0.06,
        width: widget.width * 0.9,
        padding: EdgeInsets.symmetric(
          horizontal: widget.width * 0.05,
        ),
        color: SearchScreenColor.searchBox,
        child: MaterialButton(
          padding: EdgeInsets.zero,
          highlightColor: Colors.transparent,
          focusColor: Colors.transparent,
          splashColor: Colors.transparent,
          child: Text(
            widget.controller.getSearchHistory ?? "Input here",
            style: TextStyle(
              color: SearchScreenColor.mainText,
              fontSize: widget.height * 0.025,
            ),
            overflow: TextOverflow.ellipsis,
          ),
          onPressed: () {
            Navigator.of(context).pop();
            widget.onSearchBarPressed();
          },
        ),
      ),
    );
  }

  Widget buildFilterTag() {
    List<FilterTag> _tagList = widget.controller.getTagList;
    return SingleChildScrollView(
      child: Center(
        child: Wrap(
          spacing: widget.width * 0.01,
          runSpacing: widget.height * 0.005,
          children: List<Widget>.generate(_tagList.length, (index) {
            return Container(
              height: widget.height * 0.05,
              width: widget.width * 0.3,
              decoration: BoxDecoration(
                color:
                    widget.controller.getTagColor(_tagList[index].filterStatus),
              ),
              child: MaterialButton(
                padding: EdgeInsets.symmetric(
                  horizontal: widget.width * 0.02,
                ),
                highlightColor: Colors.transparent,
                focusColor: Colors.transparent,
                //splashColor: Colors.transparent,
                elevation: 0.0,
                child: AutoSizeText(
                  _tagList[index].tag,
                  style: TextStyle(
                    fontSize: widget.height * 0.02,
                    color: (_tagList[index].filterStatus == 0)
                        ? SearchScreenColor.mainText
                        : Colors.white,
                  ),
                  maxLines: 1,
                ),
                onPressed: () {
                  setState(() {
                    widget.controller.onFilterTagPressed(index);
                  });
                },
              ),
            );
          }),
        ),
      ),
    );
  }

  Widget buildResetButton() {
    return Container(
      height: widget.height * 0.05,
      width: widget.width * 0.92,
      color: SearchScreenColor.defaultTag,
      child: MaterialButton(
        padding: EdgeInsets.symmetric(
          horizontal: widget.width * 0.02,
        ),
        highlightColor: Colors.transparent,
        focusColor: Colors.transparent,
        //splashColor: Colors.transparent,
        elevation: 0.0,
        child: AutoSizeText(
          "Reset",
          style: TextStyle(
            fontSize: widget.height * 0.02,
            color: Colors.white38,
          ),
          maxLines: 1,
        ),
        onPressed: () {
          setState(() {
            widget.controller.resetFilter();
          });
          widget.onResetPressed();
          Navigator.of(context).pop();
        },
      ),
    );
  }

  Widget buildFilterButton() {
    return Container(
      width: widget.width,
      child: Column(
        children: <Widget>[
          Container(
            height: widget.height * 0.07,
            width: widget.width,
            color: SearchScreenColor.background,
            child: MaterialButton(
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              //splashColor: Colors.transparent,
              elevation: 0.0,
              child: AutoSizeText(
                "Search",
                style: TextStyle(
                  fontSize: ScreenSize.safeBlockVerticalSize * 2,
                  color: SearchScreenColor.mainText,
                ),
                maxLines: 1,
              ),
              onPressed: () {
                Navigator.of(context).pop();
                widget.onSearchPressed();
              },
            ),
          ),
          Container(
            height: widget.height * 0.05,
            width: widget.width,
            color: SearchScreenColor.searchBox,
            child: MaterialButton(
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              //splashColor: Colors.transparent,
              elevation: 0.0,
              child: AutoSizeText(
                "Browse All",
                style: TextStyle(
                  fontSize: ScreenSize.safeBlockVerticalSize * 2,
                  color: SearchScreenColor.searchBar,
                ),
                maxLines: 1,
              ),
              onPressed: () {
                Navigator.of(context).pop();
                widget.onBrowseAllPressed();
              },
            ),
          ),
        ],
      ),
    );
  }
}
