import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:demozzzzzzz/Screen/App/DetailScreen/DetailController.dart';
import 'package:demozzzzzzz/Screen/App/DetailScreen/DetailScreen.dart';
import 'package:demozzzzzzz/Screen/App/TinderScreen/SwipeController.dart';
import 'package:demozzzzzzz/Screen/App/TinderScreen/TinderCard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

double cardPaddingX = 0.1;
double cardPaddingY = 0.2;

class SwipeableCard extends StatefulWidget {
  final String id;
  final SwipeController swipeController;
  final double topPadding;
  final double leftPadding;
  final double cardWidth;
  final double cardHeight;
  final MovieModel movieModel;
  final VoidCallback onLike;
  final VoidCallback onDislike;

  SwipeableCard({
    @required this.id,
    @required this.swipeController,
    @required this.leftPadding,
    @required this.topPadding,
    @required this.cardWidth,
    @required this.cardHeight,
    @required this.movieModel,
    @required this.onLike,
    @required this.onDislike,
  });

  State createState() => _SwipeableCardState();
}

class _SwipeableCardState extends State<SwipeableCard> {
  double spaceHeight;
  double spaceWidth;
  double posX;
  double posY;
  double defaultXPos;
  double defaultYPos;
  double leftXPos;
  double rightXPos;

  bool isTaping = false;
  int swipeDirection = 0; // 0: onScreen, -1: left swipe, 1: right swipe
  double rotateAngle = 0;
  bool isSwipeAction = true;

  @override
  initState() {
    super.initState();
    spaceHeight = ScreenSize.safeBlockVerticalSize * 100;
    spaceWidth = ScreenSize.safeBlockHorizontalSize * 100;
    defaultYPos = widget.topPadding;
    defaultXPos = widget.leftPadding;
    posX = defaultXPos;
    posY = defaultYPos;
    leftXPos = -widget.cardWidth * 0.07;
    rightXPos = spaceWidth - widget.cardWidth * 0.93;
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel<SwipeController>(
      model: widget.swipeController,
      child: Container(
        height: spaceHeight,
        width: spaceWidth,
        child: GestureDetector(
          onPanStart: (tapInfo) {
            isTaping = true;
          },
          onPanUpdate: (tapInfo) {
            setState(() {
              posX += tapInfo.delta.dx;
              posY += tapInfo.delta.dy;
              swipeDirection = isOffScreen(posX);
              setNewAngle(posX);
            });
          },
          onPanEnd: (tapInfo) {
            swipeDirection = isOffScreen(posX);
            if (swipeDirection == 1) {
              posX = spaceWidth * 2;
            } else if (swipeDirection == -1) {
              posX = -spaceWidth * 2;
            }

            setState(() {
              if (swipeDirection == 0) {
                posX = defaultXPos;
                posY = defaultYPos;
                setNewAngle(defaultXPos);
              }
            });

            isTaping = false;
          },
          child: ScopedModelDescendant<SwipeController>(
            builder: (context, child, model) {
              int likeStatus;
              if (model.nextRemoveCard == widget.id) {
                if (model.isLike) {
                  posX = spaceWidth * 2;
                  swipeDirection = 1;
                } else {
                  posX = -spaceWidth * 2;
                  swipeDirection = -1;
                }
                setNewAngle(posX);
                model.resetNext();
                likeStatus = isOffScreen(posX);
                isSwipeAction = false;
              } else {
                likeStatus = isOffScreen(posX);
              }
              return Stack(
                children: <Widget>[
                  AnimatedPositioned(
                    left: posX,
                    top: posY,
                    child: Transform.rotate(
                      angle: rotateAngle,
                      origin: Offset(
                        0,
                        -widget.cardHeight * 0.5,
                      ),
                      child: Container(
                        width: widget.cardWidth,
                        height: widget.cardHeight,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: TinderCard(
                          height: widget.cardHeight,
                          width: widget.cardWidth,
                          movieDetail: widget.movieModel,
                          likeStatus: likeStatus,
                          onPressed: onTinderCardPressed,
                        ),
                      ),
                    ),
                    duration: Duration(
                        milliseconds:
                            (posX == spaceWidth * 2 || posX == -spaceWidth * 2)
                                ? widget.swipeController.swipeDelay
                                : widget.swipeController.cardSpeed),
                    onEnd: (isSwipeAction) ? onSwipeEnd : null,
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  int isOffScreen(double posX) {
    if (posX < leftXPos) {
      isSwipeAction = true;
      return -1;
    } else if (posX > rightXPos) {
      isSwipeAction = true;
      return 1;
    } else {
      return 0;
    }
  }

  void setNewAngle(double posX) {
    double ratio = (posX - defaultXPos) / (spaceWidth * 0.1);
    if (ratio > 1.0) {
      ratio = 1.0;
    } else if (ratio < -1.0) {
      ratio = -1.0;
    }
    rotateAngle = ratio * 0.2;
  }

  void onSwipeEnd() {
    if (!isTaping) {
      if (swipeDirection == 1) {
        widget.onLike();
      }
      else if (swipeDirection == -1) {
        widget.onDislike();
      }
    }
  }

  Future<void> onTinderCardPressed() async {
    //0: back
    //1: main button pressed
    //2: sub button pressed

    int popReason = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) =>
            DetailScreen(
              controller: DetailController(
                widget.movieModel,
                0,
              ),
            ),
      ),
    );

    if (popReason == 1) {
      await Future.delayed(Duration(milliseconds: 300));
      setState(() {
        posX = spaceWidth * 2;
        setNewAngle(posX);
      });
      await Future.delayed(Duration(milliseconds: 300));
      widget.onLike();
    }
    else if (popReason == 2) {
      await Future.delayed(Duration(milliseconds: 300));
      setState(() {
        posX = -spaceWidth * 2;
        setNewAngle(posX);
      });
      await Future.delayed(Duration(milliseconds: 300));
      widget.onLike();
    }
  }
}
