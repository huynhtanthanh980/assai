import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:demozzzzzzz/Screen/App/DetailScreen/DetailController.dart';
import 'package:demozzzzzzz/Screen/App/DetailScreen/DetailScreen.dart';
import 'package:demozzzzzzz/Screen/App/FavWatchController.dart';
import 'package:demozzzzzzz/Screen/App/HeaderBar.dart';
import 'package:demozzzzzzz/Screen/App/LoadMoreList.dart';
import 'package:demozzzzzzz/Screen/App/MovieCard/EmptyMovieCard.dart';
import 'package:demozzzzzzz/Screen/App/MovieCard/MovieCard.dart';
import 'package:demozzzzzzz/Screen/CustomDialog.dart';
import 'package:demozzzzzzz/Screen/CustomScrollBehavior.dart';
import 'package:demozzzzzzz/Services/ServiceError.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WatchingScreen extends StatefulWidget {
  final FavWatchController controller = FavWatchController.getInstance();

  @override
  State<StatefulWidget> createState() {
    return WatchingScreenState();
  }
}

class WatchingScreenState extends State<WatchingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: FavouriteScreenColor.background,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            HeaderBar(
              height: ScreenSize.safeBlockVerticalSize * 6,
              width: ScreenSize.safeBlockHorizontalSize * 100,
              title: "watching",
              color: WatchingScreenColor.header,
            ),
            Expanded(
              child: (widget.controller.isWatchingNull())
                  ? buildList()
                  : buildMovieList(
                      widget.controller.sort(
                        widget.controller.getWatchingList,
                        true,
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildList() {
    return FutureBuilder<List<MovieModel>>(
      future: widget.controller.loadWatchingList(20).catchError((error) async {
        if (error.status == ErrorCode.TimeOut ||
            error.status == ErrorCode.Unavailable) {
          bool isRetry = await CustomDialog.showSelectionDialog(
              context,
              "Error " + error.code.toString(),
              error.toString() + "\nDo you want to retry?") ??
              false;
          if (isRetry) {
            setState(() {
              widget.controller.setWatchingNull();
            });
          }
        } else {
          await CustomDialog.showAlertDialog(
              context, "Error " + error.code.toString(), error.toString());
          widget.controller.onLogOutError();
        }
        return [];
      }),
      builder: (context, snapshot) {
        if (!snapshot.hasData ||
            snapshot.connectionState == ConnectionState.waiting) {
          return buildEmptyMovieList();
        } else {
          return buildMovieList(widget.controller.sort(snapshot.data, true));
        }
      },
    );
  }

  Widget buildListHeader(int listLength) {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 5.5,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      color: WatchingScreenColor.sortBar,
      padding: EdgeInsets.symmetric(
        vertical: ScreenSize.safeBlockVerticalSize * 0.5,
        horizontal: ScreenSize.safeBlockHorizontalSize * 5,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4.5,
            child: Center(
              child: Text(
                (listLength > 1)
                    ? (listLength.toString() + " movies")
                    : (listLength == 1)
                    ? (listLength.toString() + " movie")
                    : "No movie",
                style: TextStyle(
                  fontSize: ScreenSize.safeBlockVerticalSize * 2.4,
                  color: MovieCardColor.mainText,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4.5,
            width: ScreenSize.safeBlockVerticalSize * 4.5,
            child: Center(
              child: MaterialButton(
                padding: EdgeInsets.zero,
                shape: CircleBorder(),
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                child: widget.controller.getSortIcon(true,
                  ScreenSize.safeBlockVerticalSize * 3.5,
                  MovieCardColor.mainText,
                ),
                onPressed: () {
                  setState(() {
                    widget.controller.changeSort(true);
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildMovieList(List<MovieModel> movieList) {
    return Container(
      child: Column(
        children: <Widget>[
          buildListHeader(movieList.length),
          Expanded(
            child: ScrollConfiguration(
              behavior: CustomScrollBehavior(),
              child: RefreshIndicator(
                color: WatchingScreenColor.loadText,
                backgroundColor: WatchingScreenColor.loadBackGround,
                onRefresh: () async {
                  setState(() {
                    widget.controller.setWatchingNull();
                  });
                },
                child: LoadMoreList(
                  movieList: movieList,
                  itemBuild: (context, index) {
                    return MovieCard(
                      height: ScreenSize.safeBlockVerticalSize * 20,
                      width: ScreenSize.safeBlockHorizontalSize * 100,
                      movieDetail: movieList[index],
                      onPressed: () async {
                        await onMovieCardPressed(movieList[index]);
                      },
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildEmptyMovieList() {
    return Container(
      child: Column(
        children: <Widget>[
          buildListHeader(0),
          Expanded(
            child: ScrollConfiguration(
              behavior: CustomScrollBehavior(),
              child: RefreshIndicator(
                color: WatchingScreenColor.loadText,
                backgroundColor: WatchingScreenColor.loadBackGround,
                onRefresh: () async {
                  setState(() {
                    widget.controller.setWatchingNull();
                  });
                },
                child: ListView.builder(
                    itemCount: 5,
                    itemBuilder: (context, index) {
                      if (index < 4) {
                        return EmptyMovieCard(
                          height: ScreenSize.safeBlockVerticalSize * 20,
                          width: ScreenSize.safeBlockHorizontalSize * 100,
                        );
                      }
                      return Container(
                        height: ScreenSize.safeBlockVerticalSize * 10,
                        width: ScreenSize.safeBlockHorizontalSize * 100,
                        child: Center(
                          child: Text(
                            "Loading",
                            style: TextStyle(
                              fontSize: ScreenSize.safeBlockVerticalSize * 2,
                              color: WatchingScreenColor.loadText,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      );
                    }
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> onMovieCardPressed(MovieModel movieDetail) async {
    //0: back
    //1: main button pressed
    //2: sub button pressed

    int popReason = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => DetailScreen(
          controller: DetailController(
            movieDetail,
            5,
          ),
        ),
      ),
    );

    if (popReason != 0) {
      setState(() {
        widget.controller.setWatchingNull();
      });
    }

    if (popReason == 2) {
      widget.controller.reloadSearch();
    }
  }
}