import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmptyMovieCard extends StatefulWidget {
  final double height;
  final double width;
  double verticalPadding;
  double horizontalPadding;
  double thumbnailSize;
  double cardHeight;
  double cardWidth;
  double infoButtonWidth;
  double detailBoxWidth;
  double detailBoxLeftShift;

  EmptyMovieCard({
    @required this.height,
    @required this.width,
  }) {
    verticalPadding = height * 0.085;
    horizontalPadding = width * 0.05;
    thumbnailSize = height * 0.9;
    cardHeight = height * 0.7;
    cardWidth = width * 0.75;
    infoButtonWidth = width * 0.05;

    detailBoxLeftShift =
        cardWidth + thumbnailSize - (width - horizontalPadding) + width * 0.03;
    detailBoxWidth =
        cardWidth - detailBoxLeftShift - infoButtonWidth - width * 0.03;
  }

  @override
  State<StatefulWidget> createState() {
    return EmptyMovieCardState();
  }
}

class EmptyMovieCardState extends State<EmptyMovieCard>
    with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 2500), vsync: this);
    final CurvedAnimation curve =
        CurvedAnimation(parent: controller, curve: Curves.linear);
    animation = Tween<double>(begin: -0.5, end: 1.5).animate(curve);
    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        controller.forward();
      }
      setState(() {});
    });
    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      width: widget.width,
      padding: EdgeInsets.only(
        top: widget.verticalPadding,
        bottom: widget.verticalPadding,
        left: widget.horizontalPadding,
        right: widget.horizontalPadding,
      ),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment(1.0, -1.0),
            child: buildCard(context),
          ),
          Align(
            alignment: Alignment(-1.0, 1.0),
            child: buildMovieThumbnailWithScore(context),
          ),
        ],
      ),
    );
  }

  LinearGradient getGradient() {
    return LinearGradient(
      begin: Alignment.bottomLeft,
      end: Alignment.topRight,
      stops: [
        0.0,
        animation.value,
      ],
      colors: [Colors.grey, Colors.white30,],
    );
  }

  Widget buildCard(BuildContext context) {
    return Container(
      height: widget.cardHeight,
      width: widget.cardWidth,
      padding: EdgeInsets.only(
        left: widget.detailBoxLeftShift,
      ),
      decoration: BoxDecoration(
        color: MovieCardColor.movieCard,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
            color: MovieCardColor.movieCardShadow,
            blurRadius: 0.0,
            spreadRadius: 0.0,
            offset: Offset(
              -8.0,
              8.0,
            ),
          ),
        ],
      ),
      child: Container(
        height: widget.cardHeight,
        width: widget.detailBoxWidth + widget.infoButtonWidth,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: widget.cardHeight,
              width: widget.detailBoxWidth,
              padding: EdgeInsets.only(
                top: widget.height * 0.03,
                bottom: widget.height * 0.03,
              ),
              child: AnimatedBuilder(
                animation: animation,
                builder: (context, child) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: widget.height * 0.15,
                        width: widget.detailBoxWidth * 0.9,
                        decoration: BoxDecoration(
                          gradient: getGradient(),
                        ),
                      ),
                      Container(
                        height: widget.height * 0.12,
                        width: widget.detailBoxWidth * 0.8,
                        decoration: BoxDecoration(
                          gradient: getGradient(),
                        ),
                      ),
                      Container(
                        height: widget.height * 0.13,
                        width: widget.detailBoxWidth,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: 2,
                          itemBuilder: (context, index) {
                            return Container(
                              padding: (index != 1)
                                  ? EdgeInsets.only(right: widget.width * 0.02)
                                  : EdgeInsets.zero,
                              child: Container(
                                height: widget.height * 0.13,
                                width: widget.width * 0.2,
                                decoration: BoxDecoration(
                                  gradient: getGradient(),
                                  borderRadius: BorderRadius.circular(10.0),),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
            Container(
              height: widget.cardHeight,
              width: widget.infoButtonWidth,
              child: MaterialButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                ),
                padding: EdgeInsets.zero,
                elevation: 1.0,
                focusElevation: 0.0,
                highlightElevation: 0.0,
                color: MovieCardColor.infoButton,
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.white,
                  size: widget.width * 0.05,
                ),
                onPressed: () async {},
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildMovieThumbnailWithScore(BuildContext context) {
    return Container(
      height: widget.thumbnailSize,
      width: widget.thumbnailSize,
      child: AnimatedBuilder(
        animation: animation,
        builder: (context, child) {
          return Container(
            padding: EdgeInsets.zero,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: MovieCardColor.movieCard,
            ),
            child: AspectRatio(
              aspectRatio: 1.0,
              child: Container(
                height: widget.thumbnailSize,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                      width: widget.thumbnailSize * 0.02,
                      color: MovieCardColor.thumbnailBorder),
                  gradient: getGradient(),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      blurRadius: 1.0,
                      spreadRadius: 1.0,
                      offset: Offset(
                        1.0,
                        1.0,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}