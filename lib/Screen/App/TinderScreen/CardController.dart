import 'dart:async';

import 'package:async/async.dart';
import 'package:demozzzzzzz/Data/DataSource.dart';
import 'package:demozzzzzzz/Model/MovieModel.dart';
import 'package:demozzzzzzz/Screen/App/FavWatchController.dart';
import 'package:demozzzzzzz/Screen/App/HomeScreen/HomeScreenController.dart';
import 'package:demozzzzzzz/Screen/App/SearchScreen/SearchController.dart';
import 'package:demozzzzzzz/Screen/App/TinderScreen/SwipeController.dart';
import 'package:demozzzzzzz/Screen/CustomDialog.dart';
import 'package:demozzzzzzz/Services/MovieLoader.dart';
import 'package:demozzzzzzz/Services/PostService.dart';
import 'package:demozzzzzzz/Services/ServiceError.dart';
import 'package:demozzzzzzz/Services/SharePref.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class CardController {
  static CardController _instance;

  MovieLoader _movieLoader;
  PostService _postService;

  bool willReload = false;

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  CancelableOperation<List<MovieModel>> _completer;

  List<MovieModel> _recommendList = new List<MovieModel>();
  SwipeController swipeController = new SwipeController();
  bool _isLoading = false;

  //Constructor/////////////////////////////////////////////////////////////////

  CardController._privateConstructor(String _token) {
    _likeCardController.stream.listen(_likeMovie);
    _dislikeCardController.stream.listen(_dislikeMovie);
    String _apiLink = (_token == DataSource.localTestToken)
        ? DataSource.recommendDemo
        : (SharePref.sourceAPI + "/recommend");
    this._movieLoader = MovieLoader(_apiLink, _token);
    this._postService = PostService(_token);
  }

  factory CardController(String token) {
    if (_instance == null) {
      _instance = CardController._privateConstructor(token);
    }
    return _instance;
  }

  factory CardController.getInstance() {
    return _instance;
  }

  static void dispose() {
    _instance._likeCardController.close();
    _instance._dislikeCardController.close();
    _instance._getCardListSubject.close();
    _instance = null;
  }

  //Controller//////////////////////////////////////////////////////////////////

  Future<void> onLikeButtonPressed() async {
    if (!swipeController.isSwipeLocked() && _recommendList.isNotEmpty) {
      swipeController.lockSwipe();
      swipeController.swipeCard(_recommendList.last.id.toString(), true);
      await Future.delayed(Duration(milliseconds: swipeController.swipeDelay));
      swipeController.unlockSwipe();
      await _likeMovie(_recommendList.last);
    }
  }

  Future<void> onDislikeButtonPressed() async {
    if (!swipeController.isSwipeLocked() && _recommendList.isNotEmpty) {
      swipeController.lockSwipe();
      swipeController.swipeCard(_recommendList.last.id.toString(), false);
      await Future.delayed(Duration(milliseconds: swipeController.swipeDelay));
      swipeController.unlockSwipe();
      await _dislikeMovie(_recommendList.last);
    }
  }

  Future<void> onAddButtonPressed() async {
    if (!swipeController.isSwipeLocked() && _recommendList.isNotEmpty) {
      swipeController.lockSwipe();
      swipeController.swipeCard(_recommendList.last.id.toString(), true);
      await Future.delayed(Duration(milliseconds: swipeController.swipeDelay));
      swipeController.unlockSwipe();
      await _addMovie(_recommendList.last);
    }
  }

  Future<void> _likeMovie(MovieModel movieDetail) async {
    willReload = true;
    _removeCard(movieDetail);

    try {
      await _postService.addToFavourite(movieDetail);
      await _postService.addToWatching(movieDetail);
    } catch (error) {
      print(error.toString());
      BuildContext context = scaffoldKey.currentState.context;
      if (error.status == ErrorCode.TimeOut ||
          error.status == ErrorCode.Unavailable) {
        bool isRetry = await CustomDialog.showSelectionDialog(
                context,
                "Error " + error.code.toString(),
                "Error when trying to like ${movieDetail.name}\nDo you want to retry?") ??
            false;
        if (isRetry) {
          await _likeMovie(movieDetail);
          return;
        }
      } else {
        await CustomDialog.showAlertDialog(
            context, "Error" + error.code.toString(), error.toString());
        onLogOutError();
      }
    }

    getNewList(false, 10);
  }

  Future<void> _dislikeMovie(MovieModel movieDetail) async {
    willReload = true;
    _removeCard(movieDetail);

    try {
      await _postService.addToNonFavourite(movieDetail);
    } catch (error) {
      BuildContext context = scaffoldKey.currentState.context;
      if (error.status == ErrorCode.TimeOut ||
          error.status == ErrorCode.Unavailable) {
        bool isRetry = await CustomDialog.showSelectionDialog(
            context, "Error " + error.code.toString(),
            "Error when trying to dislike ${movieDetail
                .name}\nDo you want to retry?") ??
            false;
        if (isRetry) {
          await _dislikeMovie(movieDetail);
          return;
        }
      }
      else {
        await CustomDialog.showAlertDialog(
            context, "Error" + error.code.toString(),
            error.toString());
        onLogOutError();
      }
    }

    await getNewList(false, 10);
  }

  Future<void> _addMovie(MovieModel movieDetail) async {
    willReload = true;
    _removeCard(movieDetail);

    try {
      await _postService.addToFavourite(movieDetail);
    } catch (error) {
      BuildContext context = scaffoldKey.currentState.context;
      if (error.status == ErrorCode.TimeOut ||
          error.status == ErrorCode.Unavailable) {
        bool isRetry = await CustomDialog.showSelectionDialog(
          context, "Error " + error.code.toString(),
          "Error when trying to add ${movieDetail
              .name}\nDo you want to retry?",) ?? false;
        if (isRetry) {
          await _addMovie(movieDetail);
          return;
        }
      }
      else {
        await CustomDialog.showAlertDialog(
            context, "Error " + error.code.toString(),
            error.toString());
        onLogOutError();
      }
    }

    await getNewList(false, 10);
  }

  Future<void> getNewList(bool isNew, int timeOut) async {
    try {
      if (isNew) {
        _recommendList = [];
        _getCardListSubject.add([]);
      }
      if (_recommendList.length <= 3 && !_isLoading) {
        _isLoading = true;
        _getCardListSubject.add(
            _recommendList); //needed to reloaded the stream builder, if not then it wont reload after
        List<MovieModel> newList = await createLoadMovieCompleter(
            isNew, timeOut);
        if (newList != null) {
          for (MovieModel movie in newList) {
            _recommendList.insert(0, movie);
          }
          _getCardListSubject.add(_recommendList);
        }
        _isLoading = false;
      }
    } on Exception {
      rethrow;
    }
  }

  Future<List<MovieModel>> createLoadMovieCompleter(bool isNew,
      int timeOut) async {
    try {
      _completer = CancelableOperation.fromFuture(
        Future.value(_loadMovie(isNew, timeOut)),
        onCancel: () {
          return [];
        },
      );
      return _completer.value;
    } on Exception {
      _completer.cancel();
      rethrow;
    }
  }

  void onCancelCompleter() async {
    if (_completer != null) {
      await _completer.cancel();
    }
  }

  Future<List<MovieModel>> _loadMovie(bool isNew, int timeOut) async {
    var list = await _movieLoader.loadMovie(timeOut).catchError((error) async {
      //if there are some movies left, let the user finish them before raise error
      if (_recommendList.length != 0) {
        return;
      }
      BuildContext context = scaffoldKey.currentState.context;
      if (error.status == ErrorCode.TimeOut ||
          error.status == ErrorCode.Unavailable) {
        bool isRetry = await CustomDialog.showSelectionDialog(
            context, "Error " + error.code.toString(),
            "Cannot get new list\nDo you want to retry?") ??
            false;
        if (isRetry) {
          _isLoading = false;
          await getNewList(isNew, timeOut * 2);
          return;
        }
        else {
          if (willReload) {
            reloadOtherList();
          }
          Navigator.of(context).pop();
        }
      }
      else {
        await CustomDialog.showAlertDialog(
            context, "Error " + error.code.toString(),
            error.toString());
        onLogOutError();
      }
    });
    return list;
  }

  void _removeCard(MovieModel movieModel) {
    int index = _recommendList.indexWhere((element) =>
    element.id == movieModel.id);
    if (index >= 0) {
      _recommendList.removeAt(index);
      _getCardListSubject.add(_recommendList);
    }
  }

  void reloadOtherList() {
    SearchController.getInstance().reload();
    FavWatchController.getInstance().reload();
  }

  void onLogOutError() {
    HomeScreenController().logOut(true);
  }

  //Bloc////////////////////////////////////////////////////////////////////////
  Sink<MovieModel> get likeCard => _likeCardController.sink;
  final _likeCardController = StreamController<MovieModel>();

  Sink<MovieModel> get dislikeCard => _dislikeCardController.sink;
  final _dislikeCardController = StreamController<MovieModel>();

  Stream<List<MovieModel>> get getCardList => _getCardListSubject.stream;
  final _getCardListSubject = BehaviorSubject<List<MovieModel>>();
}