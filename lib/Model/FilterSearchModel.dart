import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FilterSearchModel {
  final String name;
  final List<String> addedTag;
  final List<String> removedTag;

  FilterSearchModel({
    this.name = "",
    @required this.addedTag,
    @required this.removedTag,
  });
}
