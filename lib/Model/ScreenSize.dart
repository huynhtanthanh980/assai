import 'package:flutter/widgets.dart';

class ScreenSize {
  static bool isInit = false;

  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double horizontalBlockSize;
  static double verticalBlockSize;

  static double _safeAreaHorizontalSize;
  static double _safeAreaVerticalSize;
  static double safeBlockHorizontalSize;
  static double safeBlockVerticalSize;

  void init(BuildContext context) {
    isInit = true;
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    horizontalBlockSize = screenWidth / 100;
    verticalBlockSize = screenHeight / 100;

    _safeAreaHorizontalSize =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVerticalSize =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    safeBlockHorizontalSize = (screenWidth - _safeAreaHorizontalSize) / 100;
    safeBlockVerticalSize = (screenHeight - _safeAreaVerticalSize) / 100;
  }
}
