import 'package:auto_size_text/auto_size_text.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashSlide extends StatelessWidget {
  final String title;
  final String subTitle;
  final Color backgroundColor;
  final String picturePath;
  final Color titleColor;
  final Color subTitleColor;
  final VoidCallback onSlide;

  SplashSlide({
    this.title,
    this.titleColor,
    this.subTitle,
    this.subTitleColor,
    this.backgroundColor,
    this.picturePath,
    this.onSlide,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 100,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      padding: EdgeInsets.symmetric(
        vertical: ScreenSize.safeBlockVerticalSize * 5,
        horizontal: ScreenSize.safeBlockHorizontalSize * 5,
      ),
      color: backgroundColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: ScreenSize.safeBlockVerticalSize * 30,
            alignment: Alignment(-1.0, -1.0),
            child: AutoSizeText(
              title,
              maxLines: 2,
              style: TextStyle(
                color: titleColor,
                fontSize: ScreenSize.safeBlockVerticalSize * 5,
                fontWeight: FontWeight.w700,
              ),
              overflow: TextOverflow.visible,
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 40,
            alignment: Alignment(1.0, 0.0),
            child: Image.asset(
              picturePath,
              fit: BoxFit.fitHeight,
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 20,
            alignment: Alignment(1.0, 1.0),
            child: MaterialButton(
              padding: EdgeInsets.zero,
              highlightColor: Colors.transparent,
              focusColor: Colors.transparent,
              splashColor: Colors.transparent,
              elevation: 0.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  AutoSizeText(
                    subTitle,
                    maxLines: 1,
                    style: TextStyle(
                      color: subTitleColor,
                      fontSize: ScreenSize.safeBlockVerticalSize * 3,
                      fontWeight: FontWeight.w500,
                    ),
                    overflow: TextOverflow.visible,
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    size: ScreenSize.safeBlockVerticalSize * 3,
                    color: subTitleColor,
                  ),
                ],
              ),
              onPressed: onSlide,
            ),
          ),
        ],
      ),
    );
  }
}
