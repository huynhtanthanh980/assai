import 'package:carousel_slider/carousel_slider.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef void PageChangeCallBack(int index);

class BackGroundSlider extends StatefulWidget {
  final PageChangeCallBack onchange;

  const BackGroundSlider({
    Key key,
    this.onchange,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return BackGroundSliderState();
  }
}

class BackGroundSliderState extends State<BackGroundSlider> {
  List<String> _picturePath = [
    "assets/Login/1.jpg",
    "assets/Login/2.jpg",
    "assets/Login/3.jpg",
    "assets/Login/4.jpg",
    "assets/Login/5.png",
    "assets/Login/6.jpg",
    "assets/Login/7.jpg",
    "assets/Login/8.jpg",
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: ScreenSize.safeBlockVerticalSize * 75,
          width: ScreenSize.safeBlockHorizontalSize * 100,
          child: CarouselSlider(
            items: buildPictureCard(),
            options: CarouselOptions(
                height: ScreenSize.safeBlockVerticalSize * 80,
                aspectRatio: ScreenSize.safeBlockVerticalSize /
                    ScreenSize.safeBlockHorizontalSize,
                viewportFraction: 1.0,
                initialPage: 0,
                enableInfiniteScroll: true,
                reverse: false,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 5),
                autoPlayAnimationDuration: Duration(milliseconds: 1000),
                autoPlayCurve: Curves.fastOutSlowIn,
                scrollDirection: Axis.horizontal,
                onPageChanged: (newValue, reason) {
                  widget.onchange(newValue);
                }),
          ),
        ),
      ),
    );
  }

  List<Widget> buildPictureCard() {
    return List<Widget>.generate(
      _picturePath.length,
      (index) {
        return Container(
          height: ScreenSize.safeBlockVerticalSize * 75,
          width: ScreenSize.safeBlockHorizontalSize * 100,
          decoration: BoxDecoration(
            color: Colors.white30,
            image: DecorationImage(
              image: AssetImage(
                _picturePath[index],
              ),
              fit: BoxFit.fitHeight,
            ),
          ),
        );
      },
    );
  }
}
