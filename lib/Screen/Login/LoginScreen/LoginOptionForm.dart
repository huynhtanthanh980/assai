import 'package:demozzzzzzz/Color/CustomColor.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginOptionForm extends StatelessWidget {
  final double height;
  final double width;
  final VoidCallback onLoginPressed;
  final VoidCallback onSignUpPressed;

  const LoginOptionForm({
    Key key,
    @required this.height,
    @required this.width,
    @required this.onLoginPressed,
    @required this.onSignUpPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: height,
        width: width,
        padding: EdgeInsets.symmetric(
          vertical: height * 0.03,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: width * 0.8,
              padding: EdgeInsets.only(
                bottom: ScreenSize.safeBlockVerticalSize * 2,
              ),
              child: Text(
                "Let's get started!",
                style: TextStyle(
                  color: LoginScreenColor.textColor,
                  fontSize: ScreenSize.safeBlockVerticalSize * 3.2,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Container(
              width: width * 0.8,
              child: MaterialButton(
                elevation: 0.0,
                padding: EdgeInsets.symmetric(
                  vertical: ScreenSize.safeBlockVerticalSize * 1.5,
                  horizontal: ScreenSize.safeBlockHorizontalSize * 5,
                ),
                color: LoginScreenColor.textColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child: Center(
                  child: Text(
                    "Continue with ASSAI account",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: ScreenSize.safeBlockVerticalSize * 2.2,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                onPressed: onLoginPressed,
              ),
          ),
          SizedBox(
            height: ScreenSize.safeBlockVerticalSize * 1,
          ),
          Container(
            width: width * 0.8,
            child: Center(
              child: Text(
                "Or just a few second to join us",
                style: TextStyle(
                  color: Colors.white38,
                  fontSize: ScreenSize.safeBlockVerticalSize * 2,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
          Container(
            width: width * 0.8,
            child: MaterialButton(
              elevation: 0.0,
              padding: EdgeInsets.symmetric(
                vertical: ScreenSize.safeBlockVerticalSize * 1.5,
                horizontal: ScreenSize.safeBlockHorizontalSize * 5,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
                side: BorderSide(color: LoginScreenColor.textColor),
              ),
              child: Center(
                child: Text(
                  "Sign up",
                  style: TextStyle(
                    color: LoginScreenColor.textColor,
                    fontSize: ScreenSize.safeBlockVerticalSize * 2,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              onPressed: onSignUpPressed,
            ),
          ),
          ],
        ),
      ),);
  }
}
