import 'dart:async';
import 'dart:math';

import 'package:demozzzzzzz/Data/RandomLoadingQuote.dart';
import 'package:demozzzzzzz/Model/ScreenSize.dart';
import 'package:demozzzzzzz/Screen/App/FavWatchController.dart';
import 'package:demozzzzzzz/Screen/App/HomeScreen/HomeScreen.dart';
import 'package:demozzzzzzz/Screen/App/SearchScreen/SearchController.dart';
import 'package:demozzzzzzz/Screen/App/TinderScreen/CardController.dart';
import 'package:demozzzzzzz/Screen/CustomDialog.dart';
import 'package:demozzzzzzz/Screen/Login/SignUpScreen/ExRegisterScreen.dart';
import 'package:demozzzzzzz/Services/LoginInfo.dart';
import 'package:demozzzzzzz/Services/LoginService.dart';
import 'package:demozzzzzzz/Services/ServiceError.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginController {
  static LoginController _instance;

  final LoginService _loginService;
  int _loginPageIndex = 0;
  int _backGroundIndex = 0;
  bool _quoteLock = false;

  //Constructor/////////////////////////////////////////////////////////////////

  LoginController._privateConstructor(this._loginService) {
    _formController.stream.listen(_buildNewForm);
    _quoteController.stream.listen(_generateQuote);
    _progressController.stream.listen(_changeProgress);
  }

  factory LoginController(LoginService _loginService) {
    if (_instance == null) {
      _instance = LoginController._privateConstructor(_loginService);
    }
    return _instance;
  }

  factory LoginController.getInstance() {
    return _instance;
  }

  static void dispose() {
    _instance._formController.close();
    _instance._getFormBuildSubject.close();
    _instance._quoteController.close();
    _instance._getQuoteSubject.close();
    _instance._progressController.close();
    _instance._getProgressSubject.close();
    _instance = null;
  }

  double get getPadHeight {
    switch (_loginPageIndex) {
      case 0:
        return ScreenSize.safeBlockVerticalSize * 30;
      case 1:
        return ScreenSize.safeBlockVerticalSize * 40;
      case 2:
        return ScreenSize.safeBlockVerticalSize * 50;
      case 3:
        return ScreenSize.safeBlockVerticalSize * 30;
    }
    return 0;
  }

  int get getBackgroundIndex => _backGroundIndex;

  set setNewBackGround(int index) {
    _backGroundIndex = index;
  }

  Sink<int> get changeForm => _formController.sink;
  final _formController = StreamController<int>();

  Stream<int> get getFormBuildStream => _getFormBuildSubject.stream;
  final _getFormBuildSubject = BehaviorSubject<int>();

  void _buildNewForm(int index) {
    _loginPageIndex = index;
    _getFormBuildSubject.add(_loginPageIndex);
  }

  bool onPop() {
    if (_loginPageIndex == 1 || _loginPageIndex == 2) {
      _buildNewForm(0);
      return false;
    }
    return true;
  }

  Sink<int> get changeQuote => _quoteController.sink;
  final _quoteController = StreamController<int>();

  Stream<String> get getQuoteStream => _getQuoteSubject.stream;
  final _getQuoteSubject = BehaviorSubject<String>();

  Future<void> _generateQuote(int index) async {
    final _random = Random();
    int _randomIndex = _random.nextInt(randomLoadingQuote.length);
    _getQuoteSubject.add(randomLoadingQuote[_randomIndex]);

    if (!_quoteLock) {
      _quoteLock = true;
      await Future.delayed(
        Duration(seconds: 10),
      );
      _generateQuote(0);
      _quoteLock = false;
    }
  }

  Sink<int> get loadProgress => _progressController.sink;
  final _progressController = StreamController<int>();

  Stream<double> get getProgressStream => _getProgressSubject.stream;
  final _getProgressSubject = BehaviorSubject<double>();

  Future<void> _changeProgress(int index) async {
    switch (index) {
      case 0:
        _getProgressSubject.add(0.25);
        break;
      case 1:
        _getProgressSubject.add(0.5);
        break;
      case 2:
        _getProgressSubject.add(0.75);
        break;
      case 3:
        _getProgressSubject.add(1.0);
        break;
        break;
      default:
        _getProgressSubject.add(null);
    }
  }

  //Login///////////////////////////////////////////////////////////////////////

  Future<void> _initController(LoginInfo _loginInfo) async {
    try {
      CardController(_loginInfo.getToken);
      _progressController.add(0);
      FavWatchController _favWatchController =
          FavWatchController(_loginInfo.getToken);
      SharedPreferences _pref = await SharedPreferences.getInstance();
      int savedTimeOut = _pref.getInt("timeOut") ?? 20;
      await _favWatchController.loadList(savedTimeOut);
      _progressController.add(1);
      SearchController _searchController =
          SearchController(_loginInfo.getToken);
      await _searchController.initNameList();
      _progressController.add(2);
      await _searchController.initTagList();
      _progressController.add(3);
    } on Exception {
      rethrow;
    }
  }

  void onLoginWithSavedAcc(BuildContext context) {
    _formController.add(3);
    _loginService.loginWithSavedAcc().then((_loginInfo) async {
      await _initController(_loginInfo);
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => HomeScreen(),
        ),
      );
    }).catchError((error) async {
      if (error.runtimeType == ErrorStatus) {
        await CustomDialog.showAlertDialog(
            context, "Error " + error.code.toString(), error.toString());
      } else {
        await CustomDialog.showAlertDialog(context, "Error", error.toString());
      }
      await LoginService.deleteLoginSave();
      _formController.add(1);
    });
  }

  void onLogin(BuildContext context, String username, String password) {
    _formController.add(3);
    _progressController.add(null);
    _loginService.login(username, password).then((_loginInfo) async {
      await _loginService.saveLogin(username, password);
      await _initController(_loginInfo);
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => HomeScreen(),
        ),
      );
    }).catchError((error) async {
      if (error.runtimeType == ErrorStatus) {
        await CustomDialog.showAlertDialog(
            context, "Error " + error.code.toString(), error.toString());
      } else {
        await CustomDialog.showAlertDialog(context, "Error", error.toString());
      }
      await LoginService.deleteLoginSave();
      _formController.add(1);
    });
  }

  void onRegister(BuildContext context, String username, String password) {
    _buildNewForm(3);
    _loginService.register(username, password).then((_loginInfo) async {
      await Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => ExRegisterScreen(
            token: _loginInfo.getToken,
            onLogOutError: () {
              _formController.add(2);
              return;
            },
          ),
        ),
      );
      await _loginService.saveLogin(username, password);
      await _initController(_loginInfo);
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => HomeScreen(),
        ),
      );
    }).catchError((error) async {
      if (error.runtimeType == ErrorStatus) {
        await CustomDialog.showAlertDialog(
            context, "Error " + error.code.toString(), error.toString());
      } else {
        await CustomDialog.showAlertDialog(context, "Error", error.toString());
      }
      await LoginService.deleteLoginSave();
      _formController.add(2);
    });
  }
}
