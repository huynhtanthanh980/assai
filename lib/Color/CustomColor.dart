import 'package:flutter/material.dart';

class SplashScreenColor {
  static Color background = Color(0xFF1b1b1b);
}

class LoginScreenColor {
  static Color background = Color(0xFF1b1b1b);
  static Color loginPad = Color(0xFF292929);
  static Color textColor = Color(0xFFffa500);
  static Color slide1Background = Color(0xFF1b1b1b);
  static Color slide2Background = Color(0xFF292929);
  static Color slide3Background = Colors.white;
}

class HomeScreenColor {
  static Color background = Color(0xFF1b1b1b);
  static Color navigationBarBackground = Color(0xFF808080);
  static Color floatingButton = Color(0xFFffa500);
  static Color navigationBarButtonIconOff = Color(0xFF1b1b1b);
  static Color navigationBarButton1 = Color(0xFFffa500);
  static Color navigationBarButton2 = Color(0xFFffa500);
  static Color navigationBarButton3 = Color(0xFFffa500);
  static Color navigationBarButton4 = Color(0xFFffa500);
}

class DetailScreenColor {
  static Color background = Color(0xFF292929);
  static Color appBar = Colors.grey;
  static Color seasonCard = Color(0xFFffa500);
  static Color buttonBoxShadow = Colors.black12;
  static Color mainText = Color(0xCCffa31a);
  static Color subText = Color(0xFF808080);
  static Color seasonMainText = Color(0xFF1b1b1b);
  static Color seasonSubText = Color(0xCCFFFFFF);
}

class MovieCardColor {
  static Color mainText = Color(0xCCffa31a);
  static Color subText = movieCardShadow;
  static Color movieCard = Color(0xFF292929);
  static Color movieCardShadow = Color(0xFF808080);
  static Color emptyColor = Colors.grey;
  static Color thumbnailBorder = Colors.black38;
  static const Color movieTag = Color(0xFF1b1b1b);
  static Color infoButton = Colors.white24;
}

class WatchingScreenColor {
  static Color background = Color(0xFF1b1b1b);
  static Color header = Color(0xFFffa500);
  static Color sortBar = Color(0xFF292929);
  static Color loadText = Color(0xCCffa31a);
  static Color loadBackGround = Color(0xFF808080);
}

class FavouriteScreenColor {
  static Color background = Color(0xFF1b1b1b);
  static Color header = Color(0xFFffa500);
  static Color sortBar = Color(0xFF292929);
}

class TinderScreenColor {
  static Color background = Color(0xFF1b1b1b);
  static Color header = Color(0xFFffa500);
  static Color sortBar = Color(0xFF292929);
  static Color emptyCardColor = Color(0xFF292929);
}

class SearchScreenColor {
  static Color background = Color(0xFF1b1b1b);
  static Color header = Color(0xFFffa500);
  static Color searchBar = Color(0xFF292929);
  static Color searchBox = Color(0xFF808080);
  static Color mainText = Color(0xCCffa31a);
  static Color filterBox = searchBar;
  static Color defaultTag = Colors.white10;
  static Color removedTag = Colors.red;
  static Color addedTag = Colors.green;
}

class MenuScreenColor {
  static Color background = Color(0xFF1b1b1b);
  static Color header = Color(0xFFffa500);
  static Color button = Color(0xFFffa500);
  static Color text = Color(0xFF292929);
}

class DialogColor {
  static Color background = Color(0xFF1b1b1b);
  static Color lightBackground = Color(0xFF292929);
  static Color text = Color(0xFFffa500);
}